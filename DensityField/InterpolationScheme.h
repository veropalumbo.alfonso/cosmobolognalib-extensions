#include "CBL.h"

#ifndef _INTERPS_
#define _INTERPS_

namespace cbl {
    namespace data{
        /**
         *  @enum InterpolationScheme
         *  @brief the catalogue interpolationSchemeiables
         */
        enum class InterpolationScheme {
            /// Nearest Grid Point
            _NGP_,

            /// Cloud-in-cell
            _CIC_
        };

        /**
         * @brief return a vector containing the
         * InterpolationScheme names
         * @return a vector containing the
         * InterpolationScheme names
         */
        inline std::vector<std::string> InterpolationSchemeNames ()
        { return {"NGP", "CIC"}; }

        /**
         * @brief cast an enum of type InterpolationScheme
         * from its index
         * @param interpolationSchemeIndex the interpolationScheme index
         * @return object of class InterpolationScheme
         */
        inline InterpolationScheme InterpolationSchemeCast (const int interpolationSchemeIndex)
        { return castFromValue<InterpolationScheme>(interpolationSchemeIndex); }

        /**
         * @brief cast an enum of type InterpolationScheme
         * from its name
         * @param interpolationSchemeName the interpolationScheme name
         * @return object of class InterpolationScheme
         */
        inline InterpolationScheme InterpolationSchemeCast (const std::string interpolationSchemeName)
        { return castFromName<InterpolationScheme>(interpolationSchemeName, InterpolationSchemeNames()); }

        /**
         * @brief cast an enum of type InterpolationScheme
         * from indeces
         * @param interpolationSchemeIndeces the interpolationScheme indeces
         * @return object of class InterpolationScheme
         */
        inline std::vector<InterpolationScheme> InterpolationSchemeCast (const std::vector<int> interpolationSchemeIndeces)
        { return castFromValues<InterpolationScheme>(interpolationSchemeIndeces); }

        /**
         * @brief cast an enum of type InterpolationScheme
         * from thier names
         * @param interpolationSchemeNames the interpolationScheme names
         * @return objects of class InterpolationScheme
         */
        inline std::vector<InterpolationScheme> InterpolationSchemeCast (const std::vector<std::string> interpolationSchemeNames)
        { return castFromNames<InterpolationScheme>(interpolationSchemeNames, InterpolationSchemeNames()); }
    }
}

#endif
