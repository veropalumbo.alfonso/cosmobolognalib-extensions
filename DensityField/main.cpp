#include "DensityField.h"

// these two variables contain the name of the CosmoBolognaLib
// directory and the name of the current directory (useful when
// launching the code on remote systems)
std::string cbl::par::DirCosmo = DIRCOSMO, cbl::par::DirLoc = DIRL;

using namespace std;
using namespace cbl;
using namespace cbl::catalogue;

int main() {

    string file_catalogue = "/Users/alfonso/Work/Test-Density/minerva_001.dat";

    Catalogue catalogue(ObjectType::_Galaxy_, CoordinateType::_comoving_, {file_catalogue});

    double volume = pow(1500., 3);
    double rho_mean = catalogue.nObjects()/volume;
    double pad = -1;
    double cell_size = 15;
    data::InterpolationScheme interpType = data::InterpolationScheme::_CIC_;
    double bias = 2.7;

    data::DensityContrastField density(catalogue, cell_size, pad, interpType, false, bias, rho_mean);
    density.FourierTransformField();

    return 0;
}
