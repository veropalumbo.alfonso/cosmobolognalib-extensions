%module densityfield

%include "stl.i"
%include "stdint.i"
%include "std_string.i"
%include "std_vector.i"
%include <std_shared_ptr.i>

%shared_ptr(cbl::catalogue::Catalogue);

%import (module="CosmoBolognaLib") "Field3D.h"
%import (module="CosmoBolognaLib") "Catalogue.h"

%{
#include "InterpolationScheme.h"
#include "DensityField.h"
%}

%include "InterpolationScheme.h"
%include "DensityField.h"
