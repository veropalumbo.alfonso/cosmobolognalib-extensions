#include "InterpolationScheme.h"

#ifndef _DENSF_
#define _DENSF_

namespace cbl {
    namespace data{
        class DensityContrastField : public cbl::data::ScalarField3D
        {
            protected:

                bool m_isPeriodic;

                int m_pad;

                void m_interp_NGP (const double xx, const double yy, const double zz, const double ww);

                void m_interp_CIC (const double xx, const double yy, const double zz, const double ww);

                void m_initialize ();

                void m_set_volume (const double cell_size, const double xMin, const double xMax, const double yMin, const double yMax, const double zMin, const double zMax, const double pad);

            public:
                DensityContrastField() : ScalarField3D() {}

                ~DensityContrastField() = default;

                /**
                 *  @brief constructor
                 *
                 *  @param catalogue input catalogue
                 *  @param cell_size size of the cubic cells
                 *  @param pad padding region, same coordinates of the catalogue
                 *  @param interpScheme, interpolation scheme
                 *  @param useMass, use mass information from catalogue
                 *  @param bias, the bias
                 *  @param rho_mean, the mean_density
                 *
                 *  @return object of type Field3D
                 */
                DensityContrastField (const catalogue::Catalogue catalogue, const double cell_size, const double pad=-1, const InterpolationScheme interpScheme = InterpolationScheme::_CIC_, const bool useMass=false, const double bias=1, const double rho_mean=-1);

                /**
                 *  @brief constructor
                 *
                 *  @param catalogue input catalogue
                 *  @param mask the mask, in form of random sample of objects
                 *  @param cell_size size of the cubic cells
                 *  @param pad padding region, same coordinates of the catalogue
                 *  @param interpScheme, interpolation scheme
                 *  @param useMass, use mass information from catalogue
                 *  @param bias, the bias
                 *  @param rho_mean, the mean_density
                 *
                 *  @return object of type Field3D
                 */
                DensityContrastField (const catalogue::Catalogue catalogue, const catalogue::Catalogue mask, const double cell_size, const double pad=-1, const InterpolationScheme interpScheme = InterpolationScheme::_CIC_, const bool useMass=false, const double bias=1, const double rho_mean=-1);

                /**
                 *  @brief constructor
                 *
                 *  @param catalogue input catalogue
                 *  @param cell_size size of the cubic cells
                 *  @param pad padding region, same coordinates of the catalogue
                 *  @param interpScheme, interpolation scheme
                 *  @param useMass, use mass information from catalogue
                 *  @param bias, the bias
                 *  @param rho_mean, the mean_density
                 */
                void set_catalogue (const catalogue::Catalogue catalogue, const double cell_size, const double pad=-1, const InterpolationScheme interpScheme = InterpolationScheme::_CIC_, const bool useMass=false, const double bias=1, const double rho_mean=-1);

                /**
                 *  @brief constructor
                 *
                 *  @param catalogue input catalogue
                 *  @param mask the mask, in form of random sample of objects
                 *  @param cell_size size of the cubic cells
                 *  @param pad padding region, same coordinates of the catalogue
                 *  @param interpScheme, interpolation scheme
                 *  @param useMass, use mass information from catalogue
                 *  @param bias, the bias
                 *  @param rho_mean, the mean_density
                 */
                void set_catalogue (const catalogue::Catalogue catalogue, const catalogue::Catalogue mask, const double cell_size, const double pad=-1, const InterpolationScheme interpScheme = InterpolationScheme::_CIC_, const bool useMass=false, const double bias=1, const double rho_mean=-1);
        };
    }
}

#endif
