#include "DensityField.h"

using namespace std;
using namespace cbl;

//===========================================================


void cbl::data::DensityContrastField::m_interp_NGP(const double xx, const double yy, const double zz, const double ww)
{
   int i = int((xx-m_MinX)/m_deltaX);
   int j = int((yy-m_MinY)/m_deltaY);
   int k = int((zz-m_MinZ)/m_deltaZ);

   this->set_ScalarField(ww, i, j, k, true);
}


//===========================================================


void cbl::data::DensityContrastField::m_interp_CIC(const double xx, const double yy, const double zz, const double ww)
{
    int i1 = int((xx-m_MinX)/m_deltaX);
    int j1 = int((yy-m_MinY)/m_deltaY);
    int k1 = int((zz-m_MinZ)/m_deltaZ);

    double dx = (xx-this->XX(i1))/m_deltaX;
    double dy = (yy-this->YY(j1))/m_deltaY;
    double dz = (zz-this->ZZ(k1))/m_deltaZ;

    int i2 = (dx<0) ? i1-1 : i1+1;
    int j2 = (dy<0) ? j1-1 : j1+1;
    int k2 = (dz<0) ? k1-1 : k1+1;

    if (m_isPeriodic) {
        i2 = (i2<0) ? m_nX-i2 : i2;
        i2 = (i2>m_nX) ? i2-m_nX : i2;
        j2 = (j2<0) ? m_nY-j2 : j2;
        j2 = (j2>m_nY) ? j2-m_nY : j2;
        k2 = (k2<0) ? m_nZ-k2 : k2;
        k2 = (k2>m_nZ) ? k2-m_nZ : k2;
    }

    dx = fabs(dx);
    dy = fabs(dy);
    dz = fabs(dz);

    double tx = 1-dx;
    double ty = 1-dy;
    double tz = 1-dz;

    double weight = 0.;
    weight += ww*tx*ty*tz;
    weight += ww*dx*ty*tz;
    weight += ww*tx*dy*tz;
    weight += ww*tx*ty*dz;
    weight += ww*dx*dy*tz;
    weight += ww*dx*ty*dz;
    weight += ww*tx*dy*dz;
    weight += ww*dx*dy*dz;

    this->set_ScalarField(weight*tx*ty*tz, i1, j1, k1, 1);
    this->set_ScalarField(weight*dx*ty*tz, i2, j1, k1, 1);
    this->set_ScalarField(weight*tx*dy*tz, i1, j2, k1, 1);
    this->set_ScalarField(weight*tx*ty*dz, i1, j1, k2, 1);
    this->set_ScalarField(weight*dx*dy*tz, i2, j2, k1, 1);
    this->set_ScalarField(weight*dx*ty*dz, i2, j1, k2, 1);
    this->set_ScalarField(weight*tx*dy*dz, i1, j2, k2, 1);
    this->set_ScalarField(weight*dx*dy*dz, i2, j2, k2, 1);
}


//===========================================================


void cbl::data::DensityContrastField::m_set_volume(const double cell_size, const double xMin, const double xMax, const double yMin, const double yMax, const double zMin, const double zMax, const double pad)
{
    // Periodic volume
    m_pad = pad;
    m_isPeriodic = false;

    if (pad < 0) {
        m_isPeriodic = true;
        m_pad = 0;
    }
    double pp = m_pad*cell_size;

    this->set_parameters (cell_size, xMin-pp, xMax+pp, yMin-pp, yMax+pp, zMin-pp, zMax+pp);

    m_initialize();
}


//===========================================================


void cbl::data::DensityContrastField::m_initialize ()
{
  m_field = fftw_alloc_real(m_nCells);
  m_field_FourierSpace = fftw_alloc_complex(m_nCells_Fourier);

  for(int i=0;i<m_nCells;i++)
    m_field[i]=0;

  for(int i=0;i<m_nCells_Fourier;i++){
    m_field_FourierSpace[i][0] = 0;
    m_field_FourierSpace[i][1] = 0;
  }
}



//===========================================================


cbl::data::DensityContrastField::DensityContrastField (const catalogue::Catalogue catalogue, const double cell_size, const double pad, const InterpolationScheme interpScheme, const bool useMass, const double bias, const double rho_mean)
{
    set_catalogue(catalogue, cell_size, pad, interpScheme, useMass, bias, rho_mean);
}


//===========================================================


cbl::data::DensityContrastField::DensityContrastField (const catalogue::Catalogue catalogue, const catalogue::Catalogue mask, const double cell_size, const double pad, const InterpolationScheme interpScheme, const bool useMass, const double bias, const double rho_mean)
{
    set_catalogue(catalogue, mask, cell_size, pad, interpScheme, useMass, bias, rho_mean);
}


//===========================================================


void cbl::data::DensityContrastField::set_catalogue (const catalogue::Catalogue catalogue, const double cell_size, const double pad, const InterpolationScheme interpScheme, const bool useMass, const double bias, const double rho_mean)
{
    double minX = catalogue.Min(catalogue::Var::_X_);
    double maxX = catalogue.Max(catalogue::Var::_X_);
    double minY = catalogue.Min(catalogue::Var::_Y_);
    double maxY = catalogue.Max(catalogue::Var::_Y_);
    double minZ = catalogue.Min(catalogue::Var::_Z_);
    double maxZ = catalogue.Max(catalogue::Var::_Z_);

    this->m_set_volume(cell_size, minX, maxX, minY, maxY, minZ, maxZ, pad);

    double cell_volume = pow(cell_size, 3);
    double den = (useMass) ? rho_mean :  cell_volume * rho_mean;
    coutCBL << "Mean density used is " << den << endl;

    for (size_t i=0; i<catalogue.nObjects(); i++) {
        double num = (useMass) ? catalogue.weight(i) * catalogue.mass(i) :  catalogue.weight(i);

        if (interpScheme == InterpolationScheme::_NGP_) {
            m_interp_NGP (catalogue.xx(i), catalogue.yy(i), catalogue.zz(i), num);
        }
        else if  (interpScheme == InterpolationScheme::_CIC_) {
            m_interp_CIC (catalogue.xx(i), catalogue.yy(i), catalogue.zz(i), num);
        }
    }

    for (int i=0; i<m_nX; i++)
        for (int j=0; j<m_nY; j++)
            for (int k=0; k<m_nZ; k++) {
                double ww =( this->ScalarField(i,j,k)/den-1)/bias;
                this->set_ScalarField(ww, i, j, k, true);
            }
}


//===========================================================


void cbl::data::DensityContrastField::set_catalogue (const catalogue::Catalogue catalogue, const catalogue::Catalogue mask, const double cell_size, const double pad, const InterpolationScheme interpScheme, const bool useMass, const double bias, const double rho_mean)
{   
    (void)catalogue; (void)mask;
    (void)cell_size; (void)pad; (void)rho_mean;
    (void)interpScheme; (void)useMass; (void)bias;

    ErrorCBL("Method not yet implemented", "set_catalogue", "DensityField.cpp");

}
