CXX = g++-7
SWIG = swig
PY = python

# GSL installation directories
GSL_VERSION = $(shell gsl-config --version)
dir_INC_GSL = $(shell gsl-config --prefix)/include/
dir_LIB_GSL = $(shell gsl-config --prefix)/lib/

# FFTW installation directories
dir_INC_FFTW = /usr/local/include/
dir_LIB_FFTW =/usr/local/lib/

# boost installation directories
dir_INC_BOOST = /usr/local/include/
dir_LIB_BOOST = /usr/local/lib/


FLAGS0 = -std=c++11 -fopenmp 
FLAGS = -O3 -unroll -Wall -Wextra -pedantic -Wfatal-errors -Werror

dirLib = $(HOME)/CosmoBolognaLib/
dirH = $(dirLib)Headers/
dir_Eigen = $(dirLib)External/Eigen/eigen-3.3.7/
dir_CCfits = $(dirLib)External/CCfits/include
dirCUBA = $(dirLib)External/Cuba-4.2/

varDIR = -DDIRCOSMO=\"$(dirLib)\" -DDIRL=\"$(PWD)/\"

FLAGS_LIB = -Wl,-rpath,$(HOME)/lib/ -Wl,-rpath,$(dirLib) -L$(dirLib) -lCBL
FLAGS_INC = -I$(dirH) -I$(dirCUBA) -I$(dir_Eigen) -I$(dir_CCfits) -I$(dir_INC_GSL) -I$(dir_INC_FFTW) -isystem$(dir_INC_BOOST)

OBJ1 = DensityField.o 
OBJ2 = main.o 

FLAGS_LINK = -shared
ES = so

# Python-related flags
python_version_full := $(wordlist 2,4,$(subst ., ,$(shell $(PY) --version 2>&1)))
python_version_major := $(word 1,${python_version_full})
python_version_minor := $(word 2,${python_version_full})

ifeq ($(python_version_major),2)
	PYINC = $(shell $(PY) -c 'from distutils import sysconfig; print sysconfig.get_config_var("INCLUDEDIR")')
	SWIG_FLAG = -python -c++
	PYVERSION = $(python_version_major).$(python_version_minor)
endif
ifeq ($(python_version_major),3)
	PYINC = $(shell $(PY) -c 'from distutils import sysconfig; print(sysconfig.get_config_var("INCLUDEDIR"))')
	SWIG_FLAG = -python -c++ -py3
	PYVERSION = $(python_version_major).$(python_version_minor)m
endif

PFLAGS = -I$(PYINC)/python$(PYVERSION)

SYS:=$(shell uname -s)

ifeq ($(SYS),Darwin)
	    FLAGS_LINK = -dynamiclib -undefined dynamic_lookup
        ES = dylib
endif

libDF: $(OBJ1) $(PWD)/Makefile
	$(CXX) $(FLAGS_LINK) -o $(PWD)/libDF.$(ES) $(FLAGS_LIB) $(OBJ1)

libdensityfield: DensityField_wrap.o DensityField.i
	$(CXX) $(FLAGS_LINK) -o _densityfield.so DensityField_wrap.o $(FLAGS_LIB)  -Wl,-rpath,$(PWD) -L$(PWD) -lDF
 
main: $(OBJ1) $(OBJ2)
	$(CXX) $(OBJ2) -o main.exe $(FLAGS_LIB)  -Wl,-rpath,$(PWD) -L$(PWD) -lDF

clean:
	rm -f *.o *.exe *.py *.so *.$(ES) *.cxx *~ \#* temp* core*

main.o: main.cpp makefile $(dirLib)*.$(ES)
	$(CXX) $(FLAGS0) $(FLAGS) $(FLAGS_INC) $(varDIR) -c main.cpp

DensityField.o: DensityField.h DensityField.cpp makefile $(dirLib)*.$(ES)
	$(CXX) $(FLAGS0) $(FLAGS) $(FLAGS_INC) $(varDIR) -c -fPIC DensityField.cpp

DensityField_wrap.o: DensityField_wrap.cxx DensityField.i DensityField.h
	$(CXX) $(FLAGST) -Wno-uninitialized $(PFLAGS) -c -fPIC $(FLAGS_INC) DensityField_wrap.cxx -o DensityField_wrap.o 

DensityField_wrap.cxx: DensityField.h DensityField.i
	$(call colorecho, "\n"Running swig... "\n")
	$(SWIG) $(SWIG_FLAG) -I$(dirH) -I$(dirLib)/Python/ DensityField.i
