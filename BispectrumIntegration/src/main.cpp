#include "CBL.h"
#include "Integrator.h"
#include <chrono>
  
// these two variables contain the name of the CosmoBolognaLib
// directory and the name of the current directory (useful when
// launching the code on remote systems)
std::string cbl::par::DirCosmo = DIRCOSMO, cbl::par::DirLoc = DIRL;

using namespace std;
using namespace cbl;

int main() {

    vector<double> kk, pk;

    cbl::read_vector ("./pk.dat", kk, pk, {0, 1});

    auto interp_pk = make_shared<glob::FuncGrid>(glob::FuncGrid(kk, pk, "Spline"));

    // ----------------------------------------
    // ---------------- Speed test ------------
    // ----------------------------------------

    const size_t niter = 1;

    const double k1 = 10;
    const double k2 = 20;
    const double k3 = 25;
    const double qq = 10;
    const double pp = 9.09;
    const double phi = 3;
    double ii;

    // Start timer
    auto begin = std::chrono::high_resolution_clock::now();

    for (size_t i=0; i<niter; i++) {
        double i1 = sptintegration::kernel_B222(qq, pp, phi, k1, k2, k3);
        //double i2 = sptintegration::Integrand_B411(qq, k2, k3, k1, *interp_pk);
        //double i3 = sptintegration::Integrand_B411(qq, k3, k1, k2, *interp_pk);
        ii = i1;
        (void)ii;
    }
    auto end = std::chrono::high_resolution_clock::now();
    std::cout << std::chrono::duration_cast<std::chrono::milliseconds>(end-begin).count()/niter << "ns" << std::endl;

    std::cout<<setprecision(20) << ii<<endl;

    // -----------------------------------------
    // ---------------- Integration ------------
    // -----------------------------------------
    
    /*
    begin = std::chrono::high_resolution_clock::now();

    double error;
    int nsub = 100;
    size_t nevals = 10000;
    
    for (size_t i=0; i<niter; i++) {
        double val = sptintegration::integrate_B411 (k1, k2, k3, *interp_pk, 1.e-4, 30, 1.e-5, 0, nsub, nevals, error);
        cout << i << " " << val << " " << error << " " << error/val << " " << nevals <<  endl;
    }

    end = std::chrono::high_resolution_clock::now();

    std::cout << std::chrono::duration_cast<std::chrono::milliseconds>(end-begin).count()/niter << "ms" << std::endl;
    */

    return 0;
}
