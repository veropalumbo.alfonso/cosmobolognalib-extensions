#include "Integrator.h"

using namespace std;
using namespace cbl;

// =============================================================================


double sptintegration::kernel_B222 (const double &q, const double &p, const double &phi, const double &k1, const double &k2, const double &k3)
{
  double kernel = 0;
  sptintegration::kernel_b222_(&kernel, &q, &p, &phi, &k1, &k2, &k3);
  return kernel;
}


// =============================================================================


double sptintegration::kernel_B321_I (const double &q, const double &p, const double &k3, const double &k1, const double &k2)
{
  double kernel = 0;
  sptintegration::kernel_b321_i_(&kernel, &q, &p, &k3, &k1, &k2);
  return kernel;
}


// =============================================================================


double sptintegration::kernel_B321_II (const double &q, const double &k1, const double &k2, const double &k3)
{
  double kernel = 0;
  sptintegration::kernel_b321_ii_(&kernel, &q, &k1, &k2, &k3);
  return kernel;
}
 

// =============================================================================


double sptintegration::kernel_B411 (const double &q, const double &k1, const double &k2, const double &k3)
{
  double kernel = 0;
  sptintegration::kernel_b411_(&kernel, &q, &k1, &k2, &k3);
  return kernel;
}


// =============================================================================


double sptintegration::kernel_B411_old (const double &q, const double &k1, const double &k2, const double &k3)
{
  double kernel = 0;
  sptintegration::kernel_b411_old_(&kernel, &q, &k1, &k2, &k3);
  return kernel;
}


// =============================================================================


double sptintegration::integrand_B222(const double &q, const double &p,  const double &phi, const double &k1, const double &k2, const double &k3, const cbl::glob::FuncGrid &interp_Pk, const double &qmin, const double &qmax)
{
   const double cos_theta = (Power(p, 2) - Power(k1, 2) - Power(q, 2))/(-2*q*k1);
   double const cos_theta2 = (Power(k3, 2) - Power(k1, 2) - Power(k2, 2))/(2*k1*k2);
   const double sin_theta = sqrt(1-Power(cos_theta, 2));
   const double sin_theta2 = sqrt(1-Power(cos_theta2, 2));

    double const phi2 = 0;
    double const mu = sin_theta*sin_theta2*cos(phi2 - phi) + cos_theta*cos_theta2;
    double const p2 = sqrt(Power(k2, 2) + Power(q, 2) + 2*q*k2*mu);

    if ( p2<qmin or p2>qmax)
      return 0;

    double value = fabs((p*q / k1)) * kernel_B222(q, p, phi, k1, k2, k3) * interp_Pk(q) * interp_Pk(p) * interp_Pk(p2);
    return value;
}


// =============================================================================


int sptintegration::CUBA_integrand_B222 (const int *ndim, const cubareal xx[], const int *ncomp, cubareal ff[], void *userdata)
{

  (void)*ndim;
  bispectrum_integral_inputs *pp = static_cast<bispectrum_integral_inputs *>(userdata);

  std::vector<double> var(*ndim, 0);

  var[0] = exp(xx[0]*(pp->log_qmax-pp->log_qmin)+pp->log_qmin);

  double pmin = (pp->force_range) ? max(pp->qmin, fabs(pp->k1-var[0])) : fabs(pp->k1-var[0]);
  double pmax = (pp->force_range) ? min(pp->qmax, pp->k1+var[0]) : pp->k1+var[0];

  var[1] = xx[1]*(pmax-pmin)+pmin;

  var[2] = 2*cbl::par::pi *xx[2];

  ff[*ncomp-1] = pp->integrand(var) * (pp->log_qmax-pp->log_qmin) * (pmax-pmin) * 2 * cbl::par::pi * var[0];
  return 0;
}


// =============================================================================


double sptintegration::integrand_B321_I(const double &q, const double &p, const double &k1, const double &k2, const double &k3, const cbl::glob::FuncGrid &interp_Pk)
{
    return fabs((p*q / k1)) * 0.5 * ( kernel_B321_I(q, p, k3, k1, k2) * interp_Pk(k2) + kernel_B321_I(q, p, k2, k1, k3) * interp_Pk(k3) ) * interp_Pk(q) * interp_Pk(p);
}



// =============================================================================


int sptintegration::CUBA_integrand_B321_I (const int *ndim, const cubareal xx[], const int *ncomp, cubareal ff[], void *userdata)
{
  (void)*ndim;
  bispectrum_integral_inputs *pp = static_cast<bispectrum_integral_inputs *>(userdata);

  std::vector<double> var(*ndim, 0);

  var[0] = xx[0]*(pp->qmax-pp->qmin)+pp->qmin;

  double pmin = (pp->force_range) ? max(pp->qmin, fabs(pp->k1-var[0])) : fabs(pp->k1-var[0]);
  double pmax = (pp->force_range) ? min(pp->qmax, pp->k1+var[0]) : pp->k1+var[0];
  //double pmax = pp->k1+var[0];
  var[1] = xx[1]*(pmax-pmin)+pmin;

  ff[*ncomp-1] = pp->integrand(var)*(pp->qmax-pp->qmin)*(pmax-pmin);
  return 0;
}


// =============================================================================


double sptintegration::integrand_B321_II(const double &q, const double &k1, const double &k2, const double &k3, const cbl::glob::FuncGrid &interp_Pk)
{
    return kernel_B321_II(q, k1, k2, k3) * Power(q, 2) * interp_Pk(q);
}


// =============================================================================


double sptintegration::integrand_B411(const double &q, const double &k1, const double &k2, const double &k3, const cbl::glob::FuncGrid &interp_Pk)
{
    return kernel_B411(q, k1, k2, k3) * Power(q, 2) * interp_Pk(q);
}


// =============================================================================


sptintegration::IntegrationResult sptintegration::integrate_1D (cbl::FunctionDoubleDouble integrand, const double a, const double b, const double rel_err, const double abs_err, const size_t nsub)
{
    sptintegration::IntegrationResult result;
    gsl_set_error_handler_off();

    gsl_integration_cquad_workspace *ww = gsl_integration_cquad_workspace_alloc(nsub);

    cbl::wrapper::gsl::STR_generic_func_GSL params;
    params.f = integrand;

    gsl_function Func;  
    Func.function = cbl::wrapper::gsl::generic_function;
    Func.params = &params;

    result.status = gsl_integration_cquad(&Func, a, b, abs_err, rel_err, ww, &result.Int, &result.error, &result.nEvals); 

    gsl_integration_cquad_workspace_free(ww);

    if (result.status != 0 )
        cbl::wrapper::gsl::check_GSL_fail(result.status, false, "integrate_1D", "gsl_integration_cquad");

    return result;
}


// =============================================================================


sptintegration::IntegrationResult sptintegration::integrate_B222 (const double &k1, const double &k2, const double &k3, const cbl::glob::FuncGrid &interp_Pk, const double a, const double b, const double rel_err, const double abs_err, const size_t nevals, const bool force_range)
{
  wrapper::cuba::STR_CUBA_inputs inputs;
  inputs.EPSREL = rel_err;
  inputs.KEY = 11;
  inputs.EPSABS = abs_err;
  inputs.MAXEVAL = nevals;

  int comp, neval, fail, nregions;
  (void)comp;
  vector<double> integral(inputs.NCOMP), error(inputs.NCOMP), prob(inputs.NCOMP);

  bispectrum_integral_inputs *userdata = new bispectrum_integral_inputs;
  userdata->integrand = [&] (const vector<double> &var) 
    {return sptintegration::integrand_B222(var[0], var[1], var[2], k1, k2, k3, interp_Pk, a, b);};
  userdata->qmin = a;
  userdata->qmax = b;

  userdata->log_qmin = log(a);
  userdata->log_qmax = log(b);
  userdata->k1 = k1;
  userdata->k2 = k2;
  userdata->k3 = k3;
  userdata->force_range = force_range;

  int ndim = 3;

  Cuhre(ndim, inputs.NCOMP, CUBA_integrand_B222, userdata, inputs.NVEC,
      inputs.EPSREL, inputs.EPSABS, inputs.VERBOSE | inputs.LAST,
      inputs.MINEVAL, inputs.MAXEVAL, inputs.KEY,
      inputs.STATEFILE, inputs.SPIN.get(),
      &nregions, &neval, &fail, integral.data(), error.data(), prob.data());

  delete userdata;

  sptintegration::IntegrationResult result;

  result.Int = integral[0];
  result.error = error[0];
  result.nEvals = neval;
  result.status = fail;

  return result;
}

// =============================================================================


sptintegration::IntegrationResult sptintegration::integrate_B321_I (const double &k1, const double &k2, const double &k3, const cbl::glob::FuncGrid &interp_Pk, const double a, const double b, const double rel_err, const double abs_err, const size_t max_evals, const bool force_range)
{
  wrapper::cuba::STR_CUBA_inputs inputs;
  inputs.EPSREL = rel_err;
  inputs.KEY = 13;
  inputs.EPSABS = abs_err;
  inputs.MAXEVAL = max_evals;

  int comp, neval, fail, nregions;
  (void)comp;
  vector<double> integral(inputs.NCOMP), error(inputs.NCOMP), prob(inputs.NCOMP);

  bispectrum_integral_inputs *userdata = new bispectrum_integral_inputs;
  userdata->integrand = [&] (const vector<double> &var) 
  {return sptintegration::integrand_B321_I(var[0], var[1], k1, k2, k3, interp_Pk);};
  userdata->qmin = a;
  userdata->qmax = b;
  userdata->k1 = k1;
  userdata->k2 = k2;
  userdata->k3 = k3;
  userdata->force_range = force_range;

  int ndim = 2;

  Cuhre(ndim, inputs.NCOMP, CUBA_integrand_B321_I, userdata, inputs.NVEC,
      inputs.EPSREL, inputs.EPSABS, inputs.VERBOSE | inputs.LAST,
      inputs.MINEVAL, inputs.MAXEVAL, inputs.KEY,
      inputs.STATEFILE, inputs.SPIN.get(),
      &nregions, &neval, &fail, integral.data(), error.data(), prob.data());

  delete userdata;

  sptintegration::IntegrationResult result;

  result.Int = integral[0];
  result.error = error[0];
  result.nEvals = neval;
  result.status = fail;

  return result;
}


// =============================================================================



sptintegration::IntegrationResult sptintegration::integrate_B321_II (const double &k1, const double &k2, const double &k3, const cbl::glob::FuncGrid &interp_Pk, const double a, const double b, const double rel_err, const double abs_err, const size_t nsub)
{
    auto integrand = [&] (const double ln_q) {return sptintegration::integrand_B321_II(exp(ln_q), k1, k2, k3, interp_Pk) * exp(ln_q);};
    return sptintegration::integrate_1D(integrand, log(a), log(b), rel_err, abs_err, nsub);
}


// =============================================================================


sptintegration::IntegrationResult sptintegration::integrate_B411 (const double &k1, const double &k2, const double &k3, const cbl::glob::FuncGrid &interp_Pk, const double a, const double b, const double rel_err, const double abs_err, const size_t nsub)
{
    vector<double> qlist = cbl::logarithmic_bin_vector(100, a*0.99, b*1.01);
    vector<double> ker(qlist.size());

#pragma omp parallel num_threads(omp_get_max_threads())
    {
#pragma omp for schedule(dynamic)
        for(size_t i=0; i<qlist.size(); i++)
            ker[i] = kernel_B411(qlist[i], k1, k2, k3);
    }

    cbl::glob::FuncGrid kernel(qlist, ker, "Spline");

    auto integrand = [&] (const double ln_q) 
    {return kernel(exp(ln_q))* interp_Pk(exp(ln_q)) * pow(exp(ln_q),3);};

    return sptintegration::integrate_1D(integrand, log(a), log(b), rel_err, abs_err, nsub);
}


// =============================================================================


sptintegration::IntegrationResult sptintegration::full_B222 (const double &k1, const double &k2, const double &k3, const cbl::glob::FuncGrid &interp_Pk, const double a, const double b, const double rel_err, const double abs_err, const size_t max_evals, const bool force_range)
{
  double norm = pow(2*Pi, 3);

  sptintegration::IntegrationResult p222 = integrate_B222(k1, k2, k3, interp_Pk, a, b, rel_err, abs_err, max_evals, force_range);
  p222.Int /= norm;
  p222.error /= norm;

  return p222;
}


// =============================================================================


sptintegration::IntegrationResult sptintegration::full_B321_I (const double &k1, const double &k2, const double &k3, const cbl::glob::FuncGrid &interp_Pk, const double a, const double b, const double rel_err, const double abs_err, const size_t max_evals, const bool force_range)
{
  double norm = pow(2*Pi, 2);

  sptintegration::IntegrationResult p123 = integrate_B321_I(k1, k2, k3, interp_Pk, a, b, rel_err, abs_err, max_evals, force_range);
  sptintegration::IntegrationResult p312 = integrate_B321_I(k3, k1, k2, interp_Pk, a, b, rel_err, abs_err, max_evals, force_range);
  sptintegration::IntegrationResult p231 = integrate_B321_I(k2, k3, k1, interp_Pk, a, b, rel_err, abs_err, max_evals, force_range);

  IntegrationResult result;

  result.Int = (p123.Int + p312.Int + p231.Int)/norm;
  result.status = p123.status + p312.status + p231.status;
  result.error = sqrt( pow(p123.error, 2) + pow(p312.error, 2) + pow(p231.error, 2))/norm;
  result.nEvals = (p123.nEvals + p312.nEvals + p231.nEvals)/3;

  return result;
}


// =============================================================================


sptintegration::IntegrationResult sptintegration::full_B321_II (const double &k1, const double &k2, const double &k3, const cbl::glob::FuncGrid &interp_Pk, const double a, const double b, const double rel_err, const double abs_err, const size_t nsub)
{
    double norm = pow(2*Pi, 3);

    double pk1 = interp_Pk(k1);
    double pk2 = interp_Pk(k2);
    double pk3 = interp_Pk(k3);

    sptintegration::IntegrationResult p123 = integrate_B321_II(k1, k2, k3, interp_Pk, a, b, rel_err, abs_err, nsub);
    sptintegration::IntegrationResult p312 = integrate_B321_II(k3, k1, k2, interp_Pk, a, b, rel_err, abs_err, nsub);
    sptintegration::IntegrationResult p231 = integrate_B321_II(k2, k3, k1, interp_Pk, a, b, rel_err, abs_err, nsub);
    sptintegration::IntegrationResult p132 = integrate_B321_II(k1, k3, k2, interp_Pk, a, b, rel_err, abs_err, nsub);
    sptintegration::IntegrationResult p213 = integrate_B321_II(k2, k1, k3, interp_Pk, a, b, rel_err, abs_err, nsub);
    sptintegration::IntegrationResult p321 = integrate_B321_II(k3, k2, k1, interp_Pk, a, b, rel_err, abs_err, nsub);

    IntegrationResult result;

    result.Int = (p123.Int * pk1 * pk2 +
                  p312.Int * pk3 * pk1 +
                  p231.Int * pk2 * pk3 +
                  p132.Int * pk1 * pk3 +
                  p213.Int * pk2 * pk1 +
                  p321.Int * pk3 * pk2)/norm;
    
    result.status = p123.status + p312.status + p231.status + p132.status + p213.status + p321.status;

    result.error = pow(p123.error, 2) + pow(p312.error, 2) + pow(p231.error, 2) + pow(p132.error, 2) + pow(p213.error, 2) + pow(p321.error, 2);
    result.error = sqrt(result.error);

    return result;
}


// =============================================================================


sptintegration::IntegrationResult sptintegration::full_B411 (const double &k1, const double &k2, const double &k3, const cbl::glob::FuncGrid &interp_Pk, const double a, const double b, const double rel_err, const double abs_err, const size_t nsub)
{
    double norm = pow(2*Pi, 3);

    double pk1 = interp_Pk(k1);
    double pk2 = interp_Pk(k2);
    double pk3 = interp_Pk(k3);

    sptintegration::IntegrationResult p123 = integrate_B411(k1, k2, k3, interp_Pk, a, b, rel_err, abs_err, nsub);
    sptintegration::IntegrationResult p312 = integrate_B411(k3, k1, k2, interp_Pk, a, b, rel_err, abs_err, nsub);
    sptintegration::IntegrationResult p231 = integrate_B411(k2, k3, k1, interp_Pk, a, b, rel_err, abs_err, nsub);

    IntegrationResult result;
    result.Int = (p123.Int * pk1 * pk2 +
                  p312.Int * pk3 * pk1 +
                  p231.Int * pk2 * pk3)/norm;
    
    result.status = p123.status + p312.status + p231.status;

    result.error = pow(p123.error, 2) + pow(p312.error, 2) + pow(p231.error, 2);
    result.error = sqrt(result.error);

    return result;
}


// =============================================================================


std::vector<sptintegration::IntegrationResult> sptintegration::full_B222 (const std::vector<double> &k1, const std::vector<double> &k2, const std::vector<double> &k3, const cbl::glob::FuncGrid &interp_Pk, const double a, const double b, const double rel_err, const double abs_err, const size_t max_evals, const bool force_range)
{
    std::vector<sptintegration::IntegrationResult> results(k1.size(), {0, 0, 0, 0});

#pragma omp parallel num_threads(omp_get_max_threads())
    {

#pragma omp for schedule(dynamic)
        for (size_t i=0; i<k1.size(); i++)
            results[i] = full_B222(k1[i], k2[i], k3[i], interp_Pk, a, b, rel_err, abs_err, max_evals, force_range);
    }
    return results;
}


// =============================================================================


std::vector<sptintegration::IntegrationResult> sptintegration::full_B321_I (const std::vector<double> &k1, const std::vector<double> &k2, const std::vector<double> &k3, const cbl::glob::FuncGrid &interp_Pk, const double a, const double b, const double rel_err, const double abs_err, const size_t max_evals, const bool force_range)
{
    std::vector<sptintegration::IntegrationResult> results(k1.size(), {0, 0, 0, 0});

#pragma omp parallel num_threads(omp_get_max_threads())
    {

#pragma omp for schedule(dynamic)
        for (size_t i=0; i<k1.size(); i++)
            results[i] = full_B321_I(k1[i], k2[i], k3[i], interp_Pk, a, b, rel_err, abs_err, max_evals, force_range);
    }
    return results;
}

// =============================================================================


std::vector<sptintegration::IntegrationResult> sptintegration::full_B321_II (const std::vector<double> &k1, const std::vector<double> &k2, const std::vector<double> &k3, const cbl::glob::FuncGrid &interp_Pk, const double a, const double b, const double rel_err, const double abs_err, const size_t nsub)
{
    std::vector<sptintegration::IntegrationResult> results(k1.size(), {0, 0, 0, 0});

#pragma omp parallel num_threads(omp_get_max_threads())
    {

#pragma omp for schedule(dynamic)
        for (size_t i=0; i<k1.size(); i++)
            results[i] = full_B321_II(k1[i], k2[i], k3[i], interp_Pk, a, b, rel_err, abs_err, nsub);
    }
    return results;
}


// =============================================================================


std::vector<sptintegration::IntegrationResult> sptintegration::full_B411 (const std::vector<double> &k1, const std::vector<double> &k2, const std::vector<double> &k3, const cbl::glob::FuncGrid &interp_Pk, const double a, const double b, const double rel_err, const double abs_err, const size_t nsub)
{
    std::vector<sptintegration::IntegrationResult> results(k1.size(), {0, 0, 0, 0});

#pragma omp parallel num_threads(omp_get_max_threads())
    {

#pragma omp for schedule(dynamic)
        for (size_t i=0; i<k1.size(); i++)
            results[i] = full_B411(k1[i], k2[i], k3[i], interp_Pk, a, b, rel_err, abs_err, nsub);
    }
    return results;
}

