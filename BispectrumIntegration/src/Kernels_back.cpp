#include "Integrator.h"

using namespace std;
using namespace cbl;

// =============================================================================


double sptintegration::kernel_B222(const double &q, const double &p, const double &phi, const double &k1, const double &k2, const double &k3)
{
    return (10./7 + (5*(-Power(q,2) + (Power(k1,2) - Power(p,2) + Power(q,2))/2.))/(7.*Power(p,2)) + (5*(-Power(q,2) + (Power(k1,2) - Power(p,2) + Power(q,2))/2.))/(7.*Power(q,2)) + 
     (2*Power(k1,2)*(-Power(q,2) + (Power(k1,2) - Power(p,2) + Power(q,2))/2.))/(7.*Power(p,2)*Power(q,2)))*
   (10./7 + (5*(-Power(q,2) - ((-Power(k1,2) - Power(k2,2) + Power(k3,2))*(Power(k1,2) - Power(p,2) + Power(q,2)) + 
             Sqrt((k1 + k2 - k3)*(k1 - k2 + k3)*(-k1 + k2 + k3)*(k1 + k2 + k3))*Sqrt((k1 + p - q)*(k1 - p + q)*(-k1 + p + q)*(k1 + p + q))*Cos(phi))/(4.*Power(k1,2))))/(7.*Power(q,2)) + 
     (5*(-Power(q,2) - ((-Power(k1,2) - Power(k2,2) + Power(k3,2))*(Power(k1,2) - Power(p,2) + Power(q,2)) + 
             Sqrt((k1 + k2 - k3)*(k1 - k2 + k3)*(-k1 + k2 + k3)*(k1 + k2 + k3))*Sqrt((k1 + p - q)*(k1 - p + q)*(-k1 + p + q)*(k1 + p + q))*Cos(phi))/(4.*Power(k1,2))))/
      (7.*(Power(k2,2) + Power(q,2) + ((-Power(k1,2) - Power(k2,2) + Power(k3,2))*(Power(k1,2) - Power(p,2) + Power(q,2)) + 
             Sqrt((k1 + k2 - k3)*(k1 - k2 + k3)*(-k1 + k2 + k3)*(k1 + k2 + k3))*Sqrt((k1 + p - q)*(k1 - p + q)*(-k1 + p + q)*(k1 + p + q))*Cos(phi))/(2.*Power(k1,2)))) + 
     (2*Power(k2,2)*(-Power(q,2) - ((-Power(k1,2) - Power(k2,2) + Power(k3,2))*(Power(k1,2) - Power(p,2) + Power(q,2)) + 
             Sqrt((k1 + k2 - k3)*(k1 - k2 + k3)*(-k1 + k2 + k3)*(k1 + k2 + k3))*Sqrt((k1 + p - q)*(k1 - p + q)*(-k1 + p + q)*(k1 + p + q))*Cos(phi))/(4.*Power(k1,2))))/
      (7.*Power(q,2)*(Power(k2,2) + Power(q,2) + ((-Power(k1,2) - Power(k2,2) + Power(k3,2))*(Power(k1,2) - Power(p,2) + Power(q,2)) + 
             Sqrt((k1 + k2 - k3)*(k1 - k2 + k3)*(-k1 + k2 + k3)*(k1 + k2 + k3))*Sqrt((k1 + p - q)*(k1 - p + q)*(-k1 + p + q)*(k1 + p + q))*Cos(phi))/(2.*Power(k1,2)))))*
   (10./7 + (5*((-Power(k1,2) - Power(k2,2) + Power(k3,2))/2. - Power(q,2) + (Power(k1,2) - Power(p,2) + Power(q,2))/2. - 
          ((-Power(k1,2) - Power(k2,2) + Power(k3,2))*(Power(k1,2) - Power(p,2) + Power(q,2)) + 
             Sqrt((k1 + k2 - k3)*(k1 - k2 + k3)*(-k1 + k2 + k3)*(k1 + k2 + k3))*Sqrt((k1 + p - q)*(k1 - p + q)*(-k1 + p + q)*(k1 + p + q))*Cos(phi))/(4.*Power(k1,2))))/(7.*Power(p,2)) + 
     (5*((-Power(k1,2) - Power(k2,2) + Power(k3,2))/2. - Power(q,2) + (Power(k1,2) - Power(p,2) + Power(q,2))/2. - 
          ((-Power(k1,2) - Power(k2,2) + Power(k3,2))*(Power(k1,2) - Power(p,2) + Power(q,2)) + 
             Sqrt((k1 + k2 - k3)*(k1 - k2 + k3)*(-k1 + k2 + k3)*(k1 + k2 + k3))*Sqrt((k1 + p - q)*(k1 - p + q)*(-k1 + p + q)*(k1 + p + q))*Cos(phi))/(4.*Power(k1,2))))/
      (7.*(Power(k2,2) + Power(q,2) + ((-Power(k1,2) - Power(k2,2) + Power(k3,2))*(Power(k1,2) - Power(p,2) + Power(q,2)) + 
             Sqrt((k1 + k2 - k3)*(k1 - k2 + k3)*(-k1 + k2 + k3)*(k1 + k2 + k3))*Sqrt((k1 + p - q)*(k1 - p + q)*(-k1 + p + q)*(k1 + p + q))*Cos(phi))/(2.*Power(k1,2)))) + 
     (2*Power(k3,2)*((-Power(k1,2) - Power(k2,2) + Power(k3,2))/2. - Power(q,2) + (Power(k1,2) - Power(p,2) + Power(q,2))/2. - 
          ((-Power(k1,2) - Power(k2,2) + Power(k3,2))*(Power(k1,2) - Power(p,2) + Power(q,2)) + 
             Sqrt((k1 + k2 - k3)*(k1 - k2 + k3)*(-k1 + k2 + k3)*(k1 + k2 + k3))*Sqrt((k1 + p - q)*(k1 - p + q)*(-k1 + p + q)*(k1 + p + q))*Cos(phi))/(4.*Power(k1,2))))/
      (7.*Power(p,2)*(Power(k2,2) + Power(q,2) + ((-Power(k1,2) - Power(k2,2) + Power(k3,2))*(Power(k1,2) - Power(p,2) + Power(q,2)) + 
             Sqrt((k1 + k2 - k3)*(k1 - k2 + k3)*(-k1 + k2 + k3)*(k1 + k2 + k3))*Sqrt((k1 + p - q)*(k1 - p + q)*(-k1 + p + q)*(k1 + p + q))*Cos(phi))/(2.*Power(k1,2)))));
}


// =============================================================================


double sptintegration::kernel_B321_I(const double &q, const double &p, const double &k1, const double &k2, const double &k3)
{
    return ((2*Power(k1,4) - 5*Power(Power(p,2) - Power(q,2),2) + 3*Power(k1,2)*(Power(p,2) + Power(q,2)))*
     (-14*Power(k1,10) - 21*Power(k1,8)*(2*Power(k3,2) + 5*(Power(p,2) + Power(q,2))) - 
       3*Power(Power(k2,2) - Power(k3,2),2)*Power(Power(p,2) - Power(q,2),2)*(28*Power(k2,2) + 10*Power(k3,2) + 7*(Power(p,2) + Power(q,2))) + 
       2*Power(k1,6)*(-7*Power(k2,4) + 23*Power(k3,4) + 56*Power(Power(p,2) - Power(q,2),2) + 53*Power(k3,2)*(Power(p,2) + Power(q,2)) + Power(k2,2)*(68*Power(k3,2) + 161*(Power(p,2) + Power(q,2)))) - 
       8*Power(k1,5)*(2*Power(k3,4) + 5*Power(k3,2)*Power(p,2) - 7*Power(p,4))*Power(Power(k2,2) - Power(q,2),2)*
        Sqrt(1/(Power(k2,4)*Power(p,2) + Power(k1,2)*(Power(k3,2) - Power(p,2))*(Power(k2,2) - Power(q,2)) + Power(k3,2)*Power(q,2)*(Power(k3,2) - Power(p,2) + Power(q,2)) - 
            Power(k2,2)*(-Power(p,4) + Power(p,2)*Power(q,2) + Power(k3,2)*(Power(p,2) + Power(q,2))))) - 
       8*Power(k1,5)*Power(Power(k2,2) - Power(p,2),2)*(2*Power(k3,4) + 5*Power(k3,2)*Power(q,2) - 7*Power(q,4))*
        Sqrt(1/(Power(k2,4)*Power(q,2) + Power(k1,2)*(Power(k2,2) - Power(p,2))*(Power(k3,2) - Power(q,2)) + Power(k3,2)*Power(p,2)*(Power(k3,2) + Power(p,2) - Power(q,2)) - 
            Power(k2,2)*(Power(q,2)*(Power(p,2) - Power(q,2)) + Power(k3,2)*(Power(p,2) + Power(q,2))))) + 
       Power(k1,4)*(28*Power(k2,6) + 10*Power(k3,6) - 5*Power(k3,4)*(Power(p,2) + Power(q,2)) - 2*Power(k3,2)*(31*Power(p,4) - 74*Power(p,2)*Power(q,2) + 31*Power(q,4)) + 
          7*(Power(p,6) - 9*Power(p,4)*Power(q,2) - 9*Power(p,2)*Power(q,4) + Power(q,6)) - Power(k2,4)*(30*Power(k3,2) + 329*(Power(p,2) + Power(q,2))) + 
          Power(k2,2)*(24*Power(k3,4) + 22*Power(k3,2)*(Power(p,2) + Power(q,2)) - 56*(7*Power(p,4) - 17*Power(p,2)*Power(q,2) + 7*Power(q,4)))) + 
       2*Power(k1,2)*(28*Power(k2,6)*(Power(p,2) + Power(q,2)) + 10*Power(k3,6)*(Power(p,2) + Power(q,2)) + 7*Power(k3,2)*Power(Power(p,2) - Power(q,2),2)*(Power(p,2) + Power(q,2)) - 
          2*Power(k3,4)*(5*Power(p,4) - 24*Power(p,2)*Power(q,2) + 5*Power(q,4)) - 2*Power(k2,4)*(-91*Power(p,4) + 168*Power(p,2)*Power(q,2) - 91*Power(q,4) + 23*Power(k3,2)*(Power(p,2) + Power(q,2))) + 
          Power(k2,2)*(8*Power(k3,4)*(Power(p,2) + Power(q,2)) + 7*Power(Power(p,2) - Power(q,2),2)*(Power(p,2) + Power(q,2)) - 8*Power(k3,2)*(12*Power(p,4) - 17*Power(p,2)*Power(q,2) + 12*Power(q,4))))))/
   (18816.*Power(k1,4)*Power(k2,2)*Power(p,4)*Power(q,4));
}


// =============================================================================


double sptintegration::kernel_B321_II(const double &q, const double &k1, const double &k2, const double &k3)
{
    return ((((5*Power(k1,4) + 5*Power(k2,4) - 3*Power(k2,2)*Power(k3,2) - 2*Power(k3,4) - Power(k1,2)*(10*Power(k2,2) + 3*Power(k3,2)))*Pi*
     (4*k1*q*(-6*Power(k1,6) + 79*Power(k1,4)*Power(q,2) - 50*Power(k1,2)*Power(q,4) + 21*Power(q,6)) + 3*Power(Power(k1,2) - Power(q,2),3)*
      (2*Power(k1,2) + 7*Power(q,2))*Log(Power(k1 + q,2)/Power(k1 - q,2))))/(7056.*Power(k1,5)*Power(k2,2)*Power(q,5))) );
}


// =============================================================================


double sptintegration::kernel_B411(const double &q, const double &k1, const double &k2, const double &k3)
{
    double t1 = (Pi*(4*k1*k2*k3*q*(585*Power(k1,14)*Power(k2,6)*Power(k3,4) + 2475*Power(k2,6)*Power(k3,4)*Power(Power(k2,2) - Power(k3,2),3)*Power(q,8) - 
           15*Power(k1,2)*Power(k2,6)*Power(k3,4)*(Power(k2,2) - Power(k3,2))*Power(q,6)*(1949*Power(k2,4) + 20*Power(k3,4) - 459*Power(k3,2)*Power(q,2) + Power(k2,2)*(-1969*Power(k3,2) + 855*Power(q,2))) + 
           15*Power(k1,4)*Power(k2,6)*Power(k3,4)*Power(q,4)*(3895*Power(k2,6) + 606*Power(k3,6) + 444*Power(k3,4)*Power(q,2) + 405*Power(k3,2)*Power(q,4) + Power(k2,4)*(-7096*Power(k3,2) + 8694*Power(q,2)) + 
              Power(k2,2)*(2595*Power(k3,4) - 6566*Power(k3,2)*Power(q,2) + 471*Power(q,4))) - 
           15*Power(k1,12)*(90*Power(k2,8)*Power(k3,4) - 3895*Power(k2,4)*Power(k3,4)*Power(q,4) + 1949*Power(k2,2)*Power(k3,4)*Power(q,6) - 165*Power(k3,4)*Power(q,8) + 
              Power(k2,6)*(805*Power(k3,6) + 1822*Power(k3,4)*Power(q,2) - 210*Power(k3,2)*Power(q,4) + 189*Power(q,6))) + 
           Power(k1,6)*(585*Power(k2,14)*Power(k3,4) - 2475*Power(k3,10)*Power(q,8) + 15*Power(k2,2)*Power(k3,8)*Power(q,6)*(20*Power(k3,2) - 459*Power(q,2)) + 
              45*Power(k2,4)*Power(k3,6)*Power(q,4)*(202*Power(k3,4) + 148*Power(k3,2)*Power(q,2) + 135*Power(q,4)) - 
              15*Power(k2,12)*(805*Power(k3,6) + 1822*Power(k3,4)*Power(q,2) - 210*Power(k3,2)*Power(q,4) + 189*Power(q,6)) + 
              Power(k2,10)*(5655*Power(k3,8) + 222610*Power(k3,6)*Power(q,2) - 419226*Power(k3,4)*Power(q,4) + 54180*Power(k3,2)*Power(q,6) - 11340*Power(q,8)) + 
              2*Power(k2,6)*Power(k3,4)*(210*Power(k3,8) - 34455*Power(k3,6)*Power(q,2) + 4766*Power(k3,4)*Power(q,4) - 28605*Power(k3,2)*Power(q,6) + 5175*Power(q,8)) + 
              Power(k2,8)*Power(k3,2)*(5415*Power(k3,8) - 120370*Power(k3,6)*Power(q,2) + 250234*Power(k3,4)*Power(q,4) - 148950*Power(k3,2)*Power(q,6) + 7560*Power(q,8))) + 
           Power(k1,10)*(1530*Power(k2,10)*Power(k3,4) - 7425*Power(k3,6)*Power(q,8) + 15*Power(k2,8)*(1501*Power(k3,6) + 1318*Power(k3,4)*Power(q,2) - 210*Power(k3,2)*Power(q,4) + 189*Power(q,6)) - 
              30*Power(k2,4)*(3548*Power(k3,6)*Power(q,4) - 4347*Power(k3,4)*Power(q,6)) + 
              Power(k2,6)*(5655*Power(k3,8) + 222610*Power(k3,6)*Power(q,2) - 419226*Power(k3,4)*Power(q,4) + 54180*Power(k3,2)*Power(q,6) - 11340*Power(q,8)) + 
              45*Power(k2,2)*(1306*Power(k3,6)*Power(q,6) - 285*Power(k3,4)*Power(q,8))) + 
           Power(k1,8)*(-1350*Power(k2,12)*Power(k3,4) + 7425*Power(k3,8)*Power(q,8) + 15*Power(k2,10)*(1501*Power(k3,6) + 1318*Power(k3,4)*Power(q,2) - 210*Power(k3,2)*Power(q,4) + 189*Power(q,6)) + 
              Power(k2,6)*Power(k3,2)*(5415*Power(k3,8) - 120370*Power(k3,6)*Power(q,2) + 250234*Power(k3,4)*Power(q,4) - 148950*Power(k3,2)*Power(q,6) + 7560*Power(q,8)) + 
              2*Power(k2,8)*(7050*Power(k3,8) - 227830*Power(k3,6)*Power(q,2) + 370461*Power(k3,4)*Power(q,4) - 50400*Power(k3,2)*Power(q,6) + 11340*Power(q,8)) + 
              15*Power(k2,4)*(2595*Power(k3,8)*Power(q,4) - 6566*Power(k3,6)*Power(q,6) + 471*Power(k3,4)*Power(q,8)) - 135*Power(k2,2)*(221*Power(k3,8)*Power(q,6) - 146*Power(k3,6)*Power(q,8)))) - 
        15*Power(k2,7)*Power(k3,5)*Power(Power(k1,2) - Power(q,2),2)*(39*Power(k1,12) + 165*Power(Power(k2,2) - Power(k3,2),3)*Power(q,6) - Power(k1,10)*(582*Power(k2,2) + 791*Power(k3,2) + 1947*Power(q,2)) + 
           Power(k1,8)*(51*Power(k2,4) + 601*Power(k3,4) + 1787*Power(k3,2)*Power(q,2) + 1689*Power(q,4) + Power(k2,2)*(2120*Power(k3,2) + 6891*Power(q,2))) - 
           3*Power(k1,2)*(Power(k2,2) - Power(k3,2))*Power(q,4)*(558*Power(k2,4) + Power(k2,2)*(-473*Power(k3,2) + 285*Power(q,2)) - 17*(5*Power(k3,4) + 9*Power(k3,2)*Power(q,2))) + 
           Power(k1,6)*(492*Power(k2,6) + 151*Power(k3,6) + 179*Power(k3,4)*Power(q,2) - 1113*Power(k3,2)*Power(q,4) + 219*Power(q,6) - 3*Power(k2,4)*(211*Power(k3,2) + 2155*Power(q,2)) + 
              Power(k2,2)*(390*Power(k3,4) + 758*Power(k3,2)*Power(q,2) - 7284*Power(q,4))) + 
           Power(k1,4)*Power(q,2)*(1017*Power(k2,6) + 269*Power(k3,6) - 321*Power(k3,4)*Power(q,2) + 405*Power(k3,2)*Power(q,4) + Power(k2,4)*(-1677*Power(k3,2) + 7269*Power(q,2)) + 
              Power(k2,2)*(391*Power(k3,4) - 4376*Power(k3,2)*Power(q,2) + 471*Power(q,4))))*Log(Power(k1 + q,2)/Power(k1 - q,2)) - 
        15*Power(k1,7)*Power(k3,5)*Power(Power(k2,2) - Power(q,2),2)*(39*Power(k2,12) - 165*Power(k3,6)*Power(q,6) - Power(k2,10)*(791*Power(k3,2) + 1947*Power(q,2)) + 
           Power(k2,8)*(601*Power(k3,4) + 1787*Power(k3,2)*Power(q,2) + 1689*Power(q,4)) + 3*Power(k1,6)*(164*Power(k2,6) + 339*Power(k2,4)*Power(q,2) - 558*Power(k2,2)*Power(q,4) + 55*Power(q,6)) + 
           Power(k2,6)*(151*Power(k3,6) + 179*Power(k3,4)*Power(q,2) - 1113*Power(k3,2)*Power(q,4) + 219*Power(q,6)) + 
           Power(k2,4)*(269*Power(k3,6)*Power(q,2) - 321*Power(k3,4)*Power(q,4) + 405*Power(k3,2)*Power(q,6)) - 51*Power(k2,2)*(5*Power(k3,6)*Power(q,4) + 9*Power(k3,4)*Power(q,6)) + 
           3*Power(k1,4)*(17*Power(k2,8) - 165*Power(k3,2)*Power(q,6) - Power(k2,6)*(211*Power(k3,2) + 2155*Power(q,2)) + Power(k2,4)*(-559*Power(k3,2)*Power(q,2) + 2423*Power(q,4)) + 
              Power(k2,2)*(1031*Power(k3,2)*Power(q,4) - 285*Power(q,6))) + Power(k1,2)*
            (-582*Power(k2,10) + 495*Power(k3,4)*Power(q,6) + Power(k2,8)*(2120*Power(k3,2) + 6891*Power(q,2)) + Power(k2,6)*(390*Power(k3,4) + 758*Power(k3,2)*Power(q,2) - 7284*Power(q,4)) + 
              Power(k2,4)*(391*Power(k3,4)*Power(q,2) - 4376*Power(k3,2)*Power(q,4) + 471*Power(q,6)) - 6*Power(k2,2)*(194*Power(k3,4)*Power(q,4) - 219*Power(k3,2)*Power(q,6))))*
         Log(Power(k2 + q,2)/Power(k2 - q,2)) + 105*Power(k1,7)*Power(k2,7)*(2*Power(k3,4) + 7*Power(k3,2)*Power(q,2) - 9*Power(q,4))*
         (Power(k1,6)*(Power(k3,4) + 2*Power(k3,2)*Power(q,2) - 3*Power(q,4)) + Power(k2,6)*(Power(k3,4) + 2*Power(k3,2)*Power(q,2) - 3*Power(q,4)) + 
           4*Power(k2,4)*(4*Power(k3,6) - 14*Power(k3,4)*Power(q,2) + 13*Power(k3,2)*Power(q,4) - 3*Power(q,6)) - 2*Power(k3,4)*(Power(k3,6) + Power(k3,2)*Power(q,4) - 2*Power(q,6)) + 
           Power(k2,2)*(-15*Power(k3,8) + 46*Power(k3,6)*Power(q,2) - 47*Power(k3,4)*Power(q,4) + 8*Power(k3,2)*Power(q,6)) + 
           Power(k1,2)*(-15*Power(k3,8) + 46*Power(k3,6)*Power(q,2) - 47*Power(k3,4)*Power(q,4) + 8*Power(k3,2)*Power(q,6) + Power(k2,4)*(-9*Power(k3,4) - 2*Power(k3,2)*Power(q,2) + 3*Power(q,4)) - 
              8*Power(k2,2)*(4*Power(k3,6) - 17*Power(k3,4)*Power(q,2) + 12*Power(k3,2)*Power(q,4) - 3*Power(q,6))) + 
           Power(k1,4)*(Power(k2,2)*(-9*Power(k3,4) - 2*Power(k3,2)*Power(q,2) + 3*Power(q,4)) + 4*(4*Power(k3,6) - 14*Power(k3,4)*Power(q,2) + 13*Power(k3,2)*Power(q,4) - 3*Power(q,6))))*
         Log(Power(k3 + q,2)/Power(k3 - q,2))));
    double t2 = (3.10464e6*Power(k1,9)*Power(k2,9)*Power(k3,5)*Power(q,5));

    double t3 =  (Pi*((-7*Power(k1,4)*Power(Power(k2,2) - Power(q,2),2)*(-2*Power(k3,4) - 7*Power(k3,2)*Power(q,2) + 9*Power(q,4))*
           Log(Power(4*Power(k1,2)*Power(q,2) + 2*(Power(k2,2) - Power(q,2))*(Power(k3,2) - Power(q,2)) + 
               4*Sqrt(Power(k1,2)*Power(q,2)*(Power(k2,2)*Power(k3,2) + (Power(k1,2) - Power(k2,2) - Power(k3,2))*Power(q,2) + Power(q,4))),2)/
             Power(-4*Power(k1,2)*Power(q,2) + 2*(Power(k2,2) - Power(q,2))*(-Power(k3,2) + Power(q,2)) + 
               4*Sqrt(Power(k1,2)*Power(q,2)*(Power(k2,2)*Power(k3,2) + (Power(k1,2) - Power(k2,2) - Power(k3,2))*Power(q,2) + Power(q,4))),2)))/
         Sqrt(Power(k1,2)*Power(q,2)*(Power(k2,2)*(Power(k3,2) - Power(q,2)) + Power(q,2)*(Power(k1,2) - Power(k3,2) + Power(q,2)))) + 
        (36*Power(k3,4)*Power(Power(k1,2) - Power(q,2),2)*Power(Power(k2,2) - Power(q,2),2)*
           Log(Power(4*Power(k3,2)*Power(q,2) + 2*(Power(k1,2) - Power(q,2))*(Power(k2,2) - Power(q,2)) + 
               4*Sqrt(Power(k3,2)*Power(q,2)*(Power(k1,2)*Power(k2,2) - (Power(k1,2) + Power(k2,2) - Power(k3,2))*Power(q,2) + Power(q,4))),2)/
             Power(-4*Power(k3,2)*Power(q,2) + 2*(Power(k1,2) - Power(q,2))*(-Power(k2,2) + Power(q,2)) + 
               4*Sqrt(Power(k3,2)*Power(q,2)*(Power(k1,2)*Power(k2,2) - (Power(k1,2) + Power(k2,2) - Power(k3,2))*Power(q,2) + Power(q,4))),2)))/
         Sqrt(Power(k3,2)*Power(q,2)*(Power(k1,2)*(Power(k2,2) - Power(q,2)) + Power(q,2)*(-Power(k2,2) + Power(k3,2) + Power(q,2)))) - 
        (7*Power(k2,4)*Power(Power(k1,2) - Power(q,2),2)*(-2*Power(k3,4) - 7*Power(k3,2)*Power(q,2) + 9*Power(q,4))*
           Log(Power(4*Power(k2,2)*Power(q,2) + 2*(Power(k1,2) - Power(q,2))*(Power(k3,2) - Power(q,2)) + 
               4*Sqrt(Power(k2,2)*Power(q,2)*(Power(k1,2)*Power(k3,2) - (Power(k1,2) - Power(k2,2) + Power(k3,2))*Power(q,2) + Power(q,4))),2)/
             Power(-4*Power(k2,2)*Power(q,2) + 2*(Power(k1,2) - Power(q,2))*(-Power(k3,2) + Power(q,2)) + 
               4*Sqrt(Power(k2,2)*Power(q,2)*(Power(k1,2)*Power(k3,2) - (Power(k1,2) - Power(k2,2) + Power(k3,2))*Power(q,2) + Power(q,4))),2)))/
         Sqrt(Power(k2,2)*Power(q,2)*(Power(k1,2)*(Power(k3,2) - Power(q,2)) + Power(q,2)*(Power(k2,2) - Power(k3,2) + Power(q,2))))))/(51744.*Power(k1,2)*Power(k2,2)*Power(q,4));
        return (t1/t2 + t3);
}
