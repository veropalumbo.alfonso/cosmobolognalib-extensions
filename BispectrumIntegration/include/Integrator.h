#include "CBL.h"

// =============================================================================

#ifndef _INTEG_
#define _INTEG_

#define Power(a, b) pow(a, b)
#define Log(a) log(a)
#define Sqrt(a) sqrt(a)
#define Cos(a) cos(a)
#define Pi cbl::par::pi

namespace sptintegration {

    struct bispectrum_integral_inputs {

        // Object used to interpolate the power spectrum
        std::function<double(const std::vector<double> &)> integrand;
        
        // Minimum range of integration
        double qmin;

        // Maximum range of integration
        double qmax;
	
        // Minimum range of integration
        double log_qmin;

        // Maximum range of integration
        double log_qmax;

        // k1 value
        double k1;
        
        // k2 value
        double k2;

        // k2 value
        double k3;

	bool force_range;
    };


    // =============================================================================
    

    struct IntegrationResult {
        double Int;
        size_t nEvals;
        double error;
        int status;
    };


    // =============================================================================


    extern"C" 
    {
      void kernel_b222_ (double *_kernel, const double *_q, const double *_p, const double *_phi, const double *_k1, const double *_k2, const double *_k3);

      void kernel_b321_i_ (double *_kernel, const double *_q, const double *_p,  const double *_k3, const double *_k1, const double *_k2);

      void kernel_b321_ii_ (double *_kernel, const double *_q, const double *_k1, const double *_k2, const double *_k3);

      void kernel_b411_ (double *_kernel, const double *_qq, const double *_kk1, const double *_kk2, const double *_kk3);

      void kernel_b411_old_ (double *_kernel, const double *_q, const double *_k1, const double *_k2, const double *_k3);
    }

    // B222 kernel
    double kernel_B222 (const double &q, const double &p, const double &phi, const double &k1, const double &k2, const double &k3);

    // B321_I kernel
    double kernel_B321_I (const double &q, const double &p, const double &k3, const double &k1, const double &k2);

    // B321_II kernel
    double kernel_B321_II (const double &q, const double &k1, const double &k2, const double &k3);

    // B411 kernel
    double kernel_B411 (const double &q, const double &k1, const double &k2, const double &k3);

    // B411 kernel
    double kernel_B411_old (const double &q, const double &k1, const double &k2, const double &k3);


    // 3D function
    double integrand_B222 (const double &q, const double &p, const double &phi, const double &k1, const double &k2, const double &k3, const cbl::glob::FuncGrid &interp_Pk, const double &qmin, const double &qmax);
    int CUBA_integrand_B222 (const int *ndim, const cubareal xx[], const int *ncomp, cubareal ff[], void *userdata);
    
    // 2D function
    double integrand_B321_I (const double &q, const double &p, const double &k1, const double &k2, const double &k3, const cbl::glob::FuncGrid  &interp_Pk);
    int CUBA_integrand_B321_I (const int *ndim, const cubareal xx[], const int *ncomp, cubareal ff[], void *userdata);
    
    // 1D function
    double integrand_B321_II (const double &q, const double &k1, const double &k2, const double &k3, const cbl::glob::FuncGrid &interp_Pk);

    // 1D function
    double integrand_B411 (const double &q, const double &k1, const double &k2, const double &k3, const cbl::glob::FuncGrid &interp_Pk);

    IntegrationResult integrate_1D (cbl::FunctionDoubleDouble integrand, const double a, const double b, const double rel_err, const double abs_err, const size_t nsub);

    IntegrationResult integrate_B222 (const double &k1, const double &k2, const double &k3, const cbl::glob::FuncGrid &interp_Pk, const double a, const double b, const double rel_err, const double abs_err, const size_t max_evals, const bool force_range);

    IntegrationResult integrate_B321_I (const double &k1, const double &k2, const double &k3, const cbl::glob::FuncGrid &interp_Pk, const double a, const double b, const double rel_err, const double abs_err, const size_t max_evals, const bool force_range);

    IntegrationResult integrate_B321_II (const double &k1, const double &k2, const double &k3, const cbl::glob::FuncGrid &interp_Pk, const double a, const double b, const double rel_err, const double abs_err, const size_t nsub);

    IntegrationResult integrate_B411 (const double &k1, const double &k2, const double &k3, const cbl::glob::FuncGrid &interp_Pk, const double a, const double b, const double rel_err, const double abs_err, const size_t nsub);
    
    // B222 full result
    IntegrationResult full_B222 (const double &k1, const double &k2, const double &k3, const cbl::glob::FuncGrid &interp_Pk, const double a, const double b, const double rel_err, const double abs_err, const size_t max_evals, const bool force_range);

    // B321_I full result
    IntegrationResult full_B321_I (const double &k1, const double &k2, const double &k3, const cbl::glob::FuncGrid &interp_Pk, const double a, const double b, const double rel_err, const double abs_err, const size_t max_evals, const bool force_range);
 
    // B321_II full result
    IntegrationResult full_B321_II (const double &k1, const double &k2, const double &k3, const cbl::glob::FuncGrid &interp_Pk, const double a, const double b, const double rel_err, const double abs_err, const size_t nsub);

    // B321_II full resul
    IntegrationResult full_B411 (const double &k1, const double &k2, const double &k3, const cbl::glob::FuncGrid &interp_Pk, const double a, const double b, const double rel_err, const double abs_err, const size_t nsub);

    // B222 full result
    std::vector<IntegrationResult> full_B222 (const std::vector<double> &k1, const std::vector<double> &k2, const std::vector<double> &k3, const cbl::glob::FuncGrid &interp_Pk, const double a, const double b, const double rel_err, const double abs_err, const size_t max_evals, const bool force_range);
 
    // B321_I full result
    std::vector<IntegrationResult> full_B321_I (const std::vector<double> &k1, const std::vector<double> &k2, const std::vector<double> &k3, const cbl::glob::FuncGrid &interp_Pk, const double a, const double b, const double rel_err, const double abs_err, const size_t max_evals, const bool force_range);
 
    // B321_II full result
    std::vector<IntegrationResult> full_B321_II (const std::vector<double> &k1, const std::vector<double> &k2, const std::vector<double> &k3, const cbl::glob::FuncGrid &interp_Pk, const double a, const double b, const double rel_err, const double abs_err, const size_t nsub);

    // B411 full resul
    std::vector<IntegrationResult> full_B411 (const std::vector<double> &k1, const std::vector<double> &k2, const std::vector<double> &k3, const cbl::glob::FuncGrid &interp_Pk, const double a, const double b, const double rel_err, const double abs_err, const size_t nsub);

}

#endif
