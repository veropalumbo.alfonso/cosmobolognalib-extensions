subroutine kernel_B411 (kernel, qq, kk1, kk2, kk3)
  use mpmodule
  implicit none
  integer ndp, nwds
  parameter (ndp = 100, nwds = int(ndp/mpdpw + 2))
  real *8, intent(in):: qq, kk1, kk2, kk3
  real *8 cc
  parameter (cc = 3.10464e6)
  type (mp_real) ::pi
  type (mp_real) :: q, k1, k2, k3, cnst1
  real*8, intent(out)  :: kernel
  
  pi = mppi(nwds)
  q = mpreal(qq, nwds)
  k1 = mpreal(kk1, nwds)
  k2 = mpreal(kk2, nwds)
  k3 = mpreal(kk3, nwds)

  cnst1 = mpreal(cc, nwds)
  kernel = 2475 * k2**6 * k3**4 *(k2**2 - k3**2)**3 * q**8
  return 
end subroutine kernel_B411

subroutine kernel_B411_old (kernel, q, k1, k2, k3)
  implicit none
  real*8, intent(in)   :: q, k1, k2, k3
  real*8, intent(out)  :: kernel
  real*8 :: pi
  parameter (pi=3.141592653589793238462643383279502884197d0)

  kernel = 2475 * k2**6 * k3**4 *(k2**2 - k3**2)**3 * q**8
  return

end subroutine kernel_B411_old

program test
implicit none

real *8 q,k1,k2,k3,kernel1, kernel2

k1 = 0.00001
k2 = 0.00002
k3 = 0.00003
q = 10

call kernel_B411_old(kernel2, q, k1, k2, k3)
call kernel_B411(kernel1, q, k1, k2, k3)

write(*,*) kernel1
write(*,*) kernel2

end program test
