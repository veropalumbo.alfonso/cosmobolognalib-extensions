// SWIG Interface to BispectrumIntegrator

%module(directors="1") bispectrumintegrator

%include "stl.i"
%include "stdint.i"
%include "std_string.i"
%include "std_vector.i"
%include <std_shared_ptr.i>

%shared_ptr(cbl::glob::FuncGrid);

%import (module="CosmoBolognaLib") "FuncGrid.h"

%{
#include "Integrator.h"
%}

%include "Integrator.h"

%template(IntegrationResultsVector) std::vector<sptintegration::IntegrationResult>;


