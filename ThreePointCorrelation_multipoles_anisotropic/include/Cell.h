#ifndef __CELL__
#define __CELL__ 


#include "CBL.h"

namespace cbl{
  namespace glob{

    struct Particle {
      size_t index;
      Vector4D coordinates;
      double weight;
    };

    class Cell {
      private:

	Vector4D m_center;

	double m_deltaX;

	double m_deltaY;

	double m_deltaZ;
	
	std::vector<Particle> m_particles;

	size_t m_nParticles = 0;

      public:

	Cell () {}

	~Cell () = default;
	
	Cell (const double &xmin, const double &xmax, const double &ymin, const double &ymax, const double &zmin, const double &zmax)
	{set (xmin, xmax, ymin, ymax, zmin, zmax);}

	void set(const double &xmin, const double &xmax, const double &ymin, const double &ymax, const double &zmin, const double &zmax);

	void set_particle (const size_t index, const cbl::Vector4D &coordinates, const double &weight);

	Particle particle (const size_t &i);

	std::vector<Particle> particles () {return m_particles;}

	size_t nParticles () {return m_particles.size(); }

	bool isEmpty () { return (nParticles() != 0) ? false : true; }
    };
  }
}

#endif
