#ifndef _GRID_
#define _GRID_

#include "Cell.h"

namespace cbl{
  namespace glob{
    class Grid {
      private:

	bool m_isPeriodic;

	double m_xBinSize;

	double m_yBinSize;

	double m_zBinSize;

	int m_xMaxSep;

	int m_yMaxSep;

	int m_zMaxSep;

	double m_xmin;

	double m_xmax;

	double m_ymin;

	double m_ymax;

	double m_zmin;

	double m_zmax;

	int m_nX;

	int m_nY;

	int m_nZ;

	std::vector<Cell> m_cells;

	void m_get_neighbours_periodic (std::vector<long int> &indeces, std::vector<Vector4D> &separations, const int &ic, const int &jc, const int &kc);

	void m_get_neighbours_nonperiodic (std::vector<long int> &indeces, std::vector<Vector4D> &separations, const int &ic, const int &jc, const int &kc);

      public:

	Grid () {}

	~Grid () = default;

	Grid (const double &rMAX, const double &xmin, const double &xmax, const double &ymin, const double &ymax, const double &zmin, const double &zmax, const int &nBins, const bool &isPeriodic) { set(rMAX, xmin, xmax, ymin, ymax, zmin, zmax, nBins, isPeriodic); }

	void set (const double &rMAX, const double &xmin, const double &xmax, const double &ymin, const double &ymax, const double &zmin, const double &zmax, const int &nBins, const bool &isPeriodic);

	void set_catalogue (const catalogue::Catalogue &catalogue);

	size_t nCells() {return m_cells.size();}

	bool isCellEmpty (const long int i) {return m_cells[i].isEmpty();}

	std::vector<Particle> get_cell_particles(const long int &cell_index) {return m_cells[cell_index].particles(); }

	std::vector<Particle> get_neighbours(const long int &cell_index);
    };
  }
}


#endif
