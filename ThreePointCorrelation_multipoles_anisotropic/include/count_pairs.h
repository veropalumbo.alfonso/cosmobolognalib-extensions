#include "Grid.h"

std::vector<double> count_pairs(const double rMin, const double rMax, const double binSize, const cbl::catalogue::Catalogue catalogue, const int nC, const bool isPeriodic, const double min=0, const double max=1500)
{ 
  // timer 
  time_t start; time(&start);
  int dp = std::cout.precision();
  std::cout.setf(std::ios::fixed); std::cout.setf(std::ios::showpoint); std::cout.precision(2);

  //Set the pairs
  const int nBins = static_cast<int>((rMax-rMin)/binSize);
  std::vector<double> pairs(nBins, 0);

  cbl::glob::Grid grid(rMax, min, max, min, max, min, max, nC, isPeriodic);
  grid.set_catalogue(catalogue);

  // factor used by the timer
  float fact_count = 100./grid.nCells();

  int nCells = grid.nCells();

  // thread number
  int tid = 0;
  double nPart = 0;
 
#pragma omp parallel num_threads(omp_get_max_threads()) private(tid)
  {
    tid = omp_get_thread_num();
    std::vector<double> _pairs(nBins);
    double _npart = 0;

    // parallelized loop
#pragma omp for schedule(dynamic)

    for (size_t i=0; i<grid.nCells(); i++) {
      if (grid.isCellEmpty(i))
	continue;

      std::vector<cbl::glob::Particle> particles = grid.get_cell_particles(i);

      std::vector<cbl::glob::Particle> neighbours = grid.get_neighbours(i);

      for (size_t p1 = 0; p1<particles.size(); p1++) {
	for (size_t p2 = 0; p2<neighbours.size(); p2++) {
	  double dist = (neighbours[p2].coordinates - particles[p1].coordinates).norm();

	  if (dist > rMax or dist < rMin or particles[p1].index >= neighbours[p2].index) {
	    _npart +=1;
	    continue;
	  }

	  int bin = std::max(0, std::min(int((dist-rMin)/binSize), nBins));
	  _pairs[bin] += particles[p1].weight * neighbours[p2].weight;
	}
      }

      // estimate the computational time and update the time count
      time_t end_temp; time(&end_temp); double diff_temp = difftime(end_temp, start);
      if (tid==0) { std::cout << "\r" << float(i)*fact_count << "% completed (" << diff_temp << " seconds)\r"; std::cout.flush(); }    
      if (i==size_t(nCells*0.25)) std::cout << ".............25% completed" << std::endl;
      if (i==size_t(nCells*0.5)) std::cout << ".............50% completed" << std::endl;
      if (i==size_t(nCells*0.75)) std::cout << ".............75% completed"<< std::endl;   
    }
#pragma omp critical
    {
      // sum all the object pairs computed by each thread
      for (int i=0; i<nBins; i++)
	pairs[i] += _pairs[i];
      nPart += _npart;
    }
  }
  std::cout << std::endl <<  "Done!           " << std::endl;
  std::cout.precision(dp);
  std::cout << nPart << std::endl;

  return pairs;
}
