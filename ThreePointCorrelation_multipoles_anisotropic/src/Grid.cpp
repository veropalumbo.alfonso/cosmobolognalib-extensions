#include "Grid.h"

using namespace cbl;
using namespace std;


// ============================================================================================


void cbl::glob::Grid::set (const double &rMAX, const double &xmin, const double &xmax, const double &ymin, const double &ymax, const double &zmin, const double &zmax, const int &nBins, const bool &isPeriodic)
{
  m_isPeriodic = isPeriodic;
  
  m_xBinSize = (xmax-xmin)/nBins;
  m_yBinSize = (ymax-ymin)/nBins;
  m_zBinSize = (zmax-zmin)/nBins;
  m_xMaxSep = ceil(rMAX*1.01/m_xBinSize);
  m_yMaxSep = ceil(rMAX*1.01/m_yBinSize);
  m_zMaxSep = ceil(rMAX*1.01/m_zBinSize);

  m_xmin = xmin;
  m_xmax = xmax;
  m_ymin = ymin;
  m_ymax = ymax;
  m_zmin = zmin;
  m_zmax = zmax;
    
  m_nX = static_cast<int>((xmax-xmin)/m_xBinSize);
  m_nY = static_cast<int>((ymax-ymin)/m_yBinSize);
  m_nZ = static_cast<int>((zmax-zmin)/m_zBinSize);

  m_cells.erase(m_cells.begin(), m_cells.end());

  cout <<"Creating Cells" << endl;

  for (int i=0; i<m_nX; i++)
    for (int j=0; j<m_nY; j++)
      for (int k=0; k<m_nZ; k++){
	m_cells.push_back(Cell(m_xmin + i*m_xBinSize, m_xmin+(i+1)*m_xBinSize, m_ymin+j*m_yBinSize, m_ymin+(j+1)*m_yBinSize, m_zmin+k*m_zBinSize, m_zmin+(k+1)*m_zBinSize));
      }
  cout <<"Done!" << endl;
}


// ============================================================================================


void cbl::glob::Grid::set_catalogue(const cbl::catalogue::Catalogue &catalogue)
{
  for (size_t i=0; i<catalogue.nObjects(); i++) {
    size_t ii = static_cast<int>( (catalogue.xx(i)-m_xmin) / m_xBinSize);
    size_t jj = static_cast<int>( (catalogue.yy(i)-m_ymin) / m_yBinSize);
    size_t kk = static_cast<int>( (catalogue.zz(i)-m_zmin) / m_zBinSize);
    m_cells[kk + m_nZ*jj + m_nY*m_nZ*ii].set_particle(i, catalogue.eigen_coordinate(i), catalogue.weight(i));
  }
}


// ============================================================================================


void cbl::glob::Grid::m_get_neighbours_periodic (vector<long int> &indeces, vector<Vector4D> &separations, const int &ic, const int &jc, const int &kc)
{
  indeces.erase(indeces.begin(), indeces.end());
  separations.erase(separations.begin(), separations.end());

  for (int i=-m_xMaxSep; i<m_xMaxSep+1; i++){

    //Periodicity along x-axis
    int icell = i+ic;
    icell = (icell>=m_nX) ? icell - m_nX : icell;
    icell = (icell<0) ? m_nX+icell : icell;

    for (int j=-m_yMaxSep; j<m_yMaxSep+1; j++) {

      //Periodicity along y-axis
      int jcell = j+jc;
      jcell = (jcell>=m_nY) ? jcell - m_nY : jcell;
      jcell = (jcell<0) ? m_nY+jcell : jcell;

      for (int k=-m_zMaxSep; k<m_zMaxSep+1; k++){
	//Periodicity along z-axis
	int kcell = k+kc;
	kcell = (kcell>=m_nZ) ? kcell - m_nZ : kcell;
	kcell = (kcell<0) ? m_nZ+kcell : kcell;

	long int index = kcell + jcell*m_nZ + icell * m_nY * m_nZ;

	if (m_cells[index].isEmpty())
	  continue;

	indeces.push_back(index);
	Vector4D sep;
	sep << (i)*m_xBinSize, (j)*m_yBinSize, (k)*m_zBinSize, 0;
	separations.emplace_back( sep );
      }
    }
  }
}


// ============================================================================================


void cbl::glob::Grid::m_get_neighbours_nonperiodic (vector<long int> &indeces, vector<Vector4D> &separations, const int &ic, const int &jc, const int &kc)
{
  indeces.erase(indeces.begin(), indeces.end());
  separations.erase(separations.begin(), separations.end());
  for (int i=-m_xMaxSep; i<m_xMaxSep+1; i++){

    //Non-Periodicity along x-axis
    int icell = i+ic;

    if (icell<0 or icell>=m_nX)
      continue;

    for (int j=-m_yMaxSep; j<m_yMaxSep+1; j++) {

      //Non-Periodicity along y-axis
      int jcell = j+jc;
      if (jcell<0 or jcell>=m_nY)
	continue;

      for (int k=-m_zMaxSep; k<m_zMaxSep+1; k++){
	//Periodicity along z-axis
	int kcell = k+kc;

	if (kcell<0 or kcell>=m_nZ)
	  continue;

	long int index = kcell + jcell*m_nZ + icell * m_nY * m_nZ;
	if (m_cells[index].isEmpty())
	  continue;

	indeces.push_back(index);
	Vector4D sep;
	sep <<  (i)*m_xBinSize, (j)*m_yBinSize, (k)*m_zBinSize, 0;
	separations.emplace_back( sep );
      }
    }
  }
}


// ============================================================================================


vector<glob::Particle> cbl::glob::Grid::get_neighbours(const long int &cell_index)
{
  const int ic = floor (cell_index/(m_nY * m_nZ));
  const int jc = floor ((cell_index - ic*m_nY*m_nZ) /(m_nZ));
  const int kc = cell_index - jc * m_nZ - ic * m_nY * m_nZ;

  vector<long int> indeces;
  vector<Vector4D> separations;

  if (m_isPeriodic)
    m_get_neighbours_periodic(indeces, separations, ic, jc, kc);
  else
    m_get_neighbours_nonperiodic(indeces, separations, ic, jc, kc);

  //count particles
  size_t nParticles = 0;
  for (size_t i=0; i<indeces.size(); i++)
    nParticles += m_cells[indeces[i]].nParticles();

  vector<Particle> particles(nParticles);

  int n=0;
  for (size_t i=0; i<indeces.size(); i++) 
    for (size_t j=0; j< m_cells[indeces[i]].nParticles(); j++) {
      particles[n] = m_cells[indeces[i]].particle(j);
      particles[n].coordinates += separations[i];
      n++;
    }

  return particles;
}
