#include "CBL.h"
#include "ThreePointCorrelation_multipoles_anisotropic.h"

// these two variables contain the name of the CosmoBolognaLib
// directory and the name of the current directory (useful when
// launching the code on remote systems)
std::string cbl::par::DirCosmo = DIRCOSMO, cbl::par::DirLoc = DIRL;


int main() {
  
    // --------------------------------------------------------------
    // ---------------- set the cosmological parameters  ------------
    // --------------------------------------------------------------

    cbl::cosmology::Cosmology cosmology {cbl::cosmology::CosmologicalModel::_Planck15_};

  
    // -----------------------------------------------------------------------------------------------------------
    // ---------------- read the input catalogue (with observed coordinates: R.A., Dec, redshift) ----------------
    // -----------------------------------------------------------------------------------------------------------
  
    std::string file_catalogue = cbl::par::DirLoc+"input/cat.dat";

    cbl::catalogue::Catalogue catalogue {cbl::catalogue::ObjectType::_Galaxy_, cbl::CoordinateType::_observed_, {file_catalogue}, cosmology};
    catalogue.computePolarCoordinates();

    // --------------------------------------------------------------------------------------
    // ---------------- construct the random catalogue (with cubic geometry) ----------------
    // --------------------------------------------------------------------------------------

    const double N_R = 1.; // random/data ratio
   
    cbl::catalogue::Catalogue random_catalogue {cbl::catalogue::RandomType::_createRandom_box_, catalogue, N_R};

    // -------------------------------------------------------------------------------
    // ---------------- measure the three-point correlation functions ----------------
    // -------------------------------------------------------------------------------

    // binning parameters

    const double rMin = 0.;  
    const double rMax = 30.;
    const double binSize = 2;
    const int nOrders = 11;

    // output data
  
    const std::string dir_output = cbl::par::DirLoc+"output/";
    const std::string dir_triplets = cbl::par::DirLoc+"output/triplets/";
    const std::string file_output = "zeta_multipoles.dat";

    auto ThreeP = cbl::measure::threept::ThreePointCorrelation_multipoles_anisotropic(catalogue, random_catalogue,
        rMin, rMax, binSize, nOrders);

    ThreeP.measure(cbl::measure::ErrorType::_None_, dir_triplets);

    return 0;
}
