/********************************************************************
 *  Copyright (C) 2015 by Federico Marulli and Alfonso Veropalumbo  *
 *  federico.marulli3@unibo.it                                      *
 *                                                                  *
 *  This program is free software; you can redistribute it and/or   * 
 *  modify it under the terms of the GNU General Public License as  *
 *  published by the Free Software Foundation; either version 2 of  *
 *  the License, or (at your option) any later version.             *
 *                                                                  *
 *  This program is distributed in the hope that it will be useful, *
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of  *
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the   *
 *  GNU General Public License for more details.                    *
 *                                                                  *
 *  You should have received a copy of the GNU General Public       *
 *  License along with this program; if not, write to the Free      *
 *  Software Foundation, Inc.,                                      *
 *  59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.       *
 ********************************************************************/

/**
 *  @file CosmoBolognaLib/Func/SphericalHarmonics_Coefficients.cpp
 *
 *  @brief Methods of the class
 *  SphericalHarmonics_Coefficients_improved used to compute the spherical
 *  harmonics coefficients at a given position on
 *
 *  This file contains the implementation of the methods of the class
 *  SphericalHarmonics_Coefficients_improved used to measure the
 *  spherical harmonics coefficients at a given position on the unit sphere
 *
 *  @authors Federico Marulli, Alfonso Veropalumbo
 *
 *  @authors federico.marulli3@unibo.it, alfonso.veropalumbo@unibo.it
 */

#include "Cell.h"

using namespace std;
using namespace cbl;


// ============================================================================================


void cbl::glob::Cell::set (const double &xmin, const double &xmax, const double &ymin, const double &ymax, const double &zmin, const double &zmax)
{
  m_center << (xmax+xmin)/2, (ymax+ymin)/2, (zmax+zmin)/2, 0;
  m_deltaX = xmax-xmin;
  m_deltaY = ymax-ymin;
  m_deltaZ = zmax-zmin;

  m_particles.erase(m_particles.begin(), m_particles.end());
}


// ============================================================================================


void cbl::glob::Cell::set_particle (const size_t index, const cbl::Vector4D &coordinates, const double &weight)
{
  Particle part;
  part.coordinates = coordinates-m_center;
  part.weight = weight;
  part.index = index;
  m_particles.push_back(part);
}


// ============================================================================================


cbl::glob::Particle cbl::glob::Cell::particle (const size_t &i)
{
  return m_particles[i];
}
