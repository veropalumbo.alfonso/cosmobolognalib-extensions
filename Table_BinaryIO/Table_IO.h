#include "Table.h"

namespace cbl {

  namespace data {

    /**
     *  @class Table_BinaryIO Table_BinaryIO.h
     *  "Table_BinaryIO.h"
     *
     *  @brief The class Table_BinaryIO
     *
     *  This is a wrapper of cbl::data::Table class
     *  intended to work with input/output binary files
     */
      class Table_BinaryIO : protected cbl::data::Table
      {
          public:

              /*
               * @brief constructor
               *
               * default constructor
               * @return object of type Table_BinaryIO
               */
              Table_BinaryIO () {}

              /**
               * @brief Constructor of object Table_BinaryIO
               *
               * This constructor initializes the table, creating empty
               * columns of size nrows
               *
               * @param names the column names
               *
               * @param nrows the columns lenght
               *
               * @return object of type Table_BinaryIO
               */
              Table_BinaryIO (const std::vector<std::string> names, const size_t nrows) : cbl::data::Table(names, nrows) {}

              /**
               * @brief Constructor of object Table_BinaryIO
               *
               * This constructor initializes the table, taking in input the
               * name of the columns and the values. The size of the two
               * arrays must be tha same.
               *
               * @param names the column names
               *
               * @param values the table values
               *
               * @return object of type Table_BinaryIO
               */
              Table_BinaryIO (const std::vector<std::string> names, const std::vector<std::vector<double>> values) : cbl::data::Table(names, values) {}

              /*
               * @brief constructor
               *
               * This constructor instatiate an object of type
               * Table_BinaryIO filling its content from a binary file.
               *
               * @return object of type Table_BinaryIO
               */
              Table_BinaryIO (const std::string binary_file) { read_binary(binary_file); }

              /*
               * @brief destructor
               *
               * default destructor
               */
              ~Table_BinaryIO () = default;

              /*
               * @brief reset the table
               *
               * This function wipes all the table content.
               *
               * @return none
               */
              void reset() { m_table.clear(); }

              /*
               * @brief read the table from a binary file
               *
               * This function replaces the current content of the table
               * by reading from a binary table
               *
               * @param binary_file file name where the table is read
               *
               * @return none
               */
              void read_binary(const std::string binary_file);
              
              /*
               * @brief write the table on a binary file
               *
               * This function writes the current content of the table
               * on a binary file
               *
               * @param binary_file file name where the table is stored
               *
               * @return none
               */
              void write_binary(const std::string binary_file);
              
              /*
               * @brief return the table keys
               *
               * @return vector containing the list of keys
               */
              std::vector<std::string> keys() { return m_keys(); }

              /*
               * @brief check if the table content matches 
               * the one from another table, in input
               *
               * @param table input table
               *
               * @return none
               */
              void checkEqual(Table_BinaryIO table);
      };


// =======================================================================


      void Table_BinaryIO::read_binary(const std::string binary_file)
      {
          // Reset the table
          reset();

          // Open file
          std::ifstream ifbin (binary_file, std::ios::in | std::ifstream::binary);

          // Read first dimension and resize
          size_t s1 = 0;
          ifbin.read(reinterpret_cast<char *>(&s1), sizeof(size_t));
          std::vector<std::string> keys(s1, " "); 
          std::vector<std::vector<double>> values(s1);

          for (size_t i=0; i<s1; i++)
          {
              // Read key lenght and chars
              size_t key_size;
              ifbin.read(reinterpret_cast<char *>(&key_size), sizeof(size_t));
              keys[i].resize(key_size);
              ifbin.read(reinterpret_cast<char *>(&keys[i][0]), key_size);

              // Read column lenght
              size_t s2 = 0;
              ifbin.read(reinterpret_cast<char *>(&s2), sizeof(size_t));

              values[i].resize(s2);

              // Read column
              ifbin.read(reinterpret_cast<char *>(&values[i][0]), s2*sizeof(double));
          }

          ifbin.close();

          m_set(keys, values);
      }


// =======================================================================


      void Table_BinaryIO::write_binary(const std::string binary_file)
      {
          // Save matrix
          std::ofstream ofbin (binary_file, std::ios::out | std::ofstream::binary);

          std::vector<std::string> keys = m_keys();
          size_t ncols = keys.size(); 

          //// Write the number of columns
          ofbin.write(reinterpret_cast<const char *>(&ncols), sizeof(size_t));

          for (size_t i=0; i<ncols; i++) {
              // write the name of the i-th column (string size and chars)
              size_t key_size = keys[i].size();
              ofbin.write(reinterpret_cast<const char *>(&key_size), sizeof(size_t));
              ofbin.write(keys[i].c_str(), key_size);

              // write the column lenght
              size_t col_lenght = m_table[keys[i]].size();
              ofbin.write(reinterpret_cast<const char *>(&col_lenght), sizeof(size_t));

              // write the column
              ofbin.write(reinterpret_cast<const char *>(&m_table[keys[i]][0]), col_lenght*sizeof(double));
          }

          ofbin.close();
      }


// =======================================================================
      

      void Table_BinaryIO::checkEqual(Table_BinaryIO table) {
          cbl::checkEqual(keys(), table.keys());
          for (size_t i=0; i<keys().size(); i++)
              cbl::checkEqual(m_table[keys()[i]], table[keys()[i]]);
      }
  }
}
