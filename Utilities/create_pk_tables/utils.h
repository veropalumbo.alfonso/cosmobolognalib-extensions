double gauss_legendre_integration(std::function< double(double) > integrand, const size_t nMu)
{
  double mu, weights;
  gsl_integration_glfixed_table *table =  gsl_integration_glfixed_table_alloc(nMu);

  double Int = 0;

  for (size_t i=0; i<nMu; i++) {
    gsl_integration_glfixed_point(-1, 1, i, &mu, &weights, table);
    Int += weights * integrand(mu);
  }

  gsl_integration_glfixed_table_free(table);
  return Int;
}


// ====================================================================================================


double get_sigma_squared(const cbl::glob::FuncGrid pk_nw_interp, const double kmin, const double losc = 110, const double ks = 0.2) 
{
    auto integrand = [&] (const double q) {
        return pk_nw_interp(q) * ( 1 - cbl::j0(q*losc) + 2*cbl::j2(q*losc) );
    };

    return cbl::wrapper::gsl::GSL_integrate_cquad(integrand, kmin, ks, 1.e-5)/(6*pow(cbl::par::pi, 2));
}


// ====================================================================================================


double model_zeta_term_0 (const double r12, const double r13, const double mu23, const std::shared_ptr<cbl::glob::FuncGrid> interp_xi0)
{
    double r23 = sqrt(r12*r12 + r13*r13 - 2 * mu23 * r12 * r13);
    double xi12 =  interp_xi0->operator()(r12);
    double xi13 =  interp_xi0->operator()(r13);
    double xi23 =  interp_xi0->operator()(r23);

    return xi12*xi13 + xi12*xi23 + xi13*xi23;
}


// ====================================================================================================


double model_zeta_ell_term_0 (const double r12, const double r13, const int ell, const std::shared_ptr<cbl::glob::FuncGrid> interp_xi0) 
{
    auto integrand = [&] (const double mu) {return model_zeta_term_0(r12, r13, mu, interp_xi0) * cbl::legendre_polynomial(mu, ell); };

    return gauss_legendre_integration(integrand, 31) * (2*ell+1) / 2;
}


// ====================================================================================================


double model_zeta_ell_term_0 (const double r12_min, const double r12_max, const double r13_min, const double r13_max, const int ell, const std::shared_ptr<cbl::glob::FuncGrid> interp_xi0) 
{
    double V1 = ( pow(r12_max, 3) - pow(r12_min, 3)) / 3;
    double V2 = ( pow(r13_max, 3) - pow(r13_min, 3)) / 3;

    auto integrand = [&] (const std::vector<double> xx) {
        double r12 = pow(xx[0], 1./3);
        double r13 = pow(xx[1], 1./3);
        double mu = xx[2];
        return model_zeta_term_0(r12, r13, mu, interp_xi0) * cbl::legendre_polynomial(mu, ell); 
    };

    size_t ndim = 3;
    std::vector<std::vector<double>> limits = {{pow(r12_min, 3), pow(r12_max, 3)}, {pow(r13_min, 3), pow(r13_max, 3)}, {-1, 1}};

    cbl::wrapper::cuba::CUBAwrapper CW(integrand, ndim);
    CW.inputs().EPSREL=1.e-6;


    return CW.IntegrateCuhre(limits) * (2*ell+1) / 2 / (9 * V1 * V2);
}


// ====================================================================================================


double model_zeta_term_1 (const double r12, const double r13, const double mu23, const std::shared_ptr<cbl::glob::FuncGrid> interp_xi1m, const std::shared_ptr<cbl::glob::FuncGrid> interp_xi1p)
{
    double r23 = sqrt(r12*r12 + r13*r13 - 2 * mu23 * r12 * r13);
    
    double mu12 = (r23*r23 + r13*r13 - r12*r12) / ( 2*r13*r23);
    double mu13 = (r23*r23 + r12*r12 - r13*r13) / ( 2*r12*r23);
    
    double xim12 = interp_xi1m->operator()(r12), xip12 = interp_xi1p->operator()(r12);
    double xim13 = interp_xi1m->operator()(r13), xip13 = interp_xi1p->operator()(r13);
    double xim23 = interp_xi1m->operator()(r23), xip23 = interp_xi1p->operator()(r23);

    return (xim12*xip13 + xim13*xip12) * mu23 +  (xim12*xip23 + xim23*xip12) * mu13 + (xim23*xip13 + xim13*xip23) * mu12;
}


// ====================================================================================================


double model_zeta_ell_term_1 (const double r12, const double r13, const int ell, const std::shared_ptr<cbl::glob::FuncGrid> interp_xi1m, const std::shared_ptr<cbl::glob::FuncGrid> interp_xi1p) 
{
    auto integrand = [&] (const double mu) {return model_zeta_term_1(r12, r13, mu, interp_xi1m, interp_xi1p) * cbl::legendre_polynomial(mu, ell); };

    return gauss_legendre_integration(integrand, 31) * (2*ell+1) / 2;
}


// ====================================================================================================


double model_zeta_ell_term_1 (const double r12_min, const double r12_max, const double r13_min, const double r13_max, const int ell, const std::shared_ptr<cbl::glob::FuncGrid> interp_xi1m, const std::shared_ptr<cbl::glob::FuncGrid> interp_xi1p) 
{
    double V1 = ( pow(r12_max, 3) - pow(r12_min, 3)) / 3;
    double V2 = ( pow(r13_max, 3) - pow(r13_min, 3)) / 3;

    auto integrand = [&] (const std::vector<double> xx) {
        double r12 = pow(xx[0], 1./3);
        double r13 = pow(xx[1], 1./3);
        double mu = xx[2];
        return model_zeta_term_1(r12, r13, mu, interp_xi1m, interp_xi1p) * cbl::legendre_polynomial(mu, ell); 
    };

    size_t ndim = 3;
    std::vector<std::vector<double>> limits = {{pow(r12_min, 3), pow(r12_max, 3)}, {pow(r13_min, 3), pow(r13_max, 3)}, {-1, 1}};

    cbl::wrapper::cuba::CUBAwrapper CW(integrand, ndim);
    CW.inputs().EPSREL=1.e-6;

    return CW.IntegrateCuhre(limits) * (2*ell+1) / 2 /(9. * V1 * V2);
}


// ====================================================================================================


double model_zeta_term_2 (const double r12, const double r13, const double mu23, const std::shared_ptr<cbl::glob::FuncGrid> interp_xi2)
{
    double r23 = sqrt(r12*r12 + r13*r13 - 2 * mu23 * r12 * r13);
    
    double mu12 = (r23*r23 + r13*r13 - r12*r12) / ( 2*r13*r23);
    double mu13 = (r23*r23 + r12*r12 - r13*r13) / ( 2*r12*r23);

    double P_12 = 0.5 * (3 * pow(mu12, 2) - 1);
    double P_13 = 0.5 * (3 * pow(mu13, 2) - 1);
    double P_23 = 0.5 * (3 * pow(mu23, 2) - 1);
    
    double xi12 =  interp_xi2->operator()(r12);
    double xi13 =  interp_xi2->operator()(r13);
    double xi23 =  interp_xi2->operator()(r23);

    return xi12*xi13 * P_23 + xi12*xi23 * P_13 + xi13*xi23 * P_12;
}


// ====================================================================================================


double model_zeta_ell_term_2 (const double r12, const double r13, const int ell, const std::shared_ptr<cbl::glob::FuncGrid> interp_xi2) 
{
    auto integrand = [&] (const double mu) {return model_zeta_term_2(r12, r13, mu, interp_xi2) * cbl::legendre_polynomial(mu, ell); };

    return gauss_legendre_integration(integrand, 31) * (2*ell+1) / 2;
}


// ====================================================================================================


double model_zeta_ell_term_2 (const double r12_min, const double r12_max, const double r13_min, const double r13_max, const int ell, const std::shared_ptr<cbl::glob::FuncGrid> interp_xi2) 
{
    double V1 = ( pow(r12_max, 3) - pow(r12_min, 3)) / 3;
    double V2 = ( pow(r13_max, 3) - pow(r13_min, 3)) / 3;

    auto integrand = [&] (const std::vector<double> xx) {
        double r12 = pow(xx[0], 1./3);
        double r13 = pow(xx[1], 1./3);
        double mu = xx[2];
        return model_zeta_term_2(r12, r13, mu, interp_xi2) * cbl::legendre_polynomial(mu, ell); 
    };

    size_t ndim = 3;
    std::vector<std::vector<double>> limits = {{pow(r12_min, 3), pow(r12_max, 3)}, {pow(r13_min, 3), pow(r13_max, 3)}, {-1, 1}};

    cbl::wrapper::cuba::CUBAwrapper CW(integrand, ndim);
    CW.inputs().EPSREL=1.e-6;


    return CW.IntegrateCuhre(limits) * (2*ell+1) / 2 / (9. * V1 * V2);
}


// ====================================================================================================


double model_zeta_term_3 (const double r12, const double r13, const double mu23, const std::shared_ptr<cbl::glob::FuncGrid> interp_xi3m, const std::shared_ptr<cbl::glob::FuncGrid> interp_xi3p)
{
    double r23 = sqrt(r12*r12 + r13*r13 - 2 * mu23 * r12 * r13);
    
    double mu12 = (r23*r23 + r13*r13 - r12*r12) / ( 2*r13*r23);
    double mu13 = (r23*r23 + r12*r12 - r13*r13) / ( 2*r12*r23);

    double P_12 = 0.5 * (5 * pow(mu12, 3) - 3 * mu12);
    double P_13 = 0.5 * (5 * pow(mu13, 3) - 3 * mu13);
    double P_23 = 0.5 * (5 * pow(mu23, 3) - 3 * mu23);
    
    double xim12 = interp_xi3m->operator()(r12), xip12 = interp_xi3p->operator()(r12);
    double xim13 = interp_xi3m->operator()(r13), xip13 = interp_xi3p->operator()(r13);
    double xim23 = interp_xi3m->operator()(r23), xip23 = interp_xi3p->operator()(r23);

    return (xim12*xip13 + xim13*xip12) * P_23 +  (xim12*xip23 + xim23*xip12) * P_13 + (xim23*xip13 + xim13*xip23) * P_12;
}


// ====================================================================================================


double model_zeta_ell_term_3 (const double r12, const double r13, const int ell, const std::shared_ptr<cbl::glob::FuncGrid> interp_xi3m, const std::shared_ptr<cbl::glob::FuncGrid> interp_xi3p) 
{
    auto integrand = [&] (const double mu) {return model_zeta_term_3(r12, r13, mu, interp_xi3m, interp_xi3p) * cbl::legendre_polynomial(mu, ell); };

    return gauss_legendre_integration(integrand, 31) * (2*ell+1) / 2;
}


// ====================================================================================================


double model_zeta_ell_term_3 (const double r12_min, const double r12_max, const double r13_min, const double r13_max, const int ell, const std::shared_ptr<cbl::glob::FuncGrid> interp_xi3m, const std::shared_ptr<cbl::glob::FuncGrid> interp_xi3p) 
{
    double V1 = ( pow(r12_max, 3) - pow(r12_min, 3)) / 3;
    double V2 = ( pow(r13_max, 3) - pow(r13_min, 3)) / 3;

    auto integrand = [&] (const std::vector<double> xx) {
        double r12 = pow(xx[0], 1./3);
        double r13 = pow(xx[1], 1./3);
        double mu = xx[2];
        return model_zeta_term_3(r12, r13, mu, interp_xi3m, interp_xi3p) * cbl::legendre_polynomial(mu, ell); 
    };

    size_t ndim = 3;
    std::vector<std::vector<double>> limits = {{pow(r12_min, 3), pow(r12_max, 3)}, {pow(r13_min, 3), pow(r13_max, 3)}, {-1, 1}};

    cbl::wrapper::cuba::CUBAwrapper CW(integrand, ndim);
    CW.inputs().EPSREL=1.e-6;

    return CW.IntegrateCuhre(limits) * (2*ell+1) / 2 /(9. * V1 * V2);
}

// ====================================================================================================


double model_zeta_term_4 (const double r12, const double r13, const double mu23, const std::shared_ptr<cbl::glob::FuncGrid> interp_xi4)
{
    double r23 = sqrt(r12*r12 + r13*r13 - 2 * mu23 * r12 * r13);
    
    double mu12 = (r23*r23 + r13*r13 - r12*r12) / ( 2*r13*r23);
    double mu13 = (r23*r23 + r12*r12 - r13*r13) / ( 2*r12*r23);

    double P_12 = 0.125 * (35 * pow(mu12, 4) - 30 * pow(mu12, 2) + 3);
    double P_13 = 0.125 * (35 * pow(mu13, 4) - 30 * pow(mu13, 2) + 3);
    double P_23 = 0.125 * (35 * pow(mu23, 4) - 30 * pow(mu23, 2) + 3);
    
    double xi12 =  interp_xi4->operator()(r12);
    double xi13 =  interp_xi4->operator()(r13);
    double xi23 =  interp_xi4->operator()(r23);

    return xi12*xi13 * P_23 + xi12*xi23 * P_13 + xi13*xi23 * P_12;
}


// ====================================================================================================


double model_zeta_ell_term_4 (const double r12, const double r13, const int ell, const std::shared_ptr<cbl::glob::FuncGrid> interp_xi4) 
{
    auto integrand = [&] (const double mu) {return model_zeta_term_4(r12, r13, mu, interp_xi4) * cbl::legendre_polynomial(mu, ell); };

    return gauss_legendre_integration(integrand, 31) * (2*ell+1) / 2;
}


// ====================================================================================================


double model_zeta_ell_term_4 (const double r12_min, const double r12_max, const double r13_min, const double r13_max, const int ell, const std::shared_ptr<cbl::glob::FuncGrid> interp_xi4) 
{
    double V1 = ( pow(r12_max, 3) - pow(r12_min, 3)) / 3;
    double V2 = ( pow(r13_max, 3) - pow(r13_min, 3)) / 3;

    auto integrand = [&] (const std::vector<double> xx) {
        double r12 = pow(xx[0], 1./3);
        double r13 = pow(xx[1], 1./3);
        double mu = xx[2];
        return model_zeta_term_4(r12, r13, mu, interp_xi4) * cbl::legendre_polynomial(mu, ell); 
    };

    size_t ndim = 3;
    std::vector<std::vector<double>> limits = {{pow(r12_min, 3), pow(r12_max, 3)}, {pow(r13_min, 3), pow(r13_max, 3)}, {-1, 1}};

    cbl::wrapper::cuba::CUBAwrapper CW(integrand, ndim);
    CW.inputs().EPSREL=1.e-6;

    return CW.IntegrateCuhre(limits) * (2*ell+1) / 2 / (9. * V1 * V2);
}
