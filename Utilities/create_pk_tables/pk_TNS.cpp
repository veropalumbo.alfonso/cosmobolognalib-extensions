#include "CBL.h"
#include "pk_taruya.h"

using namespace std;

// these two variables contain the name of the CosmoBolognaLib
// directory and the name of the current directory (useful when
// launching the code on remote systems)
std::string cbl::par::DirCosmo = DIRCOSMO, cbl::par::DirLoc = DIRL;

// ====================================================================================================


int main (int argc, char **argv) {

  if (argc != 2)
    cout << "Wrong number of inputs!" << endl;

  double sigmaNL = atof(argv[1]);

  // -----------------------------------------------------------------
  // ---------------- setting the cosmological parameters ------------
  // -----------------------------------------------------------------

  const double OmegaM = 0.307115;
  const double Omega_b = 0.048;
  const double Omega_nu = 0.;
  const double massless_neutrinos = 3.04;
  const int massive_neutrinos = 0; 
  const double OmegaL = 1.-OmegaM;
  const double Omega_radiation = 0.;
  const double hh = 0.6777;
  const double scalar_amp = 2.1395219925855664626e-9;
  const double scalar_pivot = 0.05;
  const double n_s = 0.9611;
  const double wa = 0.;
  const double w0 = -1.;   
  const double sigma8 = 0.8288;

  cbl::cosmology::Cosmology cosmology {OmegaM, Omega_b, Omega_nu, massless_neutrinos, massive_neutrinos, OmegaL, Omega_radiation, hh, scalar_amp, scalar_pivot, n_s, w0, wa};
  //cosmology.set_sigma8(sigma8);
  cout << cosmology.sigma8_Pk("CAMB", 0) << " " << sigma8 << endl;


  // Obtain the Linear Pk
  std::vector<double> kk = cbl::logarithmic_bin_vector(1024, 1.e-5, 1.e2);
  std::vector<double> pkLin = cosmology.Pk_DM (kk, "CAMB", false, 0., "./");
  std::vector<double> pkApprox = cosmology.Pk_DM (kk, "EisensteinHu", false, 0., "./");

  double lambda = 0.25;
  string method = "gaussian_1d";
  std::vector<double> pkNW = cosmology.Pk_DM_NoWiggles_gaussian (kk, pkLin, pkApprox, lambda, method); 

  ofstream fout("pk.dat");
  for (size_t i=0; i<kk.size(); i++)
    fout << kk[i] << " " << pkNW[i] + exp(-pow(kk[i]*sigmaNL, 2))*(pkLin[i]-pkNW[i]) << endl;

  fout.clear(); fout.close();

  auto interp_pk = glob::FuncGrid(kk, pkLin, "Spline");
  
  // setting parameters for PkA and PkB
  string File_par = "params_AB_termsTNS.ini";
  fout.open(File_par);
  fout << 0 << "\n"
    << "1 100. \n"
    << "pk.dat \n"
    << "1 \n" << conv(hh, par::fDP6) <<"\n"
    << "2 \n" << par::TCMB << "\n"
    << "3 \n" << n_s << "\n"
    << "4 \n" << sigma8 << "\n"
    << "5 \n" << conv(OmegaM, par::fDP6) << "\n"
    << "6 \n" <<  conv(Omega_b, par::fDP6) << "\n"
    << "7 \n" << conv(w0, par::fDP6) << "\n"
    << "0 \n" << "1 \n" << "0. \n";
  fout.close();

  string dir_CPT = cbl::par::DirCosmo+"/External/CPT_Library/";
  string calc_pk_correction  = dir_CPT+"calc_pk_correction < "  + File_par; if (system (calc_pk_correction.c_str())) {}
  string calc_pk_correction2 = dir_CPT+"calc_pk_correction2 < " + File_par; if (system (calc_pk_correction2.c_str())) {}

  string cmd = "mv pkstd_corr2_tree.dat ../../tables/pk/pk_TNS_A.dat"; if (system(cmd.c_str())) {}
  cmd = "mv pkstd_corr_tree.dat ../../tables/pk/pk_TNS_B.dat"; if (system(cmd.c_str())) {}





  /*
  // ========================== 
  // ======= Compute TNS ======
  // ==========================
  std::vector<double> klist = cbl::logarithmic_bin_vector (512, 1.e-4, 10.);

  std::vector<string> kind = {"11", "12", "22", "23", "33"};

  vector<vector<double>> Aterms;
  for (size_t i=0; i<kind.size(); i++)
  {
    vector<double> A1 = TNS_A_1(kind[i], klist, interp_pk);
    vector<double> A2 = TNS_A_2(kind[i], klist, interp_pk);
    vector<double> AA (A1.size(), 0);
    for (size_t j=0; j<AA.size(); j++)
      AA [j] = A2[j]+A1[j];
    Aterms.push_back(AA);
  }

  fout.open("Aterms.dat");
  
  for (size_t j=0; j<klist.size(); j++) {
    fout << klist[j];
    for (size_t i=0; i<Aterms.size(); i++)
      fout << " " << Aterms[i][j];
    fout << endl;
  }
  fout.clear(); fout.close();
  */
  
  return 0;
}
