#include "CBL.h"
#include "Table.h"
#include "ParameterFile.h"
#include "CBL.h"
#include "pk_modelling.h"

using namespace std;

// these two variables contain the name of the CosmoBolognaLib
// directory and the name of the current directory (useful when
// launching the code on remote systems)
std::string cbl::par::DirCosmo = DIRCOSMO, cbl::par::DirLoc = DIRL;

// ====================================================================================================


int main () {

  // -----------------------------------------------------------------
  // ---------------- setting the cosmological parameters ------------
  // -----------------------------------------------------------------

  const double OmegaM = 0.307115;
  const double Omega_b = 0.048;
  const double Omega_nu = 0.;
  const double massless_neutrinos = 3.04;
  const int massive_neutrinos = 0; 
  const double OmegaL = 1.-OmegaM;
  const double Omega_radiation = 0.;
  const double hh = 0.6777;
  const double scalar_amp = 2.742e-9;
  const double scalar_pivot = 0.05;
  const double n_s = 0.9611;
  const double wa = 0.;
  const double w0 = -1.;   
  const double sigma8 = 0.8288;

  cbl::cosmology::Cosmology cosmology {OmegaM, Omega_b, Omega_nu, massless_neutrinos, massive_neutrinos, OmegaL, Omega_radiation, hh, scalar_amp, scalar_pivot, n_s, w0, wa};
  cosmology.set_sigma8(sigma8);

  // Obtain the Linear Pk
  std::vector<double> kk = cbl::logarithmic_bin_vector(1024, 1.e-5, 1.e2);
  std::vector<double> pkLin = cosmology.Pk_DM (kk, "CAMB", false, 0., "./");
  std::vector<double> pkApprox = cosmology.Pk_DM (kk, "EisensteinHu", false, 0., "./");

  double lambda = 0.25;
  string method = "gaussian_1d";
  std::vector<double> pkNW = cosmology.Pk_DM_NoWiggles_gaussian (kk, pkLin, pkApprox, lambda, method); 

  double lBAO = 110;
  double sigmaSq = 1;

  std::vector<double> klist = cbl::logarithmic_bin_vector (512, 1.e-4, 10.);

  cbl::glob::FuncGrid interp_Pk_Lin(kk, pkLin, "Spline");
  cbl::glob::FuncGrid interp_Pk_NW(kk, pkNW, "Spline");

  // Storage
  string file_output_lin = "../../tables/pk/pk_SPT_lin.dat";
  string file_output_NW = "../../tables/pk/pk_SPT_nowiggles.dat";

  cout << "Computing SPT 1-loop integrals for power spectrum" << endl;
  cout << "Output will be stored in " << file_output_lin << " and " << file_output_NW << endl;

  cout << "k   " << "  " << "progress" << "%           " << endl;

  ofstream foutLin (file_output_lin.c_str());
  ofstream foutNW (file_output_NW.c_str());
  for (size_t i = 0; i<klist.size(); i++) {
    const string progress = cbl::conv(double(i)/klist.size()*100, cbl::par::fDP2);
    cout << klist[i] << "  " << progress << "%           \r";
    cout.flush();

    double pk_22_dd = Pk_22_dd (klist[i], interp_Pk_Lin, 1.e-5, 10);
    double pk_13_dd = Pk_13_dd (klist[i], interp_Pk_Lin, 1.e-5, 10);
    double pk_22_dt = Pk_22_dt (klist[i], interp_Pk_Lin, 1.e-5, 10);
    double pk_13_dt = Pk_13_dt (klist[i], interp_Pk_Lin, 1.e-5, 10);
    double pk_22_tt = Pk_22_tt (klist[i], interp_Pk_Lin, 1.e-5, 10);
    double pk_13_tt = Pk_13_tt (klist[i], interp_Pk_Lin, 1.e-5, 10);
    foutLin << klist[i] << " " << interp_Pk_Lin(klist[i]) << " " <<  pk_22_dd << " " << pk_13_dd << " " << pk_22_dt << " " << pk_13_dt << " " << pk_22_tt << " " << pk_13_tt << endl;

    pk_22_dd = Pk_22_dd (klist[i], interp_Pk_NW, 1.e-5, 10);
    pk_13_dd = Pk_13_dd (klist[i], interp_Pk_NW, 1.e-5, 10);
    pk_22_dt = Pk_22_dt (klist[i], interp_Pk_NW, 1.e-5, 10);
    pk_13_dt = Pk_13_dt (klist[i], interp_Pk_NW, 1.e-5, 10);
    pk_22_tt = Pk_22_tt (klist[i], interp_Pk_NW, 1.e-5, 10);
    pk_13_tt = Pk_13_tt (klist[i], interp_Pk_NW, 1.e-5, 10);
    foutNW << klist[i] << " " << interp_Pk_NW(klist[i]) << " " <<  pk_22_dd << " " << pk_13_dd << " " << pk_22_dt << " " << pk_13_dt << " " << pk_22_tt << " " << pk_13_tt << endl;
  }
  foutLin.clear(); foutLin.close();
  foutNW.clear(); foutNW.close();

  cout << endl;
  cout << "Done!" << endl;
}


