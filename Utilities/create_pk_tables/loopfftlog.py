import numpy as np
import matplotlib.pyplot as plt
import CosmoBolognaLib as cbl
import os 
from matplotlib import rc
from scipy.special import loggamma, gamma
from scipy.fft import rfft, irfft, fft
from scipy.interpolate import interp1d

class PkFFTlog:
    def __init__(self, bias=-1.6, Nmax=256, kmin0=1.e-5, kmax0=100):

        self.delta = None
        self.k_n = None
        self.k_n_bias = None
        self.eta_m = None
        self.II = None

        self.Nmax = None
        self.bias = None
        self.kmin0 = None
        self.kmax0 = None
        self.k_eta_matrix = None
        self.set_parameters(bias=bias, Nmax=Nmax, kmin0=kmin0, kmax0=kmax0)

    def set_parameters(self, bias=-1.6, Nmax=256, kmin0=1.e-5, kmax0=100):

        self.Nmax = Nmax
        self.bias = bias
        self.kmin0 = kmin0
        self.kmax0 = kmax0

        self.delta = 1 / (self.Nmax - 1) * np.log(self.kmax0 / self.kmin0)
        self.k_n = np.array(
            [self.kmin0 * np.exp(i * self.delta) for i in range(self.Nmax)])
        self.eta_m = np.array([
            self.bias + 2 * np.pi * 1j / (self.Nmax * self.delta) *
            (j - self.Nmax / 2) for j in range(0, self.Nmax + 1)
        ])
        self.k_n_bias = np.array(
            [np.exp(-self.bias * i * self.delta) for i in range(self.Nmax)])
        X, Y = np.meshgrid(self.k_n, self.eta_m)
        self.k_eta_matrix = X**Y

    def _get_II(self, nu1, nu2):
        return 1. / (8 * np.pi**1.5) * gamma(1.5 - nu1) * gamma(
            1.5 - nu2) * gamma(nu1 + nu2 - 1.5) / (gamma(nu1) * gamma(nu2) *
                                                   gamma(3 - nu1 - nu2))
    def get_c_m(self, p_k):

        pk_bias = p_k * self.k_n_bias

        cm = rfft(pk_bias) / self.Nmax
        c_m = 1j * np.zeros(self.Nmax + 1)
        c_m[int(self.Nmax / 2):] = cm
        c_m[0:int(self.Nmax / 2)] = cm[-1:0:-1].conj()

        return c_m * self.kmin0**-self.eta_m
    
        self.kmax0 = kmax0

class PkMatter(PkFFTlog):
        
    def __init__(self, bias=-1.6, Nmax=256, kmin0=1.e-5, kmax0=100):
            
        self.M22 = None
        self.M13 = None
        self.set_parameters(bias=-1.6, Nmax=256, kmin0=1.e-5, kmax0=100)

    def set_parameters(self, bias=-1.6, Nmax=256, kmin0=1.e-5, kmax0=100):
        super().set_parameters(bias=bias, Nmax=Nmax, kmin0=kmin0, kmax0=kmax0)
        self.set_tables()
        
    def _get_M13(self, nu1):
        return (1 + 9 * nu1) / 4 * np.tan(nu1 * np.pi) / (28 * np.pi *
                                                          (nu1 + 1) * nu1 *
                                                          (nu1 - 1) *
                                                          (nu1 - 2) *
                                                          (nu1 - 3))

    def _get_M22(self, nu1, nu2):
        nu12 = nu1 + nu2
        den = 196 * nu1 * (1 + nu1) * (0.5 - nu1) * nu2 * (1 + nu2) * (0.5 -
                                                                       nu2)
        num = nu1 * nu2 * (98 * nu12**2 - 14 * nu12 +
                           36) - 91 * nu12**2 + 3 * nu12 + 58
        num = (1.5 - nu12) * (0.5 - nu12) * num * self.II
        return num / den

    def set_tables(self):
        X, Y = np.meshgrid(-0.5 * self.eta_m, -0.5 * self.eta_m)
        self.II = self._get_II(X, Y)
        self.M22 = self._get_M22(X, Y)
        self.M13 = self._get_M13(-0.5 * self.eta_m)

    def __call__(self, interp_pk):

        p_k = interp_pk(self.k_n)
        c_m = self.get_c_m(p_k)
        
        p_k_13 = self.k_n**3 * p_k * np.dot(c_m * self.M13, self.k_eta_matrix).real

        p_k_22 = self.k_n**3 * np.array([
            np.dot(c_m * line, np.dot(self.M22, c_m * line)).real
            for line in self.k_eta_matrix.T
        ])

        return self.k_n, p_k, p_k_22, p_k_13
    

    