using namespace std;
using namespace cbl;


// =============================================================================


double TNS_A11 (const double rr, const double xx)
{
  return - pow(rr, 3)/7 * ( xx + 6 * pow(xx, 3) + pow(rr, 2) * xx * (-3. + 10. * pow(xx, 2)) +
           rr * ( -3 + pow(xx, 2) - 12 * pow(xx, 4)) );
}


// =============================================================================


double TNS_A12 (const double rr, const double xx)
{
  return pow(rr, 4) / 14 * ( pow(xx, 2) - 1) * (-1. + 7 * rr * xx - 6 * pow(xx, 2));
}


// =============================================================================


double TNS_A22 (const double rr, const double xx)
{
  return pow(rr, 3) / 14 * ( pow(rr, 2) * xx * (13. -  41. * pow(xx, 2) )
         -4 * ( xx + 6 * pow(xx,3 ) ) + rr * (5. + 9. * pow(xx, 2) + 42 * pow(xx, 4)) );
}


// =============================================================================


double TNS_A33 (const double rr, const double xx)
{
  return pow(rr, 3)/14 * (1. - 7. * rr * xx + 6. * pow(xx, 2)) * (-2. * xx + rr * ( -1 + 3 * pow(xx, 2)));
}


// =============================================================================


double TNS_A11_tilde (const double rr, const double xx)
{
  return 1./7 * (xx + rr - 2. * rr * pow(xx, 2)) * (3 * rr + 7 * xx - 10 * rr * pow(xx, 2));
}



// =============================================================================


double TNS_A12_tilde (const double rr, const double xx)
{
  return rr/14 * (pow(xx, 2) -1) * (3 * rr + 7 * xx - 10 * rr * pow(xx, 2));
}


// =============================================================================


double TNS_A22_tilde (const double rr, const double xx)
{
  return 1./14 * (28 * pow(xx, 2) + rr * xx * (25. - 81. * pow(xx, 2)) + pow(rr, 2) *
         (1 - 27 * pow(xx, 2) + 54*pow(xx, 4)));
}


// =============================================================================


double TNS_A33_tilde (const double rr, const double xx)
{
  return rr/14 * ( 1 - pow(xx, 2) ) * ( rr - 7 * xx + 6 * rr * pow(xx, 2));
}


// =============================================================================


struct CUBA_TNS_integral_inputs 
{
    double rmin;
    double rmax;
    double kk;
    double pk;
    function<double(const double, const double)> AA;
    function<double(const double, const double)> AA_tilde;
    function<double(const double, const double)> BB;
    glob::FuncGrid interp_Pk;
};


// =============================================================================


int CUBA_TNS_A1_Integrand (const int *ndim, const cubareal xx[], const int *ncomp, cubareal ff[], void *userdata)
{
  CUBA_TNS_integral_inputs *pp = static_cast<CUBA_TNS_integral_inputs *>(userdata);

  vector<double> var(*ndim, 2);
  var[0] = xx[0]*(pp->rmax-pp->rmin)+pp->rmin;

  double ymin = max(-1.0, (1.+pow(var[0],2)-pow(pp->rmax,2))/(2*var[0]));
  double ymax = min( 1.0, (1.+pow(var[0],2)-pow(pp->rmin,2))/(2*var[0]));

  if(var[0] > 0.5) ymax= 0.5/var[0];

  var[1] = xx[1]*2 - 1;

  double Plin_p = pp->interp_Pk(pp->kk * var[0]);
  double qq = sqrt(1 + pow(var[0], 2) - 2 * var[0] * var[1]);
  double Plin_q = pp->interp_Pk(pp->kk * qq);

  ff[*ncomp-1]  = pp->AA(var[0], var[1]) * pp->pk + pp->AA_tilde(var[0], var[1]) * Plin_p;
  ff[*ncomp-1] *= Plin_q / pow(qq, 2) * (pp->rmax-pp->rmin) * (ymax-ymin);

  return 0;
}


// =============================================================================


double TNS_a_11 (const double rr)
{
  if ( rr==1)
    return 0;

  return -1./( 84 * rr) * ( 2. * rr *(19 - 24 * pow(rr, 2) + 9 * pow(rr, 4))-
      9 * pow(pow(rr, 2) -1, 3) * log( fabs((rr+1) / (rr-1))));
}


// =============================================================================


double TNS_a_12 (const double rr)
{
  if ( rr==1)
    return 0;

  return 1./( 112 * pow(rr, 3)) * ( 2. * rr * (pow(rr, 2)+1) * (3-14*pow(rr,2)+3*pow(rr,4)) 
         -3 * pow(pow(rr, 2)-1, 4) * log( fabs((rr+1) / (rr-1))) );
}


// =============================================================================


double TNS_a_22 (const double rr)
{
  if ( rr==1)
    return 0;

  return 1./( 336 * pow(rr, 3)) * ( 2. * rr * (9-185*pow(rr,2) + 159 * pow(rr, 4) - 63 * pow(rr, 6))
     + 9 * pow(pow(rr,2)-1, 3) * (7*pow(rr,2)+1) * log( fabs((rr+1) / (rr-1))) );
}


// =============================================================================


double TNS_a_33 (const double rr)
{
  if (rr==1)
    return 0;

  return 1./( 336 * pow(rr, 3)) * ( 2. * rr * (9-109*pow(rr,2) + 63 * pow(rr, 4) - 27 * pow(rr, 6))
     + 9 * pow(pow(rr,2)-1, 3) * (3*pow(rr,2)+1) * log( fabs((rr+1) / (rr-1))) );
}


// =============================================================================


vector<double> TNS_A_1 (const string kind, const vector<double> kk, const glob::FuncGrid interp_Pk, const double kmin=1.e-5, const double kmax=1.e2, const double rel_err = 1.e-4, const int nevals = 1e8)
{

  double norm = 1. / pow(2* par::pi, 2);

  wrapper::cuba::STR_CUBA_inputs inputs;
  inputs.EPSREL = rel_err;
  inputs.KEY = 13;
  inputs.EPSABS = 0;
  inputs.MAXEVAL = nevals;

  CUBA_TNS_integral_inputs *userdata = new CUBA_TNS_integral_inputs;
  userdata->interp_Pk = interp_Pk;

  if (kind == "11") {
    userdata->AA = &TNS_A11;
    userdata->AA_tilde = &TNS_A11_tilde;
  } else if (kind == "12" or kind == "23") {
    userdata->AA = &TNS_A12;
    userdata->AA_tilde = &TNS_A12_tilde;
  } else if (kind == "22") {
    userdata->AA = &TNS_A22;
    userdata->AA_tilde = &TNS_A22_tilde;
  } else if (kind == "33") {
    userdata->AA = &TNS_A33;
    userdata->AA_tilde = &TNS_A33_tilde;
  } else {
    coutCBL << "Unvalid option for string " << kind << " exiting" << endl;
    exit(-1);
  }

  int ndim=2;
  int comp, neval, fail, nregions;
  vector<double> integral(inputs.NCOMP), error(inputs.NCOMP), prob(inputs.NCOMP);

  vector<double> term;
  for (size_t i=0; i<kk.size(); i++) {
    userdata->kk = kk[i];
    userdata->rmin = kmin/kk[i];
    userdata->rmax = kmax/kk[i];
    userdata->pk = interp_Pk(kk[i]);

    Cuhre(ndim, inputs.NCOMP, CUBA_TNS_A1_Integrand, userdata, inputs.NVEC,
	inputs.EPSREL, inputs.EPSABS, inputs.VERBOSE | inputs.LAST,
	inputs.MINEVAL, inputs.MAXEVAL, inputs.KEY,
	inputs.STATEFILE, inputs.SPIN.get(),
	&nregions, &neval, &fail, integral.data(), error.data(), prob.data());

    if(inputs.VERBOSE>0){
      printf("CUHRE RESULT:\tnregions %d\tneval %d\tfail %d\n", nregions, neval, fail);
      for( comp = 0; comp < inputs.NCOMP; ++comp )
	printf("CUHRE RESULT:\t%.8f +- %.8f\tp = %.3f\n", (double)integral[comp], (double)error[comp], (double)prob[comp]);
    }
    term.push_back ( pow(kk[i], 3) * norm * integral[0] );
  }

  delete userdata;

  return term;
}


// =============================================================================


vector<double> TNS_A_2 (const string kind, const vector<double> kk, const glob::FuncGrid interp_Pk,  const double kmin = 1.e-5, const double kmax = 100, const double rel_err = 1.e-6, const int nevals = 10000)
{
  double norm = 1. / pow(2* par::pi, 2);

  function <double(double)> func;

  if (kind == "11") {
    func = &TNS_a_11;
  } else if (kind == "12" or kind == "23") {
    func = &TNS_a_12;
  } else if (kind == "22") {
    func = &TNS_a_22;
  } else if (kind == "33") {
    func = &TNS_a_33;
  } else {
    coutCBL << "Unvalid option for string " << kind << " exiting" << endl;
    exit(-1);
  }

  vector<double> integral;
  for (size_t i=0; i<kk.size(); i++) {

    double _k = kk[i];
    auto integrand = [&] (const double rr) {
      return func(rr) * interp_Pk(rr*_k);
    };

    double II = wrapper::gsl::GSL_integrate_cquad(integrand, kmin/_k, kmax/_k, rel_err, nevals);

    integral.push_back(pow(_k, 3) * interp_Pk(_k) * norm * II);
  }
  return integral;
}

