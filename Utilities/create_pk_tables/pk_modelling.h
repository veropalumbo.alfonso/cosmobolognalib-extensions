using namespace std;
using namespace cbl;


// =============================================================================


struct CUBA_integral_inputs 
{
    double qmin;
    double qmax;
    double kk;
    function<double(vector<double>)> integrand;
};

// =============================================================================


int CUBAIntegrand (const int *ndim, const cubareal xx[], const int *ncomp, cubareal ff[], void *userdata)
{
  CUBA_integral_inputs *pp = static_cast<CUBA_integral_inputs *>(userdata);

  vector<double> var(*ndim, 2);
  var[0] = xx[0]*(pp->qmax-pp->qmin)+pp->qmin;

  double ymin = fabs(pp->kk-var[0]); //max(var[0], fabs(pp->kk-var[0]));
  //double ymin = max(var[0], fabs(pp->kk-var[0]));
  double ymax = pp->kk+var[0]; //min(pp->qmax, pp->kk+var[0]);
  var[1] = xx[1]*(ymax-ymin)+ymin;

  ff[*ncomp-1] = pp->integrand(var)*(pp->qmax-pp->qmin)*(ymax-ymin);

  return 0;
}


// =============================================================================


double Pk_Integrator (const double kk,
        function<double(const double, const double, const double)> func,
        const double qmin,
        const double qmax)
{
  int ndim=2;
  double norm = 0.25*pow(par::pi, -2)/kk;

  auto integrand = [&] (const std::vector<double> xx) {
    double qq = xx[0];
    double yy = xx[1];

    return qq * yy * func(qq, yy, kk);
  };

  wrapper::cuba::STR_CUBA_inputs inputs;
  inputs.EPSREL = 1.e-4;
  inputs.KEY = 13;
  inputs.EPSABS = 0;
  inputs.MAXEVAL = 1.e8;
  if (kk<1.e-2) {
    inputs.EPSREL = 1.e-3;
    inputs.MAXEVAL = 1.e6;
  }

  int comp, neval, fail, nregions;
  vector<double> integral(inputs.NCOMP), error(inputs.NCOMP), prob(inputs.NCOMP);

  CUBA_integral_inputs *userdata = new CUBA_integral_inputs;
  userdata->integrand = integrand;
  userdata->qmin = qmin;
  userdata->qmax = qmax;
  userdata->kk = kk;

  Cuhre(ndim, inputs.NCOMP, CUBAIntegrand, userdata, inputs.NVEC,
    inputs.EPSREL, inputs.EPSABS, inputs.VERBOSE | inputs.LAST,
    inputs.MINEVAL, inputs.MAXEVAL, inputs.KEY,
    inputs.STATEFILE, inputs.SPIN.get(),
    &nregions, &neval, &fail, integral.data(), error.data(), prob.data());

  if(inputs.VERBOSE>0){
    printf("CUHRE RESULT:\tnregions %d\tneval %d\tfail %d\n", nregions, neval, fail);
    for( comp = 0; comp < inputs.NCOMP; ++comp )
      printf("CUHRE RESULT:\t%.8f +- %.8f\tp = %.3f\n", (double)integral[comp], (double)error[comp], (double)prob[comp]);
  }

  delete userdata;

  return integral[0]*norm;
}


// =============================================================================


double Sigma2 (const vector<double> kk, const vector<double> Pk_NoWiggles, const double kmin, const double kmax, const double lenght_BAO)
{
  glob::FuncGrid interp_Pk_NoWiggles (kk, Pk_NoWiggles, "Spline");
  double kk_BAO = 1./lenght_BAO;

  double norm = 1./ (6* pow(par::pi, 2) );

  auto integrand = [&] (const double qq) {
    return interp_Pk_NoWiggles(qq) * ( 1 - cbl::j0 ( qq / kk_BAO ) + 2 * cbl::j2 ( qq / kk_BAO ) );
  };

  return cbl::wrapper::gsl::GSL_integrate_cquad(integrand, kmin, kmax, 1.e-5, 0., 10000)*norm;
}


// =============================================================================


double compute_mu (const double q, const double y, const double k)
{
  return (k*k-q*q-y*y)/(2*y*q);
}


// =============================================================================


double kernel_F2 (const double q, const double y, const double k)
{
  double mu = compute_mu(q, y, k);

  return 5./7 + 0.5 * mu * (q/y+y/q) + 2./7 * mu*mu;
}


// =============================================================================


double kernel_G2 (const double q, const double y, const double k)
{
  double mu = compute_mu(q, y, k);

  return 3./7 + 0.5 * mu * (q/y+y/q) + 4./7 * mu*mu;
}


// =============================================================================


double kernel_F3 (const double q, const double k)
{
  double term1 = 6. * pow(k, 6) - 79. * pow(k, 4) * pow(q, 2) + 50. * pow(k, 2) * pow(q, 4) - 21. * pow(q, 6);
  double den1 = 63. * pow(k, 2) * pow(q, 4);

  double term2 = 0, den2 = 1;
  if (q!=k) {
    term2 = pow(pow(q, 2) - pow(k, 2), 3) * (7. * pow(q, 2) + 2. * pow(k, 2)) * log ( fabs ( (k+q) / (k-q) ));
    den2 = 42. * pow(k, 3) * pow(q, 5);
  }

  return term1/den1+term2/den2;
}


// =============================================================================


double kernel_G3 (const double q, const double k)
{
  double term1 = 6. * pow(k, 6) - 41. * pow(k, 4) * pow(q, 2) + 2. * pow(k, 2) * pow(q, 4) - 3. * pow(q, 6);
  double den1 = 21. * pow(k, 2) * pow(q, 4);

  double term2 = 0, den2 = 1;
  if (q!=k) {
    term2 = pow(pow(q, 2) - pow(k, 2), 3) * (pow(q, 2) + 2. * pow(k, 2)) * log ( fabs ( (k+q) / (k-q) ));
    den2 = 14. * pow(k, 3) * pow(q, 5);
  }

  return term1/den1+term2/den2;
}


// =============================================================================


double kernel_S2 (const double q, const double y, const double k)
{
  double mu = compute_mu(q, y, k);

  return mu*mu - 1./3;
}


// =============================================================================


double kernel_S2_Simonovic (const double q, const double y, const double k)
{
  double mu = compute_mu(q, y, k);

  return mu*mu - 1.;
}


// =============================================================================


double Pk_22_dd (const double kk, const glob::FuncGrid interp_Pk, const double qmin, const double qmax)
{
  auto integrand = [&] (const double qq, const double yy, const double kk)
  {
    return pow(kernel_F2(qq, yy, kk), 2)  * interp_Pk(qq) * interp_Pk(yy);
  };

  return 2*Pk_Integrator(kk, integrand, qmin, qmax);
}


// =============================================================================


double Pk_22_tt (const double kk, const glob::FuncGrid interp_Pk, const double qmin, const double qmax)
{
  auto integrand = [&] (const double qq, const double yy, const double kk)
  {
    return pow(kernel_G2(qq, yy, kk), 2) * interp_Pk(qq) * interp_Pk(yy);
  };

  return 2*Pk_Integrator(kk, integrand, qmin, qmax);
}


// =============================================================================


double Pk_22_dt (const double kk, const glob::FuncGrid interp_Pk, const double qmin, const double qmax)
{
  auto integrand = [&] (const double qq, const double yy, const double kk)
  {
    return kernel_G2(qq, yy, kk) * kernel_F2(qq, yy, kk) * interp_Pk(qq) * interp_Pk(yy);
  };

  return 2*Pk_Integrator(kk, integrand, qmin, qmax);
}


// =============================================================================


double Pk_13_dd (const double kk, const glob::FuncGrid interp_Pk, const double qmin, const double qmax)
{
  double norm = 6. * pow(kk, 3)/pow(2*par::pi, 2)*interp_Pk(kk)/12;

  auto integrate_r = [&] (const double log_r) {

    double rr = exp ( log_r);
    double qq = rr * kk;
    double pk_qq = interp_Pk(qq);

    return pk_qq * rr * rr * rr * kernel_F3(qq, kk);
  };

  return cbl::wrapper::gsl::GSL_integrate_cquad(integrate_r, log(qmin/kk), log(qmax/kk), 1.e-5, 0., 1000)*norm;
}


// =============================================================================


double Pk_13_tt (const double kk, const glob::FuncGrid interp_Pk, const double qmin, const double qmax)
{
  double norm = 6.* pow(kk, 3)/pow(2*par::pi, 2)*interp_Pk(kk)/12;

  auto integrate_r = [&] (const double log_r) {

    double rr = exp ( log_r);
    double qq = rr * kk;
    double pk_qq = interp_Pk(qq);

    return pk_qq * rr * rr * rr * kernel_G3(qq, kk);
  };

  return cbl::wrapper::gsl::GSL_integrate_cquad(integrate_r, log(qmin/kk), log(qmax/kk), 1.e-5, 0., 1000)*norm;
}


// =============================================================================


double Pk_13_dt (const double kk, const glob::FuncGrid interp_Pk, const double qmin, const double qmax)
{
  double norm = 3.* pow(kk, 3)/pow(2*par::pi, 2)*interp_Pk(kk)/12;

  auto integrate_r = [&] (const double log_r) {

    double rr = exp ( log_r);
    double qq = rr * kk;
    double pk_qq = interp_Pk(qq);

    return pk_qq * rr * rr * rr * (kernel_F3(qq, kk)+kernel_G3(qq, kk));
  };

  return cbl::wrapper::gsl::GSL_integrate_cquad(integrate_r, log(qmin/kk), log(qmax/kk), 1.e-5, 0., 1000)*norm;
}


// =============================================================================
// =============================================================================


double Pk_bias (const std::string bias_type, const double kk, const glob::FuncGrid interp_Pk, const double qmin, const double qmax)
{
  std::function<double(double, double, double)> integrand;

  if (bias_type == "b2d") {
    integrand = [&] (const double qq, const double yy, const double kk)
    {
      return kernel_F2(qq, yy, kk) * interp_Pk(qq) * interp_Pk(yy);
    };
  } else if (bias_type == "bs2d") {
    integrand = [&] (const double qq, const double yy, const double kk)
    {
      return kernel_F2(qq, yy, kk) *  kernel_S2(qq, yy, kk) * interp_Pk(qq) * interp_Pk(yy);
    };
  } else if (bias_type == "b22") {
    integrand = [&] (const double qq, const double yy, const double kk)
    {
      (void)kk;
      return 0.5 * interp_Pk(qq) * (interp_Pk(yy) - interp_Pk(qq));
    };
  } else if (bias_type == "b2bs2") {
    integrand = [&] (const double qq, const double yy, const double kk)
    {
      return -0.5*interp_Pk(qq) * ( 2. / 3 * interp_Pk(qq) - interp_Pk(yy) * kernel_S2(qq, yy, kk));
    };
  } else if (bias_type == "bs22") {
    integrand = [&] (const double qq, const double yy, const double kk)
    {
      return -0.5*interp_Pk(qq) * ( 4. / 9 * interp_Pk(qq) - interp_Pk(yy) * pow(kernel_S2(qq, yy, kk), 2));
    };
  } else if (bias_type == "b2t") {
    integrand = [&] (const double qq, const double yy, const double kk)
    {
      return kernel_G2(qq, yy, kk) * interp_Pk(qq) * interp_Pk(yy);
    };
  } else if (bias_type == "bs2t") {
    integrand = [&] (const double qq, const double yy, const double kk)
    {
      return kernel_G2(qq, yy, kk) *  kernel_S2(qq, yy, kk) * interp_Pk(qq) * interp_Pk(yy);
    };
  } else if (bias_type == "b3nl") {
    integrand = [&] (const double qq, const double yy, const double kk)
    {
      double S2 = kernel_S2(qq, yy, kk);
      double D2 = kernel_S2(-qq, kk, yy);

      return interp_Pk(qq) * (5./6 + 15./8 * S2 * D2 - 5./4 * S2);
    };
  } else if (bias_type == "fix_term"){
      integrand = [&] (const double qq, const double yy, const double kk)
      {
	(void)yy; (void)kk;
	return interp_Pk(qq)*interp_Pk(qq);
      };
  } else {
    cout << "Wrong bias term in input" << endl;
    exit(0);
  }
  
  return Pk_Integrator(kk, integrand, qmin, qmax);
} 


// =============================================================================


double Pk_bias_Simonovic (const std::string bias_type, const double kk, const glob::FuncGrid interp_Pk, const double qmin, const double qmax)
{
  std::function<double(double, double, double)> integrand;

  if (bias_type == "b2d") {
    integrand = [&] (const double qq, const double yy, const double kk)
    {
      return 2*kernel_F2(qq, yy, kk) * interp_Pk(qq) * interp_Pk(yy);
    };
  } else if (bias_type == "bs2d") {
    integrand = [&] (const double qq, const double yy, const double kk)
    {
      return 2*kernel_F2(qq, yy, kk) *  kernel_S2_Simonovic(qq, yy, kk) * interp_Pk(qq) * interp_Pk(yy);
    };
  } else if (bias_type == "b22") {
    integrand = [&] (const double qq, const double yy, const double kk)
    {
      (void)kk;
      return 2 * interp_Pk(qq) * (interp_Pk(yy) - interp_Pk(qq));
    };
  } else if (bias_type == "b2bs2") {
    integrand = [&] (const double qq, const double yy, const double kk)
    {
      return 2 * interp_Pk(qq) * interp_Pk(yy) * kernel_S2_Simonovic(qq, yy, kk);
    };
  } else if (bias_type == "bs22") {
    integrand = [&] (const double qq, const double yy, const double kk)
    {
      return 2*interp_Pk(qq) * interp_Pk(yy) * pow(kernel_S2_Simonovic(qq, yy, kk), 2);
    };
  } else if (bias_type == "b2t") {
    integrand = [&] (const double qq, const double yy, const double kk)
    {
      return 2*kernel_G2(qq, yy, kk) * interp_Pk(qq) * interp_Pk(yy);
    };
  } else if (bias_type == "bs2t") {
    integrand = [&] (const double qq, const double yy, const double kk)
    {
      return 2*kernel_G2(qq, yy, kk) *  kernel_S2_Simonovic(qq, yy, kk) * interp_Pk(qq) * interp_Pk(yy);
    };
  } else if (bias_type == "b3nl") {
    integrand = [&] (const double qq, const double yy, const double kk)
    {
      return 4 * kernel_F2(-qq, kk, yy) *  kernel_S2_Simonovic(qq, yy, kk) * interp_Pk(qq) * interp_Pk(kk);
    };
  } else if (bias_type == "fix_term"){
      integrand = [&] (const double qq, const double yy, const double kk)
      {
	(void)yy; (void)kk;
	return interp_Pk(qq)*interp_Pk(qq);
      };
  } else {
    cout << "Wrong bias term in input" << endl;
    exit(0);
  }
  
  return Pk_Integrator(kk, integrand, qmin, qmax);
}
