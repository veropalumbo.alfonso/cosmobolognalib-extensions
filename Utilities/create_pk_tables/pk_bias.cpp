#include "CBL.h"
#include "Table.h"
#include "ParameterFile.h"
#include "CBL.h"
#include "pk_modelling.h"

using namespace std;

// these two variables contain the name of the CosmoBolognaLib
// directory and the name of the current directory (useful when
// launching the code on remote systems)
std::string cbl::par::DirCosmo = DIRCOSMO, cbl::par::DirLoc = DIRL;

// ====================================================================================================


int main (int argc, char **argv) {

  if (argc != 2)
    cout << "Wrong number of inputs!" << endl;

  double sigmaNL = atof(argv[1]);

  // -----------------------------------------------------------------
  // ---------------- setting the cosmological parameters ------------
  // -----------------------------------------------------------------

  const double OmegaM = 0.307115;
  const double Omega_b = 0.048;
  const double Omega_nu = 0.;
  const double massless_neutrinos = 3.04;
  const int massive_neutrinos = 0; 
  const double OmegaL = 1.-OmegaM;
  const double Omega_radiation = 0.;
  const double hh = 0.6777;
  const double scalar_amp = 2.742e-9;
  const double scalar_pivot = 0.05;
  const double n_s = 0.9611;
  const double wa = 0.;
  const double w0 = -1.;   
  const double sigma8 = 0.8288;

  cbl::cosmology::Cosmology cosmology {OmegaM, Omega_b, Omega_nu, massless_neutrinos, massive_neutrinos, OmegaL, Omega_radiation, hh, scalar_amp, scalar_pivot, n_s, w0, wa};
  cosmology.set_sigma8(sigma8);


  // Obtain the Linear Pk
  std::vector<double> kk = cbl::logarithmic_bin_vector(1024, 1.e-5, 1.e2);
  std::vector<double> pkLin = cosmology.Pk_DM (kk, "CAMB", false, 0., "./");
  std::vector<double> pkApprox = cosmology.Pk_DM (kk, "EisensteinHu", false, 0., "./");

  double lambda = 0.25;
  string method = "gaussian_1d";
  std::vector<double> pkNW = cosmology.Pk_DM_NoWiggles_gaussian (kk, pkLin, pkApprox, lambda, method); 

  vector<double> pk(kk.size());
  for (size_t i=0; i<kk.size(); i++)
    pk[i] = pkNW[i] + exp(-pow(kk[i]*sigmaNL, 2))*(pkLin[i]-pkNW[i]);


  cbl::glob::FuncGrid interp_Pk(kk, pk, "Spline");

  std::vector<double> klist = cbl::logarithmic_bin_vector (512, 1.e-4, 10.);

  // Storage
  string file_output = "../../tables/pk/pk_bias_"+cbl::conv(sigmaNL, cbl::par::fDP0)+".dat";
  string file_outputS = "../../tables/pk/pk_bias_Simonovic_"+cbl::conv(sigmaNL, cbl::par::fDP0)+".dat";

  cout << "Computing SPT 1-loop integrals for power spectrum" << endl;
  cout << "Output will be stored in " << file_output << endl;

  cout << "k   " << "  " << "progress" << "%           " << endl;

  ofstream fout (file_output.c_str());
  ofstream foutS (file_outputS.c_str());
  vector<string> bias_list = {"b2d", "bs2d", "b22", "b2bs2", "bs22", "b2t", "bs2t", "b3nl", "fix_term"};

  for (size_t i = 0; i<klist.size(); i++) {
    const string progress = cbl::conv(double(i)/klist.size()*100, cbl::par::fDP2);
    cout << klist[i] << "  " << progress << "%           \r";
    cout.flush();

    fout << klist[i];
    foutS << klist[i];
    for (size_t j = 0; j<bias_list.size(); j++){
      foutS << " " << Pk_bias_Simonovic(bias_list[j], klist[i], interp_Pk, 1.e-5, 10);
      fout << " " << Pk_bias(bias_list[j], klist[i], interp_Pk, 1.e-5, 10);
    }
    fout << endl;
    foutS << endl;
  }
  fout.clear(); fout.close();
  foutS.clear(); foutS.close();

  cout << endl;
  cout << "Done!" << endl;
}
