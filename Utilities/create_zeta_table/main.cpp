#include "CBL.h"
#include "Table.h"
#include "utils.h"

using namespace std;
using namespace cbl;

// these two variables contain the name of the CosmoBolognaLib
// directory and the name of the current directory (useful when
// launching the code on remote systems)
std::string cbl::par::DirCosmo = DIRCOSMO, cbl::par::DirLoc = DIRL;

// ====================================================================================================


int main (int argc, char **argv) {

    if (argc != 2){
        coutCBL << "You should pass a parameter file in input!" << endl;
        exit(-1);
    }

    // define the input parameter file name
    string parameter_file = argv[1];

    // construct the object ReadParameter
    glob::ReadParameters parameters {parameter_file};

    // Input parameters
    string dir_output = parameters.find<string>("dir_output");

    string out_dir =  cbl::fullpath(dir_output);
    makeDir(out_dir, "/");

    // Power spectrum
    // -----------------------------------------------------------------
    // ---------------- setting the cosmological parameters ------------
    // -----------------------------------------------------------------

    const double OmegaM = parameters.find<double>("OmegaM");
    const double Omega_b = parameters.find<double>("Omega_b");
    const double Omega_nu = parameters.find<double>("Omega_nu");
    const double massless_neutrinos = parameters.find<double>("massless_neutrinos");
    const int massive_neutrinos = parameters.find<int>("massive_neutrinos"); 
    const double OmegaL = parameters.find<double>("OmegaL");
    const double Omega_radiation = parameters.find<double>("Omega_radiation");
    const double hh = parameters.find<double>("hh");
    const double scalar_amp = parameters.find<double>("scalar_amp");
    const double scalar_pivot = parameters.find<double>("scalar_pivot");
    const double n_s = parameters.find<double>("n_s");
    const double wa = parameters.find<double>("wa");
    const double w0 = parameters.find<double>("w0");   
    const double sigma8 = parameters.find<double>("sigma8");

    cbl::cosmology::Cosmology cosmology {OmegaM, Omega_b, Omega_nu, massless_neutrinos, massive_neutrinos, OmegaL, Omega_radiation, hh, scalar_amp, scalar_pivot, n_s, w0, wa};
    cosmology.set_sigma8(sigma8);

    const double redshift = parameters.find<double>("redshift"); 

    // Obtain the Linear Pk
    std::vector<double> kk = cbl::logarithmic_bin_vector(1024, 1.e-6, 1.e3);
    std::vector<double> pkCAMB = cosmology.Pk_DM (kk, "CAMB", false, redshift, "./");
    std::vector<double> pkEH = cosmology.Pk_DM (kk, "EisensteinHu", false, redshift, "./");
    std::vector<double> pkNW = cosmology.Pk_DM_NoWiggles_gaussian (kk, pkCAMB, pkEH, 0.25, "gaussian_1d");

    //The BAO damping factor, set this to zero to use the linear power spectrum
    double sigmaNL = sqrt(get_sigma_squared(cbl::glob::FuncGrid(kk, pkNW, "Spline"), 1.e-5));
    cout << sigmaNL << endl;

    // Compute stuff
    std::vector<double> pk(kk.size(), 0), pkm(kk.size(), 0), pkp(kk.size(), 0);
    ofstream fout("pk_Minerva.dat");
    for(size_t i=0; i<kk.size(); i++) {
        pk[i] = pkNW[i] + exp(-pow(kk[i]*sigmaNL, 2)) * (pkCAMB[i]-pkNW[i]);
        pkm[i] = pk[i]/kk[i];
        pkp[i] = pk[i]*kk[i];
        fout << kk[i] << " " << pk[i] << " " << pkCAMB[i] << " " << pkNW[i] << " " << pkEH[i] << endl;
    }
    fout.close();

    std::vector<std::shared_ptr<cbl::glob::FuncGrid>> xi_interp;
    std::vector<double> xx, fx;
    cbl::wrapper::fftlog::transform_FFTlog(xx, fx, 1, kk, pk, 0);
    xi_interp.push_back(std::make_shared<cbl::glob::FuncGrid>(cbl::glob::FuncGrid(xx, fx, "Spline")));

    xx.erase(xx.begin(), xx.end()); fx.erase(fx.begin(), fx.end());
    cbl::wrapper::fftlog::transform_FFTlog(xx, fx, 1, kk, pkm, 1);
    xi_interp.push_back(std::make_shared<cbl::glob::FuncGrid>(cbl::glob::FuncGrid(xx, fx, "Spline")));

    xx.erase(xx.begin(), xx.end()); fx.erase(fx.begin(), fx.end());
    cbl::wrapper::fftlog::transform_FFTlog(xx, fx, 1, kk, pkp, 1);
    xi_interp.push_back(std::make_shared<cbl::glob::FuncGrid>(cbl::glob::FuncGrid(xx, fx, "Spline")));

    xx.erase(xx.begin(), xx.end()); fx.erase(fx.begin(), fx.end());
    cbl::wrapper::fftlog::transform_FFTlog(xx, fx, 1, kk, pk, 2);
    xi_interp.push_back(std::make_shared<cbl::glob::FuncGrid>(cbl::glob::FuncGrid(xx, fx, "Spline")));

    // The numbers of the Minerva 3PCF measurements
    double rMin = parameters.find<double>("rMin");
    double rMax = parameters.find<double>("rMax");
    double deltaR = parameters.find<double>("binSize");

    size_t nOrders = parameters.find<double>("LMAX")+1;
    size_t nBins = int((rMax-rMin)/deltaR);
    
    // Output files with basic terms for the computation of the 3PCF computed at the bin center
    string file = out_dir+"zeta_model_0.dat";
    ofstream fout1(file.c_str());
    file = out_dir+"zeta_model_1.dat";
    ofstream fout3(file.c_str());
    file = out_dir+"zeta_model_2.dat";
    ofstream fout5(file.c_str());

    // Output files with basic terms for the computation of the 3PCF computed, bin averaged
    file = out_dir+"zeta_model_average_0.dat";
    ofstream fout2(file.c_str());
    file = out_dir+"zeta_model_average_1.dat";
    ofstream fout4(file.c_str());
    file = out_dir+"zeta_model_average_2.dat";
    ofstream fout6(file.c_str());

    size_t nIter = nOrders*(nBins*nBins+1)/2;
    size_t nn=0;

    cout << "r12" << "   " << "r13" << "   " << "ell" << "  " << "progress" << "%           " << endl;

    for (size_t i=0; i<nBins; i++) {
        double r12_min = rMin+i*deltaR;
        double r12_max = rMin+(i+1)*deltaR;
        double r12 = rMin+(i+0.5)*deltaR;

        for (size_t j=i; j<nBins; j++) {
            double r13_min = rMin+j*deltaR;
            double r13_max = rMin+(j+1)*deltaR;

            double r13 = rMin+(j+0.5)*deltaR;

            fout1 << setprecision(20) << r12 << " " << r13;
            fout2 << setprecision(20) << r12 << " " << r13;
            fout3 << setprecision(20) << r12 << " " << r13;
            fout4 << setprecision(20) << r12 << " " << r13;
            fout5 << setprecision(20) << r12 << " " << r13;
            fout6 << setprecision(20) << r12 << " " << r13;

            for (size_t ell=0; ell<nOrders; ell++) {
                const string progress = cbl::conv(double(nn)/nIter*100, cbl::par::fDP2);
                nn += 1;
                cout << r12 << "   " << r13 <<  "   " << ell << "  " << progress << "%           \r";
                cout.flush();

                fout1 << " " << model_zeta_ell_term_0(r12, r13, ell, xi_interp[0]);
                fout2 << " " << model_zeta_ell_term_0(r12_min, r12_max, r13_min, r13_max, ell, xi_interp[0]);
                fout3 << " " << model_zeta_ell_term_1(r12, r13, ell, xi_interp[1], xi_interp[2]);
                fout4 << " " << model_zeta_ell_term_1(r12_min, r12_max, r13_min, r13_max, ell, xi_interp[1], xi_interp[2]);
                fout5 << " " << model_zeta_ell_term_2(r12, r13, ell, xi_interp[3]);
                fout6 << " " << model_zeta_ell_term_2(r12_min, r12_max, r13_min, r13_max, ell, xi_interp[3]);
            }

            fout1 << endl;
            fout2 << endl;
            fout3 << endl;
            fout4 << endl;
            fout5 << endl;
            fout6 << endl;
        }
    }


    fout1.clear(); fout1.close();
    fout2.clear(); fout2.close();
    fout3.clear(); fout3.close();
    fout4.clear(); fout4.close();
    fout5.clear(); fout5.close();
    fout6.clear(); fout6.close();

    return 0;
}
