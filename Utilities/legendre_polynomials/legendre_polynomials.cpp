#include "CBL.h"
#include "ParameterFile.h"

using namespace std;
using namespace cbl;

// these two variables contain the name of the CosmoBolognaLib
// directory and the name of the current directory (useful when
// launching the code on remote systems)
std::string cbl::par::DirCosmo = DIRCOSMO, cbl::par::DirLoc = DIRL;

// ====================================================================================================


int main (int argc, char **argv) {

  if (argc != 2)
    cout << "Wrong number of inputs!" << endl;

  cbl::glob::ParameterFile parameter_file;
  parameter_file.read(string(argv[1]));

  double rMin = atof(parameter_file.get_key("rMin", "0").c_str());
  double rMax = atof(parameter_file.get_key("rMax", "160").c_str());
  double deltaR = atof(parameter_file.get_key("binSize", "10").c_str());
  size_t nOrders = atof(parameter_file.get_key("LMAX", "20").c_str())+1;
  string dir_output = parameter_file.get_key("dir_output", "../../tables/legendre_polynomials/");
  makeDir(dir_output);

  size_t nBins = int((rMax-rMin)/deltaR);

  // Compute the number of integrals
  int nInt = 0;

  for (size_t i=0; i<nBins; i++) {
    double r12_max = rMin+(i+1)*deltaR;

    for (size_t j=i; j<nBins; j++) {
      double r13_min = rMin+j*deltaR;
      double r13_max = rMin+(j+1)*deltaR;

      double r23_min = max(0., r13_min-r12_max);
      double r23_max = r13_max+r12_max;
      size_t nR23 = ((r23_max-r23_min)/deltaR);
      for (size_t k=0; k<nR23; k++) {
	for (size_t ell=0; ell<nOrders; ell++) {
	  nInt += 1;
	}
      }
    }
  }

  // Compute integrals
  int ndim = 3;
  std::vector<std::vector<double>> integration_limits(ndim);

  string file_out = dir_output + "legendre_polynomials_eff_"+conv(deltaR, par::fDP0)+".dat";
  ofstream fout(file_out.c_str());

  cout << "I'm averaging legendre polynomials up to order " << nOrders-1;
  cout << ". This requires the computation of  " << nInt  << " 3D integrals." << endl;
  cout << "Output will be written in " << file_out << endl;

  fout << "# r12 r13 r23";

  for (size_t ell=0; ell<nOrders; ell++)
    fout << " average_Leg_" << ell;

  fout << " normalization" << endl;

  // Compute the number of integrals
  int nn = 0;

  cout << "r12" << "   " << "r13" << "   " << "r23" << "   " << "ell" << "  " << "progress" << "%           " << endl;
  for (size_t i=0; i<nBins; i++) {
    double r12_min = rMin+i*deltaR;
    double r12_max = rMin+(i+1)*deltaR;
    double r12 = rMin+(i+0.5)*deltaR;
    integration_limits[0] = {pow(r12_min, 3), pow(r12_max, 3)};

    for (size_t j=i; j<nBins; j++) {
      double r13_min = rMin+j*deltaR;
      double r13_max = rMin+(j+1)*deltaR;
      integration_limits[1] = {pow(r13_min, 3), pow(r13_max, 3)};

      double r13 = rMin+(j+0.5)*deltaR;

      double r23_min = max(0., r13_min-r12_max);
      double r23_max = r13_max+r12_max;
      size_t nR23 = ((r23_max-r23_min)/deltaR);

      integration_limits[2] = {pow(r23_min, 3), pow(r23_max, 3)};

      for (size_t k=0; k<nR23; k++) {

	double _min = r23_min+k*deltaR;
	double _max = r23_min+(k+1)*deltaR;
	integration_limits[2] = {pow(_min, 3), pow(_max, 3)};
	double r23 = 0.5*(_max+_min);
	fout << setprecision(20) << r12 << " " << r13 << " " << r23;
	double norm = 1; 

	for (size_t ell=0; ell<nOrders; ell++) {
	  const string progress = cbl::conv(double(nn)/nInt*100, cbl::par::fDP2);
	  nn += 1;
	  cout << r12 << "   " << r13 << "   " << r23 << "   " << ell << "  " << progress << "%           \r";
	  cout.flush();

	  auto integrand = [&] (const vector<double> xx) {
	    return cbl::three_spherical_bessel_integral(pow(xx[0], 1./3), pow(xx[1], 1./3), pow(xx[2], 1./3), ell, ell, 0);
	  };
	  cbl::wrapper::cuba::CUBAwrapper CW(integrand, ndim);
	  CW.inputs().EPSREL=1.e-6;
	  double integral = CW.IntegrateCuhre(integration_limits);
	  norm = (ell!=0) ? norm : integral;

	  fout << " " << integral/norm;
	}
	fout << " " << norm << endl;
      }
    }
  }

  fout.clear(); fout.close();

  cout << endl;
  cout << "Done!" << endl;
  return 0;
}

