#include "EigenWrapper.h"
#include "bk_modelling.h"

// these two variables contain the name of the CosmoBolognaLib
// directory and the name of the current directory (useful when
// launching the code on remote systems)
std::string cbl::par::DirCosmo = DIRCOSMO, cbl::par::DirLoc = DIRL;

using namespace std;
using namespace cbl;

int main(int argc, char **argv) {

  if (argc != 2){
    coutCBL << "You should pass a parameter file in input!" << endl;
    exit(-1);
  }

  // define the input parameter file name
  string parameter_file = argv[1];

  // construct the object ReadParameter
  glob::ReadParameters parameters {parameter_file};

  // Input parameters
  double kmin = parameters.find<double>("kmin");
  double kmax = parameters.find<double>("kmax");
  size_t nK = parameters.find<int>("nK");
  size_t nMu = parameters.find<int>("nMu");
  size_t nOrders = parameters.find<int>("nOrders");
  string dir_output = parameters.find<string>("dir_output");
  string prefix = parameters.find<string>("prefix");
  cout << kmin << endl;

  string out_dir =  cbl::fullpath(dir_output);
  makeDir(out_dir, "/");

  vector<double> xx(nMu), wi(nMu);

  gsl_integration_glfixed_table * table = gsl_integration_glfixed_table_alloc(nMu);  
  for (size_t i=0; i<nMu; i++)
    gsl_integration_glfixed_point(-1, 1, i, &xx[i], &wi[i], table);
  gsl_integration_glfixed_table_free(table);

  // LegPols

  Eigen::MatrixXd leg_pols_matrix(nMu, nOrders);

  for (size_t i=0; i<nMu; i++)
    for (size_t ell =0; ell<nOrders; ell++) {
      leg_pols_matrix(i, ell) = cbl::legendre_polynomial(xx[i], ell) * wi[i] * (2.*ell +1) * 0.5;
    }

  // Power spectrum
  // -----------------------------------------------------------------
  // ---------------- setting the cosmological parameters ------------
  // -----------------------------------------------------------------

  const double OmegaM = parameters.find<double>("OmegaM");
  const double Omega_b = parameters.find<double>("Omega_b");
  const double Omega_nu = parameters.find<double>("Omega_nu");
  const double massless_neutrinos = parameters.find<double>("massless_neutrinos");
  const int massive_neutrinos = parameters.find<int>("massive_neutrinos"); 
  const double OmegaL = parameters.find<double>("OmegaL");
  const double Omega_radiation = parameters.find<double>("Omega_radiation");
  const double hh = parameters.find<double>("hh");
  const double scalar_amp = parameters.find<double>("scalar_amp");
  const double scalar_pivot = parameters.find<double>("scalar_pivot");
  const double n_s = parameters.find<double>("n_s");
  const double wa = parameters.find<double>("wa");
  const double w0 = parameters.find<double>("w0");   
  const double sigma8 = parameters.find<double>("sigma8");

  cbl::cosmology::Cosmology cosmology {OmegaM, Omega_b, Omega_nu, massless_neutrinos, massive_neutrinos, OmegaL, Omega_radiation, hh, scalar_amp, scalar_pivot, n_s, w0, wa};
  cosmology.set_sigma8(sigma8);

  const double redshift = parameters.find<double>("redshift"); 
  const double alpha = parameters.find<double>("alpha");

  std::vector<double> kk = cbl::logarithmic_bin_vector(1024, 1.e-6, 1.e3);
  std::vector<double> pkCAMB = cosmology.Pk_DM (kk, "CAMB", false, redshift, "./");
  std::vector<double> pkEH = cosmology.Pk_DM (kk, "EisensteinHu", false, redshift, "./");
  std::vector<double> pkNW = cosmology.Pk_DM_NoWiggles_gaussian (kk, pkCAMB, pkEH, 0.25, "gaussian_1d");
  
  //The BAO damping factor, set this to zero to use the linear power spectrum
  cout << "Here"<<endl;
  double sigmaNL = parameters.find<double>("sigmaNL");

  if (sigmaNL <0)
      sigmaNL = sqrt(get_sigma_squared(cbl::glob::FuncGrid(kk, pkNW, "Spline"), 1.e-5));

  cout<<sigmaNL<<endl;

  vector<double> pk(kk.size(), 0);

  string pk_file = out_dir + "pk.dat";
  ofstream fout(pk_file.c_str());
  for (size_t i=0; i<kk.size(); i++) {
    pk[i] = pkNW[i]+exp(-kk[i]*kk[i]*sigmaNL*sigmaNL)*(pkCAMB[i]-pkNW[i]);
    fout << kk[i] << " " << pk[i] << " " << pkCAMB[i] << " " << pkNW[i] << " " << pkEH[i] << endl;
  }
  fout.close();

  const glob::FuncGrid interp_pk (kk, pk, "Spline");

  // K1-K2 grid

  vector<double> k1 = logarithmic_bin_vector(nK, kmin, kmax);
  vector<double> k2 = k1;

  size_t nK2 = nK*nK;

  Eigen::MatrixXd matrix_ref(nK2, nMu);
  
  vector<Eigen::MatrixXd> bk_matrix(14, matrix_ref);

  vector<vector<BkTerm>> bk_terms = {{B1_12, B1_13, B1_23}, 
    {B2_12, B2_13, B2_23},
    {B3_12, B3_13, B3_23},
    {B4_12, B4_13, B4_23},
    {B5_12, B5_13, B5_23},
    {B6_12, B6_13, B6_23},
    {B7_12, B7_13, B7_23},
    {B8_12, B8_13, B8_23},
    {B9_12, B9_13, B9_23},
    {B10_12, B10_13, B10_23},
    {B11_12, B11_13, B11_23},
    {B12_12, B12_13, B12_23},
    {B13_12, B13_13, B13_23},
    {B14_12, B14_13, B14_23}};

  coutCBL<< "Starting computation"<< endl;
  for (size_t i=0; i<nK; i++) {
      double _k1 = k1[i]/alpha;
      double pk1 = interp_pk(_k1);

      for (size_t j=i; j<nK; j++) {
          double _k2 = k2[j]/alpha;
          double pk2 = interp_pk(_k2);

          for (size_t k=0; k<nMu; k++) {
              double k3 = sqrt(_k1*_k1 + _k2*_k2 + 2*_k1*_k2*xx[k]);
              double pk3 = interp_pk(k3);
              double mu13 = (_k2 * _k2 - _k1 * _k1 - k3 * k3)/(2*_k1*k3);
              double mu23 = (_k1 * _k1 - _k2 * _k2 - k3 * k3)/(2*_k2*k3);

              for (size_t f=0; f<14; f++) {
                  bk_matrix[f](i*nK+j, k) = 2*(bk_terms[f][0](_k1, _k2, xx[k]) * pk1 * pk2 + 
                          bk_terms[f][0](_k1, k3, mu13)*pk1*pk3 + bk_terms[f][0](_k2, k3, mu23)*pk2*pk3);
                  bk_matrix[f](j*nK+i, k) = bk_matrix[f](i*nK+j, k);
              }
          }
      }
  }

  for (size_t i=0; i<14; i++)  {
    auto bk_ell = bk_matrix[i]*leg_pols_matrix/pow(alpha, 6);
    write_bk_table(k1, bk_ell, out_dir, prefix+"_bk_"+conv(i, par::fINT)+"_alpha_"+conv(alpha, par::fDP2)+".fits");
  }

  coutCBL << "Done!" << endl;
  
  return 0;
}
