

// ===================================================
    
    
double B1_12 (const double k1, const double k2, const double x)
{
    return (1.0/2.0)*k1*x/k2 + (2.0/7.0)*std::pow(x, 2) + 5.0/7.0 + (1.0/2.0)*k2*x/k1;
}


// ===================================================
    
    
double B1_13 (const double k1, const double k2, const double x)
{
    return (1.0/14.0)*std::pow(k2, 2)*(10*k1*(1 - std::pow(x, 2)) - 7*k1 - 7*k2*x)/(k1*(std::pow(k1, 2) + 2*k1*k2*x + std::pow(k2, 2)));
}


// ===================================================
    
    
double B1_23 (const double k1, const double k2, const double x)
{
    return (1.0/14.0)*std::pow(k1, 2)*(-7*k1*x + 10*k2*(1 - std::pow(x, 2)) - 7*k2)/(k2*(std::pow(k1, 2) + 2*k1*k2*x + std::pow(k2, 2)));
}


// ===================================================
    
    
double B4_12 (const double k1, const double k2, const double x)
{
    return 1.0/2.0;
}


// ===================================================
    
    
double B4_13 (const double k1, const double k2, const double x)
{
    return 1.0/2.0;
}


// ===================================================
    
    
double B4_23 (const double k1, const double k2, const double x)
{
    return 1.0/2.0;
}


// ===================================================
    
    
double B7_12 (const double k1, const double k2, const double x)
{
    return (1.0/2.0)*std::pow(x, 2) - 1.0/2.0;
}


// ===================================================
    
    
double B7_13 (const double k1, const double k2, const double x)
{
    return -1.0/2.0*std::pow(k2, 2)*(1 - std::pow(x, 2))/(std::pow(k1, 2) + 2*k1*k2*x + std::pow(k2, 2));
}


// ===================================================
    
    
double B7_23 (const double k1, const double k2, const double x)
{
    return -1.0/2.0*std::pow(k1, 2)*(1 - std::pow(x, 2))/(std::pow(k1, 2) + 2*k1*k2*x + std::pow(k2, 2));
}


// ===================================================
    
    
double B72_12 (const double k1, const double k2, const double x)
{
    return (1.0/2.0)*std::pow(x, 2) - 1.0/6.0;
}


// ===================================================
    
    
double B72_13 (const double k1, const double k2, const double x)
{
    return (1.0/6.0)*(-std::pow(k1, 2) - 2*k1*k2*x - std::pow(k2, 2) + 3*std::pow(k1 + k2*x, 2))/(std::pow(k1, 2) + 2*k1*k2*x + std::pow(k2, 2));
}


// ===================================================
    
    
double B72_23 (const double k1, const double k2, const double x)
{
    return (1.0/6.0)*(-std::pow(k1, 2) - 2*k1*k2*x - std::pow(k2, 2) + 3*std::pow(k1*x + k2, 2))/(std::pow(k1, 2) + 2*k1*k2*x + std::pow(k2, 2));
}
