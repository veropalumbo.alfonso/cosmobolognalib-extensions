

// ===================================================
    
    
double B2_12 (const double k1, const double k2, const double x)
{
    return (1.0/2.0)*k1*x/k2 + (8.0/21.0)*std::pow(x, 2) + 13.0/21.0 + (1.0/2.0)*k2*x/k1;
}


// ===================================================
    
    
double B2_13 (const double k1, const double k2, const double x)
{
    return -1.0/42.0*std::pow(k2, 2)*(26*k1*std::pow(x, 2) - 5*k1 + 21*k2*x)/(k1*(std::pow(k1, 2) + 2*k1*k2*x + std::pow(k2, 2)));
}


// ===================================================
    
    
double B2_23 (const double k1, const double k2, const double x)
{
    return -1.0/42.0*std::pow(k1, 2)*(21*k1*x + 26*k2*std::pow(x, 2) - 5*k2)/(k2*(std::pow(k1, 2) + 2*k1*k2*x + std::pow(k2, 2)));
}


// ===================================================
    
    
double B3_12 (const double k1, const double k2, const double x)
{
    return (1.0/210.0)*(28*std::pow(k1, 4)*std::pow(x, 3) + 35*std::pow(k1, 4)*x + 52*std::pow(k1, 3)*k2*std::pow(x, 4) + 166*std::pow(k1, 3)*k2*std::pow(x, 2) + 34*std::pow(k1, 3)*k2 + 16*std::pow(k1, 2)*std::pow(k2, 2)*std::pow(x, 5) + 200*std::pow(k1, 2)*std::pow(k2, 2)*std::pow(x, 3) + 162*std::pow(k1, 2)*std::pow(k2, 2)*x + 52*k1*std::pow(k2, 3)*std::pow(x, 4) + 166*k1*std::pow(k2, 3)*std::pow(x, 2) + 34*k1*std::pow(k2, 3) + 28*std::pow(k2, 4)*std::pow(x, 3) + 35*std::pow(k2, 4)*x)/(k1*k2*(std::pow(k1, 2) + 2*k1*k2*x + std::pow(k2, 2)));
}


// ===================================================
    
    
double B3_13 (const double k1, const double k2, const double x)
{
    return (1.0/210.0)*std::pow(k2, 2)*(-264*std::pow(k1, 5)*std::pow(x, 2) + 12*std::pow(k1, 5) - 792*std::pow(k1, 4)*k2*std::pow(x, 3) - 216*std::pow(k1, 4)*k2*x - 632*std::pow(k1, 3)*std::pow(k2, 2)*std::pow(x, 4) - 938*std::pow(k1, 3)*std::pow(k2, 2)*std::pow(x, 2) - 5*std::pow(k1, 3)*std::pow(k2, 2) - 104*std::pow(k1, 2)*std::pow(k2, 3)*std::pow(x, 5) - 860*std::pow(k1, 2)*std::pow(k2, 3)*std::pow(x, 3) - 233*std::pow(k1, 2)*std::pow(k2, 3)*x - 116*k1*std::pow(k2, 4)*std::pow(x, 4) - 324*k1*std::pow(k2, 4)*std::pow(x, 2) - k1*std::pow(k2, 4) - 28*std::pow(k2, 5)*std::pow(x, 3) - 35*std::pow(k2, 5)*x)/(k1*std::pow(std::pow(k1, 2) + 2*k1*k2*x + std::pow(k2, 2), 2)*(4*std::pow(k1, 2) + 4*k1*k2*x + std::pow(k2, 2)));
}


// ===================================================
    
    
double B3_23 (const double k1, const double k2, const double x)
{
    return (1.0/210.0)*std::pow(k1, 2)*(-28*std::pow(k1, 5)*std::pow(x, 3) - 35*std::pow(k1, 5)*x - 116*std::pow(k1, 4)*k2*std::pow(x, 4) - 324*std::pow(k1, 4)*k2*std::pow(x, 2) - std::pow(k1, 4)*k2 - 104*std::pow(k1, 3)*std::pow(k2, 2)*std::pow(x, 5) - 860*std::pow(k1, 3)*std::pow(k2, 2)*std::pow(x, 3) - 233*std::pow(k1, 3)*std::pow(k2, 2)*x - 632*std::pow(k1, 2)*std::pow(k2, 3)*std::pow(x, 4) - 938*std::pow(k1, 2)*std::pow(k2, 3)*std::pow(x, 2) - 5*std::pow(k1, 2)*std::pow(k2, 3) - 792*k1*std::pow(k2, 4)*std::pow(x, 3) - 216*k1*std::pow(k2, 4)*x - 264*std::pow(k2, 5)*std::pow(x, 2) + 12*std::pow(k2, 5))/(k2*std::pow(std::pow(k1, 2) + 2*k1*k2*x + std::pow(k2, 2), 2)*(std::pow(k1, 2) + 4*k1*k2*x + 4*std::pow(k2, 2)));
}


// ===================================================
    
    
double B5_12 (const double k1, const double k2, const double x)
{
    return 1.0/3.0;
}


// ===================================================
    
    
double B5_13 (const double k1, const double k2, const double x)
{
    return 1.0/3.0;
}


// ===================================================
    
    
double B5_23 (const double k1, const double k2, const double x)
{
    return 1.0/3.0;
}


// ===================================================
    
    
double B6_12 (const double k1, const double k2, const double x)
{
    return (1.0/15.0)*std::pow(x, 2) + 1.0/30.0;
}


// ===================================================
    
    
double B6_13 (const double k1, const double k2, const double x)
{
    return ((1.0/10.0)*std::pow(k1, 2) + (1.0/5.0)*k1*k2*x + (1.0/15.0)*std::pow(k2, 2)*std::pow(x, 2) + (1.0/30.0)*std::pow(k2, 2))/(std::pow(k1, 2) + 2*k1*k2*x + std::pow(k2, 2));
}


// ===================================================
    
    
double B6_23 (const double k1, const double k2, const double x)
{
    return ((1.0/15.0)*std::pow(k1, 2)*std::pow(x, 2) + (1.0/30.0)*std::pow(k1, 2) + (1.0/5.0)*k1*k2*x + (1.0/10.0)*std::pow(k2, 2))/(std::pow(k1, 2) + 2*k1*k2*x + std::pow(k2, 2));
}


// ===================================================
    
    
double B8_12 (const double k1, const double k2, const double x)
{
    return (2.0/3.0)*std::pow(x, 2) - 2.0/9.0;
}


// ===================================================
    
    
double B8_13 (const double k1, const double k2, const double x)
{
    return (2.0/9.0)*(2*std::pow(k1, 2) + 4*k1*k2*x + 3*std::pow(k2, 2)*std::pow(x, 2) - std::pow(k2, 2))/(std::pow(k1, 2) + 2*k1*k2*x + std::pow(k2, 2));
}


// ===================================================
    
    
double B8_23 (const double k1, const double k2, const double x)
{
    return (2.0/9.0)*(3*std::pow(k1, 2)*std::pow(x, 2) - std::pow(k1, 2) + 4*k1*k2*x + 2*std::pow(k2, 2))/(std::pow(k1, 2) + 2*k1*k2*x + std::pow(k2, 2));
}


// ===================================================
    
    
double B9_12 (const double k1, const double k2, const double x)
{
    return (2.0/15.0)*std::pow(x, 4) + (1.0/45.0)*std::pow(x, 2) - 1.0/45.0;
}


// ===================================================
    
    
double B9_13 (const double k1, const double k2, const double x)
{
    return (1.0/45.0)*(2*std::pow(k1, 2) + 4*k1*k2*x + 3*std::pow(k2, 2)*std::pow(x, 2) - std::pow(k2, 2))*(3*std::pow(k1, 2) + 6*k1*k2*x + 2*std::pow(k2, 2)*std::pow(x, 2) + std::pow(k2, 2))/std::pow(std::pow(k1, 2) + 2*k1*k2*x + std::pow(k2, 2), 2);
}


// ===================================================
    
    
double B9_23 (const double k1, const double k2, const double x)
{
    return (1.0/45.0)*(2*std::pow(k1, 2)*std::pow(x, 2) + std::pow(k1, 2) + 6*k1*k2*x + 3*std::pow(k2, 2))*(3*std::pow(k1, 2)*std::pow(x, 2) - std::pow(k1, 2) + 4*k1*k2*x + 2*std::pow(k2, 2))/std::pow(std::pow(k1, 2) + 2*k1*k2*x + std::pow(k2, 2), 2);
}


// ===================================================
    
    
double B10_12 (const double k1, const double k2, const double x)
{
    return (1.0/6.0)*k1*x/k2 + 1.0/3.0 + (1.0/6.0)*k2*x/k1;
}


// ===================================================
    
    
double B10_13 (const double k1, const double k2, const double x)
{
    return (1.0/6.0)*(4*std::pow(k1, 3) + 8*std::pow(k1, 2)*k2*x + 2*k1*std::pow(k2, 2)*std::pow(x, 2) + 3*k1*std::pow(k2, 2) + std::pow(k2, 3)*x)/(k1*(std::pow(k1, 2) + 2*k1*k2*x + std::pow(k2, 2)));
}


// ===================================================
    
    
double B10_23 (const double k1, const double k2, const double x)
{
    return (1.0/6.0)*(std::pow(k1, 3)*x + 2*std::pow(k1, 2)*k2*std::pow(x, 2) + 3*std::pow(k1, 2)*k2 + 8*k1*std::pow(k2, 2)*x + 4*std::pow(k2, 3))/(k2*(std::pow(k1, 2) + 2*k1*k2*x + std::pow(k2, 2)));
}


// ===================================================
    
    
double B11_12 (const double k1, const double k2, const double x)
{
    return (3.0/10.0)*k1*x/k2 + (4.0/15.0)*std::pow(x, 2) + 1.0/3.0 + (3.0/10.0)*k2*x/k1;
}


// ===================================================
    
    
double B11_13 (const double k1, const double k2, const double x)
{
    return (1.0/30.0)*(36*std::pow(k1, 3) + 72*std::pow(k1, 2)*k2*x + 26*k1*std::pow(k2, 2)*std::pow(x, 2) + 19*k1*std::pow(k2, 2) + 9*std::pow(k2, 3)*x)/(k1*(std::pow(k1, 2) + 2*k1*k2*x + std::pow(k2, 2)));
}


// ===================================================
    
    
double B11_23 (const double k1, const double k2, const double x)
{
    return (1.0/30.0)*(9*std::pow(k1, 3)*x + 26*std::pow(k1, 2)*k2*std::pow(x, 2) + 19*std::pow(k1, 2)*k2 + 72*k1*std::pow(k2, 2)*x + 36*std::pow(k2, 3))/(k2*(std::pow(k1, 2) + 2*k1*k2*x + std::pow(k2, 2)));
}


// ===================================================
    
    
double B12_12 (const double k1, const double k2, const double x)
{
    return (2.0/35.0)*k1*std::pow(x, 3)/k2 + (11.0/70.0)*k1*x/k2 + (12.0/35.0)*std::pow(x, 2) + 3.0/35.0 + (2.0/35.0)*k2*std::pow(x, 3)/k1 + (11.0/70.0)*k2*x/k1;
}


// ===================================================
    
    
double B12_13 (const double k1, const double k2, const double x)
{
    return (1.0/70.0)*(60*std::pow(k1, 5) + 240*std::pow(k1, 4)*k2*x + 302*std::pow(k1, 3)*std::pow(k2, 2)*std::pow(x, 2) + 73*std::pow(k1, 3)*std::pow(k2, 2) + 124*std::pow(k1, 2)*std::pow(k2, 3)*std::pow(x, 3) + 161*std::pow(k1, 2)*std::pow(k2, 3)*x + 8*k1*std::pow(k2, 4)*std::pow(x, 4) + 80*k1*std::pow(k2, 4)*std::pow(x, 2) + 17*k1*std::pow(k2, 4) + 4*std::pow(k2, 5)*std::pow(x, 3) + 11*std::pow(k2, 5)*x)/(k1*std::pow(std::pow(k1, 2) + 2*k1*k2*x + std::pow(k2, 2), 2));
}


// ===================================================
    
    
double B12_23 (const double k1, const double k2, const double x)
{
    return (1.0/70.0)*(4*std::pow(k1, 5)*std::pow(x, 3) + 11*std::pow(k1, 5)*x + 8*std::pow(k1, 4)*k2*std::pow(x, 4) + 80*std::pow(k1, 4)*k2*std::pow(x, 2) + 17*std::pow(k1, 4)*k2 + 124*std::pow(k1, 3)*std::pow(k2, 2)*std::pow(x, 3) + 161*std::pow(k1, 3)*std::pow(k2, 2)*x + 302*std::pow(k1, 2)*std::pow(k2, 3)*std::pow(x, 2) + 73*std::pow(k1, 2)*std::pow(k2, 3) + 240*k1*std::pow(k2, 4)*x + 60*std::pow(k2, 5))/(k2*std::pow(std::pow(k1, 2) + 2*k1*k2*x + std::pow(k2, 2), 2));
}


// ===================================================
    
    
double B13_12 (const double k1, const double k2, const double x)
{
    return (2.0/63.0)*k1*std::pow(x, 3)/k2 + (1.0/42.0)*k1*x/k2 + (8.0/315.0)*std::pow(x, 4) + (8.0/105.0)*std::pow(x, 2) + 1.0/105.0 + (2.0/63.0)*k2*std::pow(x, 3)/k1 + (1.0/42.0)*k2*x/k1;
}


// ===================================================
    
    
double B13_13 (const double k1, const double k2, const double x)
{
    return (1.0/630.0)*(140*std::pow(k1, 5) + 560*std::pow(k1, 4)*k2*x + 750*std::pow(k1, 3)*std::pow(k2, 2)*std::pow(x, 2) + 125*std::pow(k1, 3)*std::pow(k2, 2) + 380*std::pow(k1, 2)*std::pow(k2, 3)*std::pow(x, 3) + 285*std::pow(k1, 2)*std::pow(k2, 3)*x + 56*k1*std::pow(k2, 4)*std::pow(x, 4) + 168*k1*std::pow(k2, 4)*std::pow(x, 2) + 21*k1*std::pow(k2, 4) + 20*std::pow(k2, 5)*std::pow(x, 3) + 15*std::pow(k2, 5)*x)/(k1*std::pow(std::pow(k1, 2) + 2*k1*k2*x + std::pow(k2, 2), 2));
}


// ===================================================
    
    
double B13_23 (const double k1, const double k2, const double x)
{
    return (1.0/630.0)*(20*std::pow(k1, 5)*std::pow(x, 3) + 15*std::pow(k1, 5)*x + 56*std::pow(k1, 4)*k2*std::pow(x, 4) + 168*std::pow(k1, 4)*k2*std::pow(x, 2) + 21*std::pow(k1, 4)*k2 + 380*std::pow(k1, 3)*std::pow(k2, 2)*std::pow(x, 3) + 285*std::pow(k1, 3)*std::pow(k2, 2)*x + 750*std::pow(k1, 2)*std::pow(k2, 3)*std::pow(x, 2) + 125*std::pow(k1, 2)*std::pow(k2, 3) + 560*k1*std::pow(k2, 4)*x + 140*std::pow(k2, 5))/(k2*std::pow(std::pow(k1, 2) + 2*k1*k2*x + std::pow(k2, 2), 2));
}


// ===================================================
    
    
double B14_12 (const double k1, const double k2, const double x)
{
    return (1.0/490.0)*(7*std::pow(k1, 2)*x + 8*k1*k2*(std::pow(x, 2) - 1) + 14*k1*k2 + 7*std::pow(k2, 2)*x)*(4*std::pow(k1, 2)*std::pow(x, 2) + std::pow(k1, 2) + 4*k1*k2*std::pow(x, 3) + 6*k1*k2*x + 4*std::pow(k2, 2)*std::pow(x, 2) + std::pow(k2, 2))/(k1*k2*(std::pow(k1, 2) + 2*k1*k2*x + std::pow(k2, 2)));
}


// ===================================================
    
    
double B14_13 (const double k1, const double k2, const double x)
{
    return -1.0/490.0*std::pow(k2, 2)*(6*k1*(std::pow(x, 2) - 1) + 7*k1 + 7*k2*x)*(20*std::pow(k1, 4) + 60*std::pow(k1, 3)*k2*x + 52*std::pow(k1, 2)*std::pow(k2, 2)*std::pow(x, 2) + 13*std::pow(k1, 2)*std::pow(k2, 2) + 12*k1*std::pow(k2, 3)*std::pow(x, 3) + 18*k1*std::pow(k2, 3)*x + 4*std::pow(k2, 4)*std::pow(x, 2) + std::pow(k2, 4))/(k1*std::pow(std::pow(k1, 2) + 2*k1*k2*x + std::pow(k2, 2), 2)*(4*std::pow(k1, 2) + 4*k1*k2*x + std::pow(k2, 2)));
}


// ===================================================
    
    
double B14_23 (const double k1, const double k2, const double x)
{
    return -1.0/490.0*std::pow(k1, 2)*(7*k1*x + 6*k2*(std::pow(x, 2) - 1) + 7*k2)*(4*std::pow(k1, 4)*std::pow(x, 2) + std::pow(k1, 4) + 12*std::pow(k1, 3)*k2*std::pow(x, 3) + 18*std::pow(k1, 3)*k2*x + 52*std::pow(k1, 2)*std::pow(k2, 2)*std::pow(x, 2) + 13*std::pow(k1, 2)*std::pow(k2, 2) + 60*k1*std::pow(k2, 3)*x + 20*std::pow(k2, 4))/(k2*std::pow(std::pow(k1, 2) + 2*k1*k2*x + std::pow(k2, 2), 2)*(std::pow(k1, 2) + 4*k1*k2*x + 4*std::pow(k2, 2)));
}
