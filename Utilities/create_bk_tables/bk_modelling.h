#include "CBL.h"
#include "bk_realspace.h"
#include "bk_zspace.h"

using namespace std;

typedef function<double(double, double, double)> BkTerm;


//=================================================================


double get_sigma_squared(const cbl::glob::FuncGrid pk_nw_interp, const double kmin, const double losc = 110, const double ks = 0.2) 
{
    auto integrand = [&] (const double q) {
        return pk_nw_interp(q) * ( 1 - cbl::j0(q*losc) + 2*cbl::j2(q*losc) );
    };

    return cbl::wrapper::gsl::GSL_integrate_cquad(integrand, kmin, ks, 1.e-5)/(6*pow(cbl::par::pi, 2));
}


// ===================================================


Eigen::MatrixXd bispectrum_term(const double k1, const double k2, const vector<double> xx, const cbl::glob::FuncGrid &interp_pk, BkTerm Bn_12, BkTerm Bn_13, BkTerm Bn_23)
{
  double pk1 = interp_pk(k1);
  double pk2 = interp_pk(k2);

  Eigen::MatrixXd bk_term(1, xx.size());

  for (size_t i=0;i<xx.size(); i++) {
    double k3 = sqrt(k1*k1 + k2*k2 + 2*k1*k2*xx[i]);
    double pk3 = interp_pk(k3);

    bk_term(0, i) = Bn_12(k1, k2, xx[i]) * pk1 * pk2 +
      Bn_13(k1, k2, xx[i]) * pk1 * pk3 +
      Bn_23(k2, k3, xx[i]) * pk2 * pk3;
  }

  return bk_term;
}


// ===================================================


void write_bk_table(const vector<double> k1, Eigen::MatrixXd bk_ell, const string dir_output, const string fits_file)
{
  // Write output on fits table
  
  size_t nK = k1.size();
  size_t nOrders = bk_ell.cols();

  vector<string> column_names = {"k1", "k2"};
  for (size_t i=0; i<nOrders; i++)
    column_names.push_back("L"+cbl::conv(i, cbl::par::fINT));

  vector<vector<double>> bk_table(2+nOrders, std::vector<double>(nK*nK, 0));

  for (size_t i=0; i<nK; i++){
    for (size_t j=0; j<nK; j++){
      size_t idx = i*nK+j;
      bk_table[0][idx] = k1[i];
      bk_table[1][idx] = k1[j];
      for (size_t ell=0; ell<nOrders; ell++)
	bk_table[2+ell][idx] = bk_ell(idx, ell);
    }
  }

  cout << "Writing fits"<< endl;

  string cmd = "rm -f "+dir_output+fits_file;
  if (system(cmd.c_str())) {}

  cbl::wrapper::ccfits::write_table_fits(dir_output, fits_file, column_names, bk_table);
}

