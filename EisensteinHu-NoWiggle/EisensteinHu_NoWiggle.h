#include "CBL.h"


#ifndef _EHNOW_
#define _EHNOW_

namespace cbl {

    namespace cosmology {

        class EisensteinHu_NoWiggles {

            protected:

                double m_hh;

                double m_omegam_h2;

                double m_sound_horizon;

                double m_alpha_gamma;

                double m_theta_square;

                double m_normalization;

                double m_ns;

                double m_get_alpha_gamma (const double hh, const double OmegaM, const double OmegaB);
                
                double m_get_sound_horizon (const double hh, const double OmegaM, const double OmegaB);

                double m_get_gamma_eff (const double kh);

                double m_get_q (const double kh);

                double m_get_transfer (const double kh);

                double m_get_normalization (const double sigma8);

            public:

                EisensteinHu_NoWiggles () {}

                EisensteinHu_NoWiggles (const double hh, const double OmegaM, const double OmegaB, const double ns, const double sigma8, const double T_CMB = 2.7255);

                EisensteinHu_NoWiggles (const Cosmology cosmo, const double redshift, const double T_CMB = 2.7255);

                ~EisensteinHu_NoWiggles () {}

                void set_cosmology (const double hh, const double OmegaM, const double OmegaB, const double ns, const double sigma8, const double T_CMB = 2.7255);

                double operator () (const double kh);

                std::vector<double> operator() (const std::vector<double> kh);

                void write_all (const std::vector<double> kh, const std::string file);
                
        };


// ========================================================================


        double EisensteinHu_NoWiggles::m_get_alpha_gamma (const double hh, const double OmegaM, const double OmegaB)
        {
            return 1-0.328 * log(431. * OmegaM * hh * hh) * OmegaB/OmegaM +
                     0.38  * log(22.3 * OmegaM * hh * hh) * pow(OmegaB/OmegaM, 2);
        }


// ========================================================================
        
        
        double EisensteinHu_NoWiggles::m_get_sound_horizon (const double hh, const double OmegaM, const double OmegaB)
        {
            return 44.5 * hh * log ( 9.83 / (OmegaM * hh * hh)) / 
                sqrt(1 + 10 * pow ( OmegaB * hh * hh, 3./4));
        }


// ========================================================================
        

        double EisensteinHu_NoWiggles::m_get_gamma_eff (const double kh)
        {
            return m_omegam_h2 * (m_alpha_gamma + (1-m_alpha_gamma) /
                    (1+pow(0.43 * kh * m_sound_horizon, 4) ) ) / m_hh;
        }


// ========================================================================
 
        
        double EisensteinHu_NoWiggles::m_get_q (const double kh)
        {
            return kh * m_theta_square  / m_get_gamma_eff(kh);
        }


// ========================================================================
 
        
        double EisensteinHu_NoWiggles::m_get_transfer (const double kh)
        {
            double q = m_get_q(kh);

            double L0 = log(2 * M_E + 1.8 * q);
            double C0 = 14.2 + 731./(1+62.5 * q);

            return L0 / (L0 + C0 * q * q);
        }


// ========================================================================
 
        
        double EisensteinHu_NoWiggles::m_get_normalization (const double sigma8)
        {
            auto integrand = [&] (const double log_k) {
                double kk = exp(log_k);
                double Tk = m_get_transfer(kk);
                double pk = pow(kk, m_ns) * pow(Tk, 2);
                return pow(kk, 3) * pk * pow(cbl::TopHat_WF ( kk * 8. ), 2);
            };

            double _min = log(1.e-5);
            double _max = log(10);

            double Int = 1./(2 * cbl::par::pi * cbl::par::pi) * cbl::wrapper::gsl::GSL_integrate_cquad(integrand, _min, _max, 1.e-6);

            return pow(sigma8, 2)/Int;
        }


// ========================================================================
        

        EisensteinHu_NoWiggles::EisensteinHu_NoWiggles (const double hh, const double OmegaM, const double OmegaB, const double As, const double ns, const double T_CMB) 
        {
            set_cosmology(hh, OmegaM, OmegaB, As, ns, T_CMB);
        }


// ========================================================================
        
        
        EisensteinHu_NoWiggles::EisensteinHu_NoWiggles (const Cosmology cosmology, const double redshift, const double T_CMB) 
        {
            double sigma8 = cosmology.sigma8_Pk("CAMB", redshift);
            set_cosmology(cosmology.hh(), cosmology.Omega_matter(), cosmology.Omega_baryon(), sigma8, cosmology.n_spec(), T_CMB);
        }


// ========================================================================


        void EisensteinHu_NoWiggles::set_cosmology (const double hh, const double OmegaM, const double OmegaB, const double sigma8, const double ns, const double T_CMB)
        {
            std::cout << hh << " " << OmegaM << " " << OmegaB << " " << sigma8 << " " << ns << std::endl;
            m_hh = hh;
            m_omegam_h2 = OmegaM * hh * hh;
            m_theta_square = pow(T_CMB/2.7, 2);
            m_alpha_gamma = m_get_alpha_gamma(hh, OmegaM, OmegaB);
            m_sound_horizon = m_get_sound_horizon(hh, OmegaM, OmegaB);
            m_ns = ns;
            m_normalization = m_get_normalization(sigma8);

        }
        

// ========================================================================

        double EisensteinHu_NoWiggles::operator() (const double kh)
        {
            double Tk = m_get_transfer(kh);

            return m_normalization * pow(kh, m_ns) * pow(Tk, 2);
        }

// ========================================================================

        std::vector<double> EisensteinHu_NoWiggles::operator() (const std::vector<double> kh)
        {
            std::vector<double> pk(kh.size());
            for (size_t i=0; i<kh.size(); i++)
                pk[i] = this->operator()(kh[i]);

            return pk;
        }

// ========================================================================
        void EisensteinHu_NoWiggles::write_all (const std::vector<double> kh, const std::string file)
        {
            std::ofstream fout(file.c_str());
            fout << "# kh q L0 C0 T Pk" << std::endl;
            for (size_t i=0; i<kh.size(); i++) {
                double q = m_get_q(kh[i]);
                double L0 = log(2 * M_E + 1.8 * q);
                double C0 = 14.2 + 731./(1+62.5 * q);
                double TT =  m_get_transfer(kh[i]);
                double pk = this->operator() (kh[i]);

               fout << kh[i] << " " << q << " " << L0 << " " << C0 << " " << TT << " " << pk << std::endl;
            }

            fout.clear(); fout.close();

        }
    }

}

#endif
