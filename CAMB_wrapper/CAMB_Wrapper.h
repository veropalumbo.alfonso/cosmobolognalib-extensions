// ====================================================================================================
#include "ParameterFile.h"

class CAMB_Wrapper : public cbl::glob::ParameterFile
{
    protected:
        std::string m_camb_dir;

        std::string m_command;


    public:

        CAMB_Wrapper () : cbl::glob::ParameterFile() {}

        ~CAMB_Wrapper () = default;

        CAMB_Wrapper (std::string camb_dir, std::string camb_parfile) : cbl::glob::ParameterFile(camb_parfile) 
        {
            m_camb_dir = camb_dir;

            m_command = m_camb_dir+"camb ";
        }

        void modify_parameters (const cbl::cosmology::Cosmology cosmo, const double redshift, const std::string output_root, const bool do_nonlinear=false, const double kmax=100)
        { 
            double hh2 = pow( cosmo.hh(), 2);
            double ombh2 = cosmo.Omega_baryon() * hh2;
            double omch2 = cosmo.Omega_CDM() * hh2;
            double omk = cosmo.Omega_k();
            double omnuh2 = cosmo.Omega_neutrinos() * hh2;
            double massless_nu = cosmo.massless_neutrinos();
            int massive_nu = cosmo.massive_neutrinos();
                
            set_key("output_root", output_root); 
            set_key("do_nonlinear", cbl::conv(do_nonlinear, cbl::par::fINT), false);
            set_key("hubble", cbl::conv(cosmo.hh()*100, cbl::par::fDP6));
            set_key("ombh2", cbl::conv(ombh2, cbl::par::fDP6));
            set_key("omch2", cbl::conv(omch2, cbl::par::fDP6));
            set_key("omk", cbl::conv(omk, cbl::par::fDP6));
            set_key("omnuh2", cbl::conv(omnuh2, cbl::par::fDP6));
            set_key("transfer_redshift(1)", cbl::conv(redshift, cbl::par::fDP6));
            set_key("massless_neutrinos", cbl::conv(massless_nu, cbl::par::fDP6));
            set_key("massive_neutrinos", cbl::conv(massive_nu, cbl::par::fINT));
            set_key("scalar_spectral_index(1)", cbl::conv(cosmo.n_spec(), cbl::par::fDP6));
            set_key("w", cbl::conv(cosmo.w0(), cbl::par::fDP6));
            set_key("wa", cbl::conv(cosmo.wa(), cbl::par::fDP6));
            if (cosmo.scalar_amp()>0) {
                set_key("scalar_amp(1)", cbl::conv(cosmo.scalar_amp(), cbl::par::ee3));
                set_key("pivot_scalar", cbl::conv(cosmo.scalar_pivot(), cbl::par::fDP6));
            }

            set_key("transfer_kmax", cbl::conv(kmax, cbl::par::fDP6));
            set_key("re_optical_depth", cbl::conv(cosmo.tau(), cbl::par::fDP6));
            set_key("feedback_level", "-1");
            set_key("print_sigma8", "F");
        }

        std::string run(const cbl::cosmology::Cosmology cosmo, const double zz, const std::string output_root, const bool do_nonlinear=false, const double kmax=100) 
        {
            modify_parameters(cosmo, zz, output_root, do_nonlinear, kmax);
            write(output_root+"params.ini");
            std::string command = m_command+output_root+"params.ini";
            if (system(command.c_str())) {}

            return output_root;
        }

        void get_Pk(std::vector<double> &kk, std::vector<double> &pk, const cbl::cosmology::Cosmology cosmo, const double zz, const std::string output_root, const bool do_nonlinear=false, const double kmax=100) 
        {
            modify_parameters(cosmo, zz, output_root, do_nonlinear, kmax);
            write(output_root+"_params.ini");
            std::string command = m_command+output_root+"_params.ini";
            if (system(command.c_str())) {}

            cbl::data::Table pk_table ("./", output_root+"_matterpower.dat", {"k/h", "Pk"}, {0, 1}, 1);
            kk = pk_table["k/h"];
            pk = pk_table["Pk"];

            command = "rm "+output_root+"*";
            if (system(command.c_str())) {}
        }


        cbl::glob::FuncGrid get_Pk(const cbl::cosmology::Cosmology cosmo, const double zz, const std::string output_root, const bool do_nonlinear=false, const double kmax=100)
        {
            modify_parameters(cosmo, zz, output_root, do_nonlinear, kmax);
            write(output_root+"_params.ini");
            std::string command = m_command+output_root+"_params.ini";
            if (system(command.c_str())) {}

            cbl::data::Table pk_table ("./", output_root+"_matterpower.dat", {"k/h", "Pk"}, {0, 1}, 1);
            
            command = "rm "+output_root+"*";
            if (system(command.c_str())) {}

            return cbl::glob::FuncGrid(pk_table["k/h"], pk_table["Pk"], "Spline");
        }

};

