#include "Catalogue.h"

#include "arrow/parquet/arrow/reader.h"

#ifndef __CATPARQ__
#define __CATPARQ__

// ============================================================================================


namespace cbl {


  namespace get_catalogue {

    Catalogue get_Catalogue_parquet (const ObjectType objectType,
	                   const CoordinateType coordinateType,
			   const std::string parquet_file,
			   const int ncols;
			   const std::vector<int> coordinate_columns = {0, 1, 2},
			   const int weight_column = -1,
			   const cosmology::Cosmology &cosm = {},
			   const CoordinateUnits inputUnits=CoordinateUnits::_radians_)
    {
      coutCBL << "Reading parquet file!" << endl;
      std::shared_ptr<arrow::io::ReadableFile> infile;

      PARQUET_ASSIGN_OR_THROW(
	  infile,
	  arrow::io::ReadableFile::Open(parquet_file));

      parquet::StreamReader os{parquet::ParquetFileReader::Open(infile)};

      vector<double> line_entries(ncols);
      vector<double> coord1, coord2, coord3, weight;

      while ( !os.eof() )
      {
	for (size_t i=0; i<ncols; i++) 
	{
	  os >> line_entries[i];
	}
	os >> parquet::EndRow;

	coord1.push_back(line_entries[coordinate_columns[0]]);
	coord2.push_back(line_entries[coordinate_columns[1]]);
	coord3.push_back(line_entries[coordinate_columns[2]]);
	weight.push_back( (weight_column<0) ? 1 : line_entries[weight_column]);
      }
      coutCBL << "Done!" << endl;

      return Catalogue(objectType, coordinateType, coord1, coord2, coord3, weight, cosm, inputUnits);
    }
}

#endif
