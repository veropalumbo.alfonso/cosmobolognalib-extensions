#include "PowerSpectrum_Halofit.h"

// ============================================================================


double pkmodel::PowerSpectrum_HALOFIT::m_sigma2_halofit (const double R, const cbl::glob::FuncGrid interp_Pk, const double kmin, const double kmax)
{
    double norm = 1./(2*cbl::par::pi*cbl::par::pi);

    auto integrand = [&] (const double log_k) {
        double kk = exp(log_k);
        double exp_term = exp ( -kk*kk*R*R);
        return pow(kk, 3) * interp_Pk(kk) * exp_term;
    };

    return cbl::wrapper::gsl::GSL_integrate_cquad(integrand, log(kmin), log(kmax), 1.e-6, 0., 10000)*norm;
}


// =============================================================================


double pkmodel::PowerSpectrum_HALOFIT::m_dsigma2_dR_halofit (const double R, const cbl::glob::FuncGrid interp_Pk, const double kmin, const double kmax)
{
    double norm = -2.*R/(2*cbl::par::pi*cbl::par::pi);

    auto integrand = [&] (const double log_k) {
        double kk = exp(log_k);
        double exp_term = exp ( -kk*kk*R*R);
        return pow(kk, 5) * interp_Pk(kk) * exp_term;
    };

    return cbl::wrapper::gsl::GSL_integrate_cquad(integrand, log(kmin), log(kmax), 1.e-6, 0., 10000)*norm;
}


// =============================================================================


double pkmodel::PowerSpectrum_HALOFIT::m_d2sigma2_dR2_halofit (const double R, const cbl::glob::FuncGrid interp_Pk, const double kmin, const double kmax)
{
    double norm = 1./(2*cbl::par::pi*cbl::par::pi);

    auto integrand = [&] (const double log_k) {
        double kk = exp(log_k);
        double exp_term = exp ( -kk*kk*R*R);
        return pow(kk, 3) * interp_Pk(kk) * exp_term * 2 * kk * kk * ( 2 * kk * kk * R * R -1);
    };

    return cbl::wrapper::gsl::GSL_integrate_cquad(integrand, log(kmin), log(kmax), 1.e-6, 0., 10000)*norm;
}


// ============================================================================


double pkmodel::PowerSpectrum_HALOFIT::m_k_sigma_inv (const double sigma8)
{
    (void)sigma8;
    auto func = [&] (const double RR) { return m_sigma2_halofit(RR, m_pk_linear, 1.e-5, 100); };
    return cbl::wrapper::gsl::GSL_root_brent(func, 1., 1.e-3,  1.e3, 1.e-5, 0);
}


// ============================================================================


double pkmodel::PowerSpectrum_HALOFIT::m_generic
(const std::vector<double> var, const std::vector<double> pars, const bool isLog10) const
{
    double val = 0;

    for (size_t i=0; i<var.size(); i++)
        val += var[i] * pars[i];

    return ( (isLog10) ? pow(10, val) : val);
}


// ============================================================================


void pkmodel::PowerSpectrum_HALOFIT::m_set_internal ()
{
    m_RR = m_k_sigma_inv(1.);

    double s2 = m_sigma2_halofit(m_RR, m_pk_linear, 1.e-5, 1.e3);
    double ds2 = m_dsigma2_dR_halofit(m_RR, m_pk_linear, 1.e-5, 1.e3);
    double d2s2 = m_d2sigma2_dR2_halofit(m_RR, m_pk_linear, 1.e-5, 1.e3);
    double dlns2 = m_RR*ds2/s2;
    double d2lns2 = m_RR*(s2*(m_RR*d2s2+ds2)-m_RR*ds2*ds2)/(s2*s2);

    double neff = -3-dlns2;
    double CC = -d2lns2;
    std::vector<double> vars = {1., neff, pow(neff, 2), pow(neff, 3), pow(neff, 4), CC, m_OmegaDE * (1+m_ww)};
    std::cout << m_RR << " " << neff << " " << CC << std::endl;

    m_alpha_n = fabs(m_generic(vars, {6.0835, 1.3373, -0.1959, 0., 0., -5.5274, 0.}, false));
    m_beta_n = m_generic(vars, {2.0379, -0.7354, 0.3175, 1.2490, 0.3980, -0.1682, 0.}, false) + m_fnu*(1.081 + 0.395*neff*neff);

    m_a_n = m_generic(vars, {1.5222, 2.8553, 2.3706, 0.9903, 0.2250, -0.6038, 0.1749}, true);
    m_b_n = m_generic(vars, {-0.5642, 0.5864, 0.5716, 0., 0., -1.5474, 0.2279}, true);
    m_c_n = m_generic(vars, {0.3698, 2.0404, 0.8161, 0., 0., 0.5869, 0.}, true);
    m_gamma_n = m_generic(vars, {0.1971, -0.0843, 0., 0., 0., 0.8460, 0.}, false);
    m_mu_n = m_generic(vars, {0., 0., 0., 0., 0., 0., 0.}, false);
    m_nu_n = m_generic(vars, {5.2105, 3.6902, 0., 0., 0., 0., 0.}, true);
}


// ============================================================================


double pkmodel::PowerSpectrum_HALOFIT::m_delta2_Lin (const double kk) const
{
    return pow(kk, 3) / m_norm * linear(kk);
}


// ============================================================================


std::vector<double> pkmodel::PowerSpectrum_HALOFIT::m_delta2_Lin (const std::vector<double> kk) const
{
    std::vector<double> delta2_lin = linear(kk);

    for (size_t i=0; i<kk.size(); i++)
        delta2_lin[i] *= pow(kk[i], 3)/m_norm;

    return delta2_lin;
}


// ============================================================================


double pkmodel::PowerSpectrum_HALOFIT::m_delta2 (const double kk) const
{
    double delta2_L = m_delta2_Lin(kk);
    double delta2_L_nu = delta2_L*(1+m_fnu*47.48 * pow(kk, 2)/(1+1.5*pow(kk, 2 )));

    double y = kk * m_RR;

    // DELTA_Q
    double exp_f_y = exp(-( y / 4 + y * y / 8));
    double deltaQ = delta2_L * ( pow( 1 + delta2_L_nu, m_beta_n) / ( 1+ m_alpha_n * delta2_L_nu) ) * exp_f_y;

    // DELTA H
    double deltaH_prime = m_a_n * pow(y, 3 * m_f1) / (1 + m_b_n * pow(y, m_f2) + pow(m_c_n * m_f3 * y, 3-m_gamma_n) );
    double deltaH = deltaH_prime / (1 + m_mu_n / y + m_nu_n / (y * y));

    return deltaQ + deltaH;
}


// ============================================================================


std::vector<double> pkmodel::PowerSpectrum_HALOFIT::m_delta2 (const std::vector<double> kk) const
{
    std::vector<double> delta2_L = m_delta2_Lin(kk);
    std::vector<double> delta2 = std::vector<double>(kk.size());

    for (size_t i=0; i<kk.size(); i++)
    {
        double y = kk[i] * m_RR;
        double exp_f_y = exp(-( y/ 4 + y * y / 8));

        double delta2_L_nu =delta2_L[i]*(1+m_fnu*47.48 * pow(kk[i], 2)/(1+1.5*pow(kk[i], 2 ) ) );
        double deltaQ = delta2_L[i] * pow( 1 + delta2_L_nu, m_beta_n) / ( 1+ m_alpha_n * delta2_L_nu) * exp_f_y;

        double deltaH_prime = m_a_n * pow(y, 3 * m_f1) / (1 + m_b_n * pow(y, m_f2) + pow(m_c_n * m_f3 * y, 3-m_gamma_n) );
        double deltaH = deltaH_prime / (1 + m_mu_n / y + m_nu_n / (y * y));
        delta2[i] = deltaQ + deltaH;
    }

    return delta2;
}


// ============================================================================


pkmodel::PowerSpectrum_HALOFIT::PowerSpectrum_HALOFIT (
        const std::vector<double> kk,
        const std::vector<double> PkLin,
        double OmegaM,
        double OmegaNu,
        double OmegaDE,
        double w0)
{
    set(kk, PkLin, OmegaM, OmegaNu, OmegaDE, w0);
}


// ============================================================================


void pkmodel::PowerSpectrum_HALOFIT::set (const std::vector<double> kk,
        const std::vector<double> PkLin,
        double OmegaM,
        double OmegaNu,
        double OmegaDE,
        double w0)
{
    // Set cosmological parameters
    m_fnu = OmegaNu/OmegaM;

    double frac = OmegaDE / (1-OmegaM);
    m_f1 = (1-frac)*pow(OmegaM, -0.0732) + (frac)*pow(OmegaM, -0.0307);
    m_f2 = (1-frac)*pow(OmegaM, -0.1423) + (frac)*pow(OmegaM, -0.0585);
    m_f3 = (1-frac)*pow(OmegaM, 0.0725) + (frac)*pow(OmegaM, 0.0743);
    m_OmegaDE = OmegaDE;
    m_ww = w0;

    // Set power spectrum interpolator
    m_pk_linear = cbl::glob::FuncGrid (kk, PkLin, "Spline");

    m_set_internal();
}


// ============================================================================


double pkmodel::PowerSpectrum_HALOFIT::operator() (const double kk) const
{
    return m_delta2(kk) * m_norm / pow(kk, 3);
}


// ============================================================================


std::vector<double> pkmodel::PowerSpectrum_HALOFIT::operator() (const std::vector<double> kk) const
{
    std::vector<double> pk =  m_delta2(kk);

    for (size_t i=0; i<kk.size(); i++)
        pk[i] = pk[i] * m_norm / pow(kk[i], 3);

    return pk;
}


// ============================================================================


double pkmodel::PowerSpectrum_HALOFIT::linear (const double kk) const
{
    return m_pk_linear(kk);
}


// ============================================================================


std::vector<double> pkmodel::PowerSpectrum_HALOFIT::linear (const std::vector<double> kk) const
{
    return m_pk_linear.eval_func(kk);
}
