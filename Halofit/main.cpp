#include "CBL.h"
#include "PowerSpectrum_Halofit.h"

// these two variables contain the name of the CosmoBolognaLib
// directory and the name of the current directory (useful when
// launching the code on remote systems)
std::string cbl::par::DirCosmo = DIRCOSMO, cbl::par::DirLoc = DIRL;

using namespace std;

int main (int argc, char **argv) {

    (void) argc;
    
    cbl::cosmology::Cosmology cosmology {cbl::cosmology::CosmologicalModel::_Planck15_};

    double redshift = atof(argv[1]);

    std::vector<double> kk = cbl::logarithmic_bin_vector(1024, 1.e-5, 100.);
    std::vector<double> pk_CAMB = cosmology.Pk_DM(kk, "CAMB", false, redshift, "./");
    std::vector<double> pk_CAMB_HF = cosmology.Pk_DM(kk, "CAMB", true, redshift, "./");

    ofstream fout("pk_"+cbl::conv(redshift, cbl::par::fDP0)+".dat");

    pkmodel::PowerSpectrum_HALOFIT pk_halofit (kk, pk_CAMB, cosmology.OmegaM(redshift), cosmology.OmegaNu(redshift), cosmology.OmegaDE(redshift), cosmology.w0());

    for (size_t i=0; i<kk.size(); i++) {
        fout << kk[i] << " " << pk_CAMB[i] << " " << pk_CAMB_HF[i]  << " " << pk_halofit(kk[i])  << std::endl;
    }

    fout.clear(); fout.close();

    return 0;
}
