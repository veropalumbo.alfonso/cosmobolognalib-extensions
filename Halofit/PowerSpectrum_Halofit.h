#include "CBL.h"

#ifndef _PKHALOFIT_
#define _PKHALOFIT_

namespace pkmodel {

    class PowerSpectrum_HALOFIT
    {
        protected:

            cbl::glob::FuncGrid m_sigma8_to_RR;

            double m_norm = (2*cbl::par::pi*cbl::par::pi);

            double m_fnu;

            double m_OmegaDE;

            double m_ww;

            double m_f1;

            double m_f2;

            double m_f3;

            double m_RR;

            cbl::glob::FuncGrid m_pk_linear;

            cbl::glob::FuncGrid m_interp_s2;

            double m_k_sigma_inv (const double sigma8);

            double m_neff (const double RR) const;

            double m_CC (const double RR) const;

            std::vector<double> m_get_vars (const double neff, const double CC) const;

            double m_generic (const std::vector<double> var, const std::vector<double> pars, const bool isLog10) const;

            double m_a_n;

            double m_b_n;

            double m_c_n;

            double m_gamma_n;

            double m_alpha_n;

            double m_beta_n;

            double m_mu_n;

            double m_nu_n;

            double m_delta2_Lin (const double kk) const;

            double m_delta2 (const double kk) const;

            std::vector<double> m_delta2_Lin (const std::vector<double> kk) const;

            std::vector<double> m_delta2 (const std::vector<double> kk) const;

            double m_sigma2_halofit (const double R,
                    const cbl::glob::FuncGrid interp_Pk,
                    const double kmin=1.e-5,
                    const double kmax=10);

            double m_dsigma2_dR_halofit (const double R,
                    const cbl::glob::FuncGrid interp_Pk,
                    const double kmin=1.e-5,
                    const double kmax=10);


            double m_d2sigma2_dR2_halofit (const double R,
                    const cbl::glob::FuncGrid interp_Pk,
                    const double kmin=1.e-5,
                    const double kmax=10);

            void m_set_internal();

        public:

            PowerSpectrum_HALOFIT () {}

            ~PowerSpectrum_HALOFIT () = default;

            PowerSpectrum_HALOFIT (const std::vector<double> kk,
                    const std::vector<double> Pk_lin,
                    double OmegaM,
                    double OmegaNu,
                    double OmegaDE,
                    double w0);

            void set (const std::vector<double> kk,
                    const std::vector<double> Pk_lin,
                    double OmegaM,
                    double OmegaNu,
                    double OmegaDE,
                    double w0);

            double operator() (const double kk) const;

            std::vector<double> operator () (const std::vector<double> kk) const;

            double linear (const double kk) const;

            std::vector<double> linear (const std::vector<double> kk) const;
    };
}

    // ============================================================================

#endif
