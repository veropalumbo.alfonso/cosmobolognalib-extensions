#include "CBL.h"
#include <unsupported/Eigen/FFT>

#ifndef _UTILS_
#define _UTILS_

namespace cbl {


    typedef std::complex<double> Complex;

    typedef Eigen::Array<Complex, Eigen::Dynamic, 1> VectorC;

    typedef Eigen::Array<double, Eigen::Dynamic, 1> VectorD;

    typedef Eigen::Array<double, Eigen::Dynamic, Eigen::Dynamic> MatrixD;

    typedef Eigen::Array<Complex, Eigen::Dynamic, Eigen::Dynamic> MatrixC;


    //! Generates a mesh, just like Matlab's meshgrid
    //  Template specialization for column vectors (Eigen::VectorXd)
    //  in : x, y column vectors 
    //       X, Y matrices, used to save the mesh
    template <typename Scalar>
        void meshgrid(const Eigen::Array<Scalar, -1, 1>& x, 
                const Eigen::Array<Scalar, -1, 1>& y,
                Eigen::Array<Scalar, -1, -1>& X,
                Eigen::Array<Scalar, -1, -1>& Y) {
            const long nx = x.size(), ny = y.size();
            X.resize(ny, nx);
            Y.resize(ny, nx);
            for (long i = 0; i < ny; ++i) {
                X.row(i) = x.transpose();
            }
            for (long j = 0; j < nx; ++j) {
                Y.col(j) = y;
            }
        }


    //! Generates a mesh, just like Matlab's meshgrid
    //  Template specialization for row vectors (Eigen::RowVectorXd)
    //  in : x, y row vectors 
    //       X, Y matrices, used to save the mesh
    template <typename Scalar>
        void meshgrid(const Eigen::Array<Scalar, 1, -1>& x, 
                const Eigen::Array<Scalar, 1, -1>& y,
                Eigen::Array<Scalar, -1, -1>& X,
                Eigen::Array<Scalar, -1, -1>& Y) {
            Eigen::Array<Scalar, -1, 1> xt = x.transpose(),
            yt = y.transpose();
            meshgrid(xt, yt, X, Y);
        }

    Complex gamma (const Complex &zz);

    /**
     * @brief The namespace of the functions and classes used for
     *  <B> clustering models calculations </B>
     *
     *  The \e clustering namespace contains all the functions and classes
     *  used for clustering models calculations
     */
    namespace clusteringmodels {


        /**
         * @brief return $\sigma_{v}^2$ as defined in Eq. 7 of Taruya et al 2010
         * (https://arxiv.org/abs/1006.0699)
         *
         * Compute $\sigma_v^2$ as defined in Taruya et al 2010 
         * (Eq. 7 of https://arxiv.org/abs/1006.0699)
         * \f[
         *    \sigma_{\mathrm{v}, \mathrm{lin}}^{2}= \frac{1}{3} \int \frac{d^{3} \boldsymbol{q}}{(2 \pi)^{3}} \frac{P_{\operatorname{lin}}(q)}{q^{2}}
         * \f]
         *
         * where $P_{\operatorname{lin}} is the linear power spectrum at the desired redshift.
         * Indeed this quantity is redshift dependent. It's up to the user to provide the power 
         * spectrum at the necessary redshift or to rescale the result using the growth factor
         *
         * @param interp_Pk object of type cbl::glob::FuncGrid, linear power spectrum interpolator
         * @param k_min integration lower limit
         * @param k_max integration upper limit
         *
         * @return the value of sigma^2_{v}
         */
        double sigma_v_squared (const glob::FuncGrid interp_Pk, const double k_min=1.e-4, const double k_max=10);
    }
}

#endif
