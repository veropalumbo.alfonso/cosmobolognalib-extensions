#include "utils.h"


#ifndef _PKFFT_
#define _PKFFT_

namespace cbl {

    namespace clusteringmodels {

        class PkFFTlog 
        {

            protected:

                double m_delta;

                double m_bias;

                double m_k_min0;

                double m_k_max0;

                size_t m_n_max;

                Eigen::MatrixXcd m_k_nu_matrix;

                Eigen::VectorXd m_k_n;

                Eigen::VectorXd m_eta_m;

                Eigen::MatrixXcd  m_fft;

                Complex m_get_II_element(const Complex &nu1, const Complex &nu2);
                
                Eigen::MatrixXcd m_get_II (const Eigen::VectorXcd &nu);

                Eigen::MatrixXcd m_get_cm (const Eigen::VectorXd &power_spectrum);


            public:

                PkFFTlog() {}

                PkFFTlog(const double bias, const size_t n_max, const double k_min0, const double k_max0);

                ~PkFFTlog() = default;

                void set_parameters(const double bias, const size_t n_max, const double k_min0, const double k_max0);
        };

    }
}

#endif
