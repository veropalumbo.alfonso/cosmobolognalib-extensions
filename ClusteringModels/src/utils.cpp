#include "utils.h"

using namespace std;


// ================================================================


cbl::Complex cbl::gamma (const cbl::Complex &zz)
{
    gsl_sf_result norm, arg;
    gsl_sf_lngamma_complex_e(zz.real(), zz.imag(), &norm, &arg);

    Complex cc(norm.val, 2*par::pi+arg.val);
    return exp(cc);
}


// ================================================================


double cbl::clusteringmodels::sigma_v_squared (const glob::FuncGrid interp_Pk, const double k_min, const double k_max)
{
  double norm = 1./ (6* pow(par::pi, 2) );

  auto integrand = [&] (const double log_q) {
      double qq = exp(log_q);
      return qq * interp_Pk(qq);
  };

  return cbl::wrapper::gsl::GSL_integrate_cquad(integrand, log(k_min), log(k_max), 1.e-5, 0., 10000)*norm;
}


// ================================================================
