#include "PkFFTlog.h"

using namespace std;
using namespace cbl;


// ===========================================================


Complex clusteringmodels::PkFFTlog::m_get_II_element (const Complex &nu1, const Complex &nu2)
{
    return nu1+nu2;
}


// ===========================================================


Eigen::MatrixXcd clusteringmodels::PkFFTlog::m_get_II (const Eigen::VectorXcd &nu)
{
    double norm = 1./(8 * pow(par::pi, 1.5)); 

    Eigen::MatrixXcd out(m_n_max + 1, m_n_max + 1);

    for (size_t i=0; i<m_n_max+1; i++)
        for (size_t j=0; j<m_n_max+1; j++) {
            out(i, j) =  m_get_II_element(nu[i], nu[j]);
        }

    return norm * out;
}


// ===========================================================


Eigen::MatrixXcd clusteringmodels::PkFFTlog::m_get_cm (const Eigen::VectorXd &power_spectrum)
{
    return m_fft * power_spectrum;
}


// ===========================================================


clusteringmodels::PkFFTlog::PkFFTlog (const double bias, const size_t n_max, const double k_min0, const double k_max0)
{
    set_parameters(bias, n_max, k_min0, k_max0);
}


// ===========================================================


void clusteringmodels::PkFFTlog::set_parameters(const double bias, const size_t n_max, const double k_min0, const double k_max0)
{
    m_n_max = n_max;
    m_bias = bias;
    m_k_min0 = k_min0;
    m_k_max0 = k_max0;

    m_delta = 1./(m_n_max -1) * log ( m_k_max0 / m_k_min0);

    m_k_n.resize(m_n_max);

    for (size_t i=0; i<m_n_max; i++) 
        m_k_n(i) = m_k_min0 * exp(i * m_delta);

    m_eta_m.resize(m_n_max+1);
    for (size_t i=0; i<m_n_max+1; i++)
        m_eta_m(i) = 2 * par::pi / (m_n_max * m_delta) * (i - 0.5 * m_n_max);

    m_k_nu_matrix.resize(m_n_max+1, m_n_max);
    for (size_t i=0; i<m_n_max+1; i++) {
        for (size_t j=0; j<m_n_max; j++) {
            //int l = j;
            //int m = (i - 0.5 * m_n_max);
            //double arg = - 2 * par::pi * l * m / m_n_max;

            m_k_nu_matrix(i, j) = Complex(pow(m_k_n(j), m_bias) * cos(m_eta_m(i)*log(m_k_n(j)) ),
                                          pow(m_k_n(j), m_bias) * sin(m_eta_m(i)*log(m_k_n(j)) ) );
            //m_fft(i, j) = m_k_n_bias(i) * exp( - 2 * par::pi * l * m / m_n_max) * pow(m_k_min0, Complex(0, 1) * m_eta_m(j)) / m_n_max;
        }
        m_k_nu_matrix.row(i) *= 
    }
}
