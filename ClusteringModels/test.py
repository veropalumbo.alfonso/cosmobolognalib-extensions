import os
import numpy as np

import CosmoBolognaLib as cbl

home = os.environ["HOME"]+"/"
cbl.SetDirs("{0}CosmoBolognaLib/".format(home), home)

from clusteringmodels import sigma_v_squared

# Set the cosmology

cosmology = cbl.Cosmology()
redshift = 0

# define k vector
kk = np.logspace(-4, 2, 512)

# compute linear power spectrum

pk = [cosmology.Pk_DM(_kk, "CAMB", False, redshift) for _kk in kk]

# Set P(k) interpolator

interp_pk  = cbl.FuncGrid(kk, pk, "Spline")

# Compute sigma_v^2

sv2 = sigma_v_squared(interp_pk)

print("Sigma_v_squared is {sv2} Mpc^2 h^-2".format(sv2=sv2) )
