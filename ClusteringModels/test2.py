from scipy.special import loggamma, gamma
import os

import CosmoBolognaLib as cbl
import numpy as np

home = os.environ["HOME"]+"/"
cbl.SetDirs("{0}CosmoBolognaLib/".format(home), home)

from clusteringmodels import sigma_v_squared, gamma
nn = 10+1j*2
vv =  loggamma(nn)
vv2 = gamma(nn)

print(np.exp(vv), vv2, gamma(nn))
