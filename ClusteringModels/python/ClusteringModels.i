%module clusteringmodels

%include "stl.i"
%include "stdint.i"
%include "std_string.i"
%include "std_vector.i"
%include "std_complex.i"
%include <std_shared_ptr.i>

%shared_ptr(cbl::glob::FuncGrid);

%import (module="CosmoBolognaLib") "FuncGrid.h"
%import (module="CosmoBolognaLib") "Cosmology.h"

%{
#include "utils.h"
%}

%include "utils.h"
