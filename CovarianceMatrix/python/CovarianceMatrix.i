// SWIG Interface to CovarianceMatrix

%module(directors="1") covariancematrix

%include "stl.i"
%include "stdint.i"
%include "std_string.i"
%include "std_vector.i"
%include <std_shared_ptr.i>

%ignore *::operator[];
%ignore *::operator=;

%shared_ptr(cbl::catalogue::Catalogue);
%shared_ptr(cbl::glob::FuncGrid);

%shared_ptr(cbl::measure::covmat::Covariance_TwoPointCorrelation1D_monopole);
%shared_ptr(cbl::measure::covmat::Covariance_TwoPointCorrelation_multipoles);

%import (module="CosmoBolognaLib") "Kernel.h"
%import (module="CosmoBolognaLib") "FuncGrid.h"
%import (module="CosmoBolognaLib") "CovarianceMatrix.h"
%import (module="CosmoBolognaLib") "Catalogue.h"

%{
#include "CovarianceMatrix_TwoPointCorrelation1D_monopole.h"
#include "CovarianceMatrix_TwoPointCorrelation_multipoles.h"
%}

%include "CovarianceMatrix_TwoPointCorrelation1D_monopole.h"
%include "CovarianceMatrix_TwoPointCorrelation_multipoles.h"
